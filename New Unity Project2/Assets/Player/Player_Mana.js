﻿var maxMana : int;
public static var manaPoints : int;
var manaRegen : int;
var tex : Texture2D;
var x = 25;
var tex2 : Texture2D;
var timer : float;
var told : float;

function Start () 
{
	help2 = GameObject.Find("Main Camera");
	maxMana = help2.GetComponent(CharacterMenu).personMana;
	manaPoints = maxMana;
	manaMana = help2.GetComponent(CharacterMenu).personMRegen;
	if (networkView.isMine)	
	{
		timer = 0;
		told = 0;
		manaPoints = maxMana;
	}
}

function Update () {
	timer = Time.time;
	if (manaRegen != 0)
	{	
		if (manaRegen * (timer - told) >= 1)
		{
			if (manaPoints < maxMana)
			{
				manaPoints += 1;
			}
			told = timer;
		}
	}
	if (manaPoints > maxMana)
	{
		manaPoints = maxMana;
	}
}

function OnGUI()
{
	if ((networkView.isMine) && (maxMana != 0))
	{
		var y : int = Screen.height / 3;
		var dy : int = y / maxMana;
		y = dy * maxMana;
		GUI.DrawTexture(new Rect(Screen.width - 2 * x - 5, 5, x, y), tex);
		GUI.DrawTexture(new Rect(Screen.width - 2 * x - 5, 5, x, dy * manaPoints), tex2);
		GUI.Label(new Rect(Screen.width - 2 * x - 5, y + 5, x, 100), manaPoints.ToString());
	}
}