﻿var maxStamina : int;
public static var staminaPoints : int;
var staminaRegen : int;
var tex : Texture2D;
var x = 25;
var tex2 : Texture2D;
var timer : float;
var told : float;
public static var timerStop : float;
var timerStopLast : float;
var st : int;
public var nw : boolean;

function Start () 
{
	//timerStopLast = Time.time;
	timerStop = Time.time;
	help2 = GameObject.Find("Main Camera");
	maxStamina = help2.GetComponent(CharacterMenu).personStamina;
	staminaPoints = maxStamina;
	staminaRegen = help2.GetComponent(CharacterMenu).personSRegen;
	if (networkView.isMine)	
	{
		timer = 0;
		told = 0;
		staminaPoints = maxStamina;
	}
}

function Update () {
	nw = false;
	if (networkView.isMine)
	{
	nw = true;
	st = staminaPoints;
	timer = Time.time;
	if (staminaRegen != 0)
	{	
		//Debug.Log(staminaRegen);
		//Debug.Log(1 / staminaRegen);
		if ((staminaRegen * (timer - told) >= 1) && (Time.time + 1.5 >= timerStop))
		{
			if (staminaPoints < maxStamina)
			{
				staminaPoints += 1;
			}
			told = timer;
		}
	}
	if (staminaPoints > maxStamina)
	{
		staminaPoints = maxStamina;
	}
	}
}

function OnGUI()
{
	if (networkView.isMine)
	{
		var y : int = Screen.height / 3;
		var dy : int = y / maxStamina;
		y = dy * maxStamina;
		GUI.DrawTexture(new Rect(Screen.width - x - 5, 5, x, y), tex);
		GUI.DrawTexture(new Rect(Screen.width - x - 5, 5, x, dy * staminaPoints), tex2);
		GUI.Label(new Rect(Screen.width - x - 5, y + 5, x, 100), staminaPoints.ToString());
	}
}