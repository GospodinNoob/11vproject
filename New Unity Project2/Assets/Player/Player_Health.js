﻿var maxHealth : int;
public var hitPoints : int;
var healthRegen : int;
var detonationDelay = 0.0;
var explosion : Transform;
var deadReplacement : Rigidbody;
var team : int;
var respawn : GameObject;
var go : Rigidbody;
var PlayerPrefab : Rigidbody;
var cam : GameObject;
var help : GameObject;
var help2 : GameObject;
var tex : Texture2D;
var x = 25;
var tex2 : Texture2D;
public var killer : String;
public var weapon : String;
var nickname : String;
var hitstr : String;
var d1 : int;
var d2 : int;
public var help3 : GameObject;
var main : boolean;
var cam2 : GameObject;

public var wetPoint : int = 0;
public var firePoint : int = 0;
public var frozenPoint : int = 0;
public var poisonPoint : int = 0;
public var lightPoint : int = 0;
public var darknessPoint : int = 0;
public var natureForsePoint : int = 0;
public var shockPoint : int = 0;
public var physicalShieldPoint : int = 0;
public var bleedingPoint : int = 0;

var slashingArmour : int = 0;
var hackingArmour : int = 0;
var crushingArmour : int = 0;

var fireResistance : float = 0;
var frozenResistance : float = 0;
var poisonResistance : float = 0;
var lightResistance : float = 0;
var darknessResistance : float = 0;
var natureResistance : float = 0;
var shockResistance : float = 0;
var bleedingResistance : float = 0;

var timer : float;
var told : float;


function Start()
{
	help = GameObject.Find("ShitHelpMagicObject");
	cam2 = GameObject.Find("Main Camera");
	maxHealth = cam2.GetComponent(CharacterMenu).personHits;
	hitPoints = maxHealth;
	healthRegen = cam2.GetComponent(CharacterMenu).personHRegen;
	nickname = gameObject.name;
	if (networkView.isMine)	
	{
		timer = 0;
		told = 0;
		hitPoints = maxHealth;
	}
}


function OnGUI()
{
	if (hitPoints > maxHealth)
	{
		hitPoints = maxHealth;
	}
//		networkView.RPC ("ApH", RPCMode.All, hitPoints);
	//}
	//networkView.RPC ("ApKil", RPCMode.All, killer);
//	}
	if (networkView.isMine)
	{
		var y : float = Screen.height / 3;
		var dy : float = y / maxHealth;
		y = dy * maxHealth;
		//GUI.Label(new Rect(0, 0, 25, 100), tex);
		GUI.DrawTexture(new Rect(5, 5, x, Mathf.Round(y)), tex);
		GUI.DrawTexture(new Rect(5, 5, x, Mathf.Round(dy * hitPoints)), tex2);
		//var str : String;
		//str = str(hitPoints);
		var hits : int = hitPoints;
		if (hits == 0)
		{
			hitstr = '0+';
		}
		else
		{
			hitstr = hits.ToString();
		}
		GUI.Label(new Rect(5, y + 5, x, 100), hitstr);
		//GUI.Label(new Rect(5, y + 15, x, 100), y.ToString());
		//GUI.Label(new Rect(5, y + 25, x, 100), (dy * hitPoints).ToString());
	}
	//GUI.Label(new Rect((Screen.width)/2, 5, 100, 5), killer + " убил " + help.tag + " (" + weapon + ")");
}

@RPC
function Respawn()
{
	//if (networkView.viewID == 1)
	//{
		//respawn = GameObject.Find("Spawn_1");
//		team = 1;
	//}
//	else
	//{
		//team = 2;
//		respawn = GameObject.Find("Spawn_2");
	//}
	//Detonate();
	//gameObject.GetComponentInChildren(Camera).camera.enabled = false;
	//gameObject.GetComponentInChildren(AudioListener).enabled = false;
	if ((networkView.isMine) && (main))
	{
		//cam = GameObject.Find("Main Camera");
		cam.GetComponent(Camera).enabled = false;
		cam.GetComponent(AudioListener).enabled = false;
		cam2.GetComponent(Camera).enabled = true;
		cam2.GetComponent(AudioListener).enabled = true;
		help = GameObject.Find("ShitHelpMagicObject");
		help.rigidbody.mass = 2;
		MessageKillSender(killer, nickname, weapon, Time.timeSinceLevelLoad);
//		yield WaitForSeconds(1.0);
		Network.Destroy(gameObject);
		//gameObject.SetActiveRecursively(false);
		//help3.SetActiveRecursively(false);
		//gameObject.light.enabled = true;
		
	}
	//Destroy(gameObject);
	//cam.gameObject.GetComponent(AudioListener).enabled = true;
	//go = Network.Instantiate(PlayerPrefab, respawn.transform.position, respawn.transform.rotation, team);
	//go.transform.GetComponentInChildren(Camera).camera.enabled = true;
	//go.transform.GetComponentInChildren(AudioListener).enabled = true;
	//Detonate();
}

function MessageKillSender(killer : String, deathman : String, weapon : String, team : float)
{	//if (networkView.isMine)
	//{
		help.GetComponent(KillerScript).killer = killer;
		help.GetComponent(KillerScript).deathman = deathman;
		help.GetComponent(KillerScript).weapon = weapon;
		//Debug.Log(killer);
		//Debug.Log(deathman);
		help.GetComponent(KillerScript).teamkil = killer[killer.Length - 2].ToString();
		help.GetComponent(KillerScript).teamdet = deathman[deathman.Length - 2].ToString();
		help.GetComponent(KillerScript).time = Time.time;
			//}
}

@RPC 
function ApH(hea : int)
{
	if (hitPoints > hea)
	{
		hitPoints = hea;
	}
//	if (hitPoints <= 0.0)
	//{
	//	MessageKillSender(killer, nickname, weapon, Time.timeSinceLevelLoad);
//		Network.Destroy(gameObject);//
	//	if (main//)
		//{
	///		Network.Destroy(gameObject);
//			networkView.RPC ("Destr", RPCMode.All);//
			//Respawn();
		//}
		//else
		//{
		//}
	//}
}

@RPC 
function Destr()
{
	if(main)
	{
		Destroy(gameObject);
	}
}

@RPC
public function ApplyK(kil : String)
{
//	Debug.Log(kil);
	killer = kil;
//	Debug.Log(killer);
}

public function ApplyKiller(kil : String)
{
//	Debug.Log(kil);
	killer = kil;
	networkView.RPC ("ApplyK", RPCMode.All, kil);//, SendMessageOptions.DontRequireReceiver);
//	Debug.Log(killer);
}

@RPC
public function ApplyW(wea : String)
{
	weapon = wea;
}

public function ApplyWeapon(wea : String)
{
	weapon = wea;
	networkView.RPC ("ApplyW", RPCMode.All, wea);//, SendMessageOptions.DontRequireReceiver);
}

function ApplyTeam(tea : int)
{
	team = tea;
}

function Update()
{
	timer = Time.time;
	if (healthRegen != 0)
	{	
		if (healthRegen * (timer - told) >= 1)
		{
			if (hitPoints < maxHealth)
			{
				hitPoints += 1;
			}
			told = timer;
		}
	}
//	if (hitPoints > maxHealth)//
//	{/
//		hitPoints = maxHealth;
	//}
	nickname = gameObject.transform.root.gameObject.name;
	main = gameObject.transform.root.gameObject.light.enabled;
	//if (main)
	//{
	networkView.RPC ("ApH", RPCMode.All, hitPoints);
	//}
	//networkView.RPC ("ApKil", RPCMode.All, killer);
	if (hitPoints <= 0.0)
	{
		//var emitter : ParticleEmitter = GetComponentInChildren(ParticleEmitter); 
		//if (emitter)
		//emitter.emit = true;
		//Invoke("DelayedDetonate", detonationDelay);
		//if (networkView.ismine)
		//{
		//	MessageKillSender(killer, nickname, weapon, Time.timeSinceLevelLoad);
		//}
		//gameObject.GetComponent(NicnameMessager).Set
	//	Network.Destroy(gameObject);
//		if (main)
	//	{
			//Network.Destroy(gameObject);
		//	networkView.RPC ("Destr", RPCMode.All);
			//Respawn();
			networkView.RPC ("Respawn", RPCMode.All);
		//}
//		else
		//{
		//	gameObject.active = false;
		//}
	}
}

function ApplyDamage (damage : float)
{
	hitPoints -= damage; 
}

function ApplySlashDamage (damage : float)
{
	var physical = physicalShieldPoint;
	if (physical > damage)
	{
		physicalShieldPoint -= damage;
		damage = 0;
	}
	else
	{
		physicalShieldPoint = 0;
		damage -= physical;
	}
	damage -= slashingArmour;
	if (damage <= 0)
	{
		damage = 0;
	}
	hitPoints -= damage ; 
}

function ApplyHackDamage (damage : float)
{
	var physical = physicalShieldPoint;
	if (physical > damage)
	{
		physicalShieldPoint -= damage;
		damage = 0;
	}
	else
	{
		physicalShieldPoint = 0;
		damage -= physical;
	}
	damage -= hackingArmour;
	if (damage <= 0)
	{
		damage = 0;
	}
	hitPoints -= damage ; 
}

function ApplyCrushDamage (damage : float)
{
	var physical = physicalShieldPoint;
	if (physical > damage)
	{
		physicalShieldPoint -= damage;
		damage = 0;
	}
	else
	{
		physicalShieldPoint = 0;
		damage -= physical;
	}
	damage -= crushingArmour;
	if (damage <= 0)
	{
		damage = 0;
	}
	hitPoints -= damage ; 
}

function ApplyFireDamage (damage : float)
{
	damage *= (1 - fireResistance);
	var frost = frozenPoint;
	if (frost > damage)
	{
		frozenPoint -= damage;
		damage = 0;
	}
	else
	{
		frozenPoint = 0;
		damage -= frost;
		//firePoint += damage;
	}
	var wet = wetPoint;
	if (wet > damage)
	{
		damage = 0;
		wetPoint -= damage;
	}
	else
	{
		wetPoint = 0;
		damage -= wet;
		firePoint += damage;
	}
	if (damage <= 0)
	{
		damage = 0;
	}
	hitPoints -= damage ; 
}

function ApplyFrostDamage (damage : float)
{
	damage *= (1 - frozenResistance);
	var fire = firePoint;
	if (fire > damage)
	{
		frozenPoint -= damage;
		damage = 0;
	}
	else
	{
		firePoint = 0;
		damage -= fire;
		//frozenPoint += damage;
	}
	var wet = wetPoint;
	if (wet > damage)
	{
		damage = 0;
		wetPoint -= damage;
		frozenPoint += damage * 2;
	}
	else
	{
		wetPoint = 0;
		frozenPoint += damage + wet;
	}
	if (damage <= 0)
	{
		damage = 0;
	}
	hitPoints -= damage ; 
}

function ApplyPoisonDamage (damage : float)
{
	damage *= (1 - poisonResistance);
	var nature = natureForsePoint;
	if (nature > damage)
	{
		natureForsePoint -= damage;
		damage = 0;
	}
	else
	{
		natureForsePoint = 0;
		damage -= nature;
		poisonPoint += damage;
	}
	if (damage <= 0)
	{
		damage = 0;
	}
	hitPoints -= damage ; 
}

function ApplyShockDamage (damage : float)
{
	damage *= (1 - shockResistance);
	var wet = wetPoint;
	if (wet >= 3 * damage)
	{
		damage *= 3;
		shockPoint += damage;
	}
	else
	{
		damage += wet;
		shockPoint += damage;
	}
	if (damage <= 0)
	{
		damage = 0;
	}
	hitPoints -= damage ; 
}

function ApplyBleedidngDamage (damage : float)
{
	damage *= (1 - bleedingResistance);
	var nature = natureForsePoint;
	if (nature > damage)
	{
		natureForsePoint -= damage;
		damage = 0;
	}
	else
	{
		natureForsePoint = 0;
		damage -= nature;
		bleedingPoint += damage;
	}
	if (damage <= 0)
	{
		damage = 0;
	}
	hitPoints -= damage ; 
}

function ApplyDarknessDamage (damage : float)
{
	damage *= (1 - darknessResistance);
	var light = lightPoint;
	if (light > damage)
	{
		lightPoint -= damage;
		damage = 0;
	}
	else
	{
		lightPoint = 0;
		damage -= light;
	}
	var nature = natureForsePoint;
	if (nature > damage)
	{
		natureForsePoint -= damage;
		damage = 0;
	}
	else
	{
		naturePoint = 0;
		damage -= nature;
		darknessPoint += damage;
	}
	if (damage <= 0)
	{
		damage = 0;
	}
	hitPoints -= damage ; 
}

function ApplyLightDamage (damage : float)
{
	damage *= (1 - lightResistance);
	var darkness = darknessPoint;
	if (darkness > damage)
	{
		darknessPoint -= damage;
		damage = 0;
	}
	else
	{
		darknessPoint = 0;
		damage -= darkness;
	}
	if (damage <= 0)
	{
		damage = 0;
	}
	hitPoints -= damage ; 
}

function ApplyNatureDamage (damage : float)
{
	damage *= (1 - natureResistance);
	var darkness = darknessPoint;
	if (darkness > damage)
	{
		darknessPoint -= damage;
		damage = 0;
	}
	else
	{
		darknessPoint = 0;
		damage -= darkness;
	}
	var nature = natureForsePoint;
	if (nature > damage)
	{
		natureForsePoint -= damage;
		damage = 0;
	}
	else
	{
		natureForsePoint = 0;
		damage -= nature;
	}
	if (damage <= 0)
	{
		damage = 0;
	}
	hitPoints -= damage ; 
}

function ApplyFirePoints (point : int)
{
	point *= (1 - fireResistance);
	var frost = frozenPoint;
	if (frost > point)
	{
		frozenPoint -= point;
	}
	else
	{
		frozenPoint = 0;
		point -= frost;
		firePoint += point;
	}
}

function ApplyFrozenPoints (point : int)
{
	point *= (1 - frozenResistance);
	var fire = firePoint;
	if (fire > point)
	{
		firePoint -= point;
	}
	else
	{
		firePoint = 0;
		point -= fire;
		frozenPoint += point;
	}
}

function ApplyPhysicalShieldPoints (point : int)
{
	physicalShieldPoint += point;
}

function ApplyNatureForsePoints (point : int)
{
	point *= (1 - natureResistance);
	var darkness = darknessPoint;
	if (darkness > point)
	{
		darknessPoint -= point;
		point = 0;
	}
	else
	{
		darknessPoint = 0;
		point -= darkness;
	}
	var poison = poisonPoint;
	if (poison > point)
	{
		poisonPoint -= point;
		point = 0;
	}
	else
	{
		poisonPoint = 0;
		point -= poison;
	}
	var bleeding = bleedingPoint;
	if (bleeding > point)
	{
		bleedingPoint -= point;
		point = 0;
	}
	else
	{
		bleedingPoint = 0;
		point -= bleeding;
	}
	natureForsePoint += point;
}

function ApplyLightPoints (point : int)
{
	point *= (1 - lightResistance);
	var darkness = darknessPoint;
	if (darkness > point)
	{
		darknessPoint -= point;
		point = 0;
	}
	else
	{
		darknessPoint = 0;
		point -= darkness;
	}
	var bleeding = bleedingPoint;
	if (bleeding > point)
	{
		bleedingPoint -= point;
		point = 0;
	}
	else
	{
		bleedingPoint = 0;
		point -= bleeding;
	}
	var poison = poisonPoint;
	if (poison > point)
	{
		poisonPoint -= point;
		point = 0;
	}
	else
	{
		poisonPoint = 0;
		point -= poison;
	}
	lightPoint += point;
}

function ApplyDarknessPoints (point : int)
{
	point *= (1 - darknessResistance);
	var light = lightPoint;
	if (light > point)
	{
		lightPoint -= point;
		point = 0;
	}
	else
	{
		lightPoint = 0;
		point -= light;
	}
	var nature = natureForsePoint;
	if (nature > point)
	{
		natureForsePoint -= point;
		point = 0;
	}
	else
	{
		naturePoint = 0;
		point -= nature;
	}
	darknessPoint += point;
}

function ApplyShockPoints (point : int)
{
	point *= (1 - shockResistance);
	shockPoint += point;
}

function ApplyWetPoint (point : int)
{
	wetPoint += point;
}

function ApplyBleedingPoints (point : int)
{
	point *= (1 - bleedingResistance);
	var nature = natureForsePoint;
	if (nature > point)
	{
		natureForsePoint -= point;
		point = 0;
	}
	else
	{
		naturePoint = 0;
		point -= nature;
	}
	var light = lightPoint;
	if (light > point)
	{
		lightPoint -= point;
		point = 0;
	}
	else
	{
		lightPoint = 0;
		point -= light;
	}
	bleedingPoint += point;
}


function DelayedDetonate ()
{
	BroadcastMessage ("Detonate");
}

@RPC 



function Detonate ()
{
	
	//if (main)
	//{//Network.Destroy(gameObject);
		Network.Destroy(gameObject);
//	}
	//else
	//{
		
		//yield WaitForSeconds(0.1); 
		//Destroy(gameObject);
	//}
	//if (explosion)
	//	Instantiate (explosion, transform.position, transform.rotation);
	//if (deadReplacement)
	//{
		//var dead : Rigidbody = Instantiate(
		//deadReplacement, transform.position, transform.rotation);
		//dead.rigidbody.velocity = rigidbody.velocity; 
		//dead.angularVelocity = rigidbody.angularVelocity;
	//}

	//var emitter : ParticleEmitter = GetComponentInChildren(ParticleEmitter); 
	//if (emitter)
	//{
		//emitter.emit = false; 
		//emitter.transform.parent = null;
	//}
}