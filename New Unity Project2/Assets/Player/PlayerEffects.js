﻿#pragma strict

var wetPoint : int = 0;
var firePoint : int = 0;
var frozenPoint : int = 0;
var poisonPoint : int = 0;
var lightPoint : int = 0;
var darknessPoint : int = 0;
var natureForsePoint : int = 0;
var shockPoint : int = 0;
var physicalShieldPoint : int = 0;
var bleedingPoint : int = 0;

var fireResistance : float = 0;
var frozenResistance : float = 0;
var poisonResistance : float = 0;
var lightResistance : float = 0;
var darknessResistance : float = 0;
var natureResistance : float = 0;
var shockResistance : float = 0;
var bleedingResistance : float = 0;

var wetTimer : float = -10;
var fireTimer : float = -10;
var frozenTimer : float = -10;
var poisonTimer : float = -10;
var lightTimer : float = -10;
var darknessTimer : float = -10;
var natureTimer : float = -10;
var shockTimer : float = -10;
var shieldTimer : float = -10;
var bleedingTimer : float = -10;

var wetTexture : Texture2D;
var fireTexture : Texture2D;
var frozenTexture : Texture2D;
var poisonTexture : Texture2D;
var lightTexture : Texture2D;
var darknessTexture : Texture2D;
var natureTexture : Texture2D;
var shockTexture : Texture2D;
var shieldTexture : Texture2D;
var bleedingTexture : Texture2D;

var wetEffect : GameObject;
var fireEffect : GameObject;
var darknessEffect : GameObject;
var lightEffect : GameObject;
var shieldEffect : GameObject;
var shockEffect : GameObject;

var max : int;
var num : int;
var arr = [-1] * 11;
var arr2 = [-1] * 11;


function Start () {
	fireResistance = gameObject.GetComponent(Player_Health).fireResistance;
	frozenResistance = gameObject.GetComponent(Player_Health).frozenResistance;
	poisonResistance = gameObject.GetComponent(Player_Health).poisonResistance;
	lightResistance = gameObject.GetComponent(Player_Health).lightResistance;
	darknessResistance = gameObject.GetComponent(Player_Health).darknessResistance;
	natureResistance = gameObject.GetComponent(Player_Health).natureResistance;
	shockResistance = gameObject.GetComponent(Player_Health).shockResistance;
	bleedingResistance = gameObject.GetComponent(Player_Health).bleedingResistance;

}

function OnWet()
{
	arr[num + 1] = 0;
	arr2[num + 1] = wetPoint;
	num += 1;
	if (Time.time - wetTimer >= 1)
	{
		///gameObject.rigidbody.SendMessageUpwards("ApplyDamage", (1 - fireResistance) * 0.33, SendMessageOptions.DontRequireReceiver);
		gameObject.GetComponent(Player_Health).wetPoint -= 1;
		wetTimer = Time.time;
	}
	wetEffect.active = true;
}

function OnFire()
{
	arr[num + 1] = 1;
	arr2[num + 1] = firePoint;
	num += 1;
	if (Time.time - fireTimer >= 0.3333)
	{
		gameObject.rigidbody.SendMessageUpwards("ApplyDamage", (1 - fireResistance) * 0.33, SendMessageOptions.DontRequireReceiver);
		gameObject.GetComponent(Player_Health).firePoint -= 1;
		fireTimer = Time.time;
	}
	fireEffect.active = true;
}

function OnFrozen()
{
	arr[num + 1] = 2;
	arr2[num + 1] = frozenPoint;
	num += 1;
	if (Time.time - frozenTimer >= 1)
	{
		gameObject.rigidbody.SendMessageUpwards("ApplyDamage", 1 - frozenResistance, SendMessageOptions.DontRequireReceiver);
		gameObject.GetComponent(Player_Health).frozenPoint -= 1;
		frozenTimer = Time.time;
	}
}

function OnPoison()
{
	arr[num + 1] = 3;
	arr2[num + 1] = poisonPoint;
	num += 1;
	if (Time.time - poisonTimer >= 0.1)
	{
		gameObject.rigidbody.SendMessageUpwards("ApplyDamage", (1 - poisonResistance) * 0.75, SendMessageOptions.DontRequireReceiver);
		gameObject.GetComponent(Player_Health).poisonPoint -= 1;
		poisonTimer = Time.time;
	}
}

function OnLight()
{
	arr[num + 1] = 4;
	arr2[num + 1] = lightPoint;
	num += 1;
	if (Time.time - lightTimer >= 0.1)
	{
		gameObject.rigidbody.SendMessageUpwards("ApplyDamage", -1 + lightResistance, SendMessageOptions.DontRequireReceiver);
		gameObject.GetComponent(Player_Health).lightPoint -= 1;
		lightTimer = Time.time;
	}
	lightEffect.active = true;
}

function OnDarkness()
{
	arr[num + 1] = 5;
	arr2[num + 1] = darknessPoint;
	num += 1;
	if (Time.time - darknessTimer >= 1)
	{
		gameObject.rigidbody.SendMessageUpwards("ApplyDamage", (1 - darknessResistance) * 0.2, SendMessageOptions.DontRequireReceiver);
		gameObject.GetComponent(Player_Health).darknessPoint -= 1;
		darknessTimer = Time.time;
	}
	darknessEffect.active = true;
}

function OnNature()
{
	arr[num + 1] = 6;
	arr2[num + 1] = natureForsePoint;
	num += 1;
	if (Time.time - natureTimer >= 1)
	{
		gameObject.rigidbody.SendMessageUpwards("ApplyDamage", -1 + natureResistance, SendMessageOptions.DontRequireReceiver);
		gameObject.GetComponent(Player_Health).natureForsePoint -= 1;
		natureTimer = Time.time;
	}
}

function OnShock()
{
	arr[num + 1] = 7;
	arr2[num + 1] = shockPoint;
	num += 1;
	if (Time.time - shockTimer >= 0.1)
	{
		if (wetPoint > 0)
		{
			if (wetPoint > shockPoint)
			{
				gameObject.rigidbody.SendMessageUpwards("ApplyDamage", (1 - shockResistance) * shockPoint, SendMessageOptions.DontRequireReceiver);
				gameObject.GetComponent(Player_Health).shockPoint = 0;
			}
			else
			{
				gameObject.rigidbody.SendMessageUpwards("ApplyDamage", (1 - shockResistance) * wetPoint, SendMessageOptions.DontRequireReceiver);
				gameObject.GetComponent(Player_Health).shockPoint -= wetPoint;
			}
			shockEffect.active = true;
		}
		else
		{
			gameObject.GetComponent(Player_Health).shockPoint -= 1;
			shockEffect.active = false;
		}
		shockTimer = Time.time;
	}
}

function OnShield()
{
	arr[num + 1] = 8;
	arr2[num + 1] = physicalShieldPoint;
	num += 1;
	if (Time.time - shieldTimer >= 1)
	{
		//gameObject.rigidbody.SendMessageUpwards("ApplyDamage", -1 + natureResistance, SendMessageOptions.DontRequireReceiver);
		gameObject.GetComponent(Player_Health).physicalShieldPoint -= 1;
		shieldTimer = Time.time;
	}
	shieldEffect.active = true;
}

function OnBleeding()
{
	arr[num + 1] = 9;
	arr2[num + 1] = bleedingPoint;
	num += 1;
	if (Time.time - bleedingTimer >= 1)
	{
		gameObject.rigidbody.SendMessageUpwards("ApplyDamage", 1 - bleedingResistance, SendMessageOptions.DontRequireReceiver);
		gameObject.GetComponent(Player_Health).bleedingPoint -= 1;
		bleedingTimer = Time.time;
	}
}

@RPC 
function ApFir(hea : int)
{
	if (firePoint < hea)
	{
		firePoint = hea;
	}
}

@RPC 
function ApFro(hea : int)
{
	if ( frozenPoint < hea)
	{
		frozenPoint = hea;
	}
}

@RPC 
function ApPoi(hea : int)
{
	if ( poisonPoint < hea)
	{
		poisonPoint = hea;
	}
}

@RPC 
function ApDar(hea : int)
{
	if ( darknessPoint < hea)
	{
		darknessPoint = hea;
	}
}

@RPC 
function ApLig(hea : int)
{
	if (lightPoint < hea)
	{
		lightPoint = hea;
	}
}

@RPC 
function ApNat(hea : int)
{
	if ( natureForsePoint < hea)
	{
		natureForsePoint = hea;
	}
}

@RPC 
function ApWet(hea : int)
{
	if (wetPoint < hea)
	{
		wetPoint = hea;
	}
}

@RPC 
function ApBle(hea : int)
{
	if ( bleedingPoint < hea)
	{
		bleedingPoint = hea;
	}
}

@RPC 
function ApShi(hea : int)
{
	if ( physicalShieldPoint < hea)
	{
		physicalShieldPoint = hea;
	}
}

@RPC 
function ApSho(hea : int)
{
	if (shockPoint > hea)
	{
		shockPoint = hea;
	}
}

function OnGUI () {
	arr = [-1] * 11;
	arr2 = [-1] * 11;
	num = 0;
	networkView.RPC ("ApFir", RPCMode.All, firePoint);
	networkView.RPC ("ApFro", RPCMode.All, frozenPoint);
	networkView.RPC ("ApPoi", RPCMode.All, poisonPoint);
	networkView.RPC ("ApDar", RPCMode.All, darknessPoint);
	networkView.RPC ("ApLig", RPCMode.All, lightPoint);
	networkView.RPC ("ApNat", RPCMode.All, natureForsePoint);
	networkView.RPC ("ApSho", RPCMode.All, shockPoint);
	networkView.RPC ("ApBle", RPCMode.All, bleedingPoint);
	networkView.RPC ("ApWet", RPCMode.All, wetPoint);
	networkView.RPC ("ApShi", RPCMode.All, physicalShieldPoint);
	max = gameObject.GetComponent(Player_Health).maxHealth;
	
	wetPoint = gameObject.GetComponent(Player_Health).wetPoint;
	firePoint = gameObject.GetComponent(Player_Health).firePoint;
	frozenPoint = gameObject.GetComponent(Player_Health).frozenPoint;
	poisonPoint = gameObject.GetComponent(Player_Health).poisonPoint;
	lightPoint = gameObject.GetComponent(Player_Health).lightPoint;
	darknessPoint = gameObject.GetComponent(Player_Health).darknessPoint;
	natureForsePoint = gameObject.GetComponent(Player_Health).natureForsePoint;
	shockPoint = gameObject.GetComponent(Player_Health).shockPoint;
	physicalShieldPoint = gameObject.GetComponent(Player_Health).physicalShieldPoint;
	bleedingPoint = gameObject.GetComponent(Player_Health).bleedingPoint;
	if (wetPoint > 0 && networkView.isMine)
	{
		OnWet();
	}
	else
	{
		wetEffect.active = false;
	}
	if (firePoint > 0 && networkView.isMine)
	{
		OnFire();
	}
	else
	{
		fireEffect.active = false;
	}
	if (frozenPoint > 0 && networkView.isMine)
	{
		OnFrozen();
	}
	if (poisonPoint > 0 && networkView.isMine)
	{
		OnPoison();
	}
	if (lightPoint > 0 && networkView.isMine)
	{
		OnLight();
	}
	else
	{
		lightEffect.active = false;
	}
	if (darknessPoint > 0 && networkView.isMine)
	{
		OnDarkness();
	}
	else
	{
		darknessEffect.active = false;
	}
	if (natureForsePoint > 0 && networkView.isMine)
	{
		OnNature();
	}
	if (shockPoint > 0)
	{
		OnShock();
	}
	else
	{
		shockEffect.active = false;
	}
	if (physicalShieldPoint > 0 && networkView.isMine)
	{
		OnShield();
	}
	else
	{
		shieldEffect.active = false;
	}
	if (bleedingPoint > 0 && networkView.isMine)
	{
		OnBleeding();
	}
	var y = 5;
	if (networkView.isMine)
	{
	for (var i = 1; i <= num; i++)
	{
		if (arr[i] == 0)
		{
			GUI.DrawTexture(new Rect(30, y, 10, 10), wetTexture, ScaleMode.ScaleToFit);
		}
		if (arr[i] == 1)
		{
			GUI.DrawTexture(new Rect(30, y, 10, 10), fireTexture,  ScaleMode.ScaleToFit);
		}
		if (arr[i] == 2)
		{
			GUI.DrawTexture(new Rect(30, y, 10, 10), frozenTexture, ScaleMode.ScaleToFit);
		}
		if (arr[i] == 3)
		{
			GUI.DrawTexture(new Rect(30, y, 10, 10), poisonTexture, ScaleMode.ScaleToFit);
		}
		if (arr[i] == 4)
		{
			GUI.DrawTexture(new Rect(30, y, 10, 10), lightTexture, ScaleMode.ScaleToFit);
		}
		if (arr[i] == 5)
		{
			GUI.DrawTexture(new Rect(30, y, 10, 10), darknessTexture, ScaleMode.ScaleToFit);
		}
		if (arr[i] == 6)
		{
			GUI.DrawTexture(new Rect(30, y, 10, 10), natureTexture, ScaleMode.ScaleToFit);
		}
		if (arr[i] == 7)
		{
			GUI.DrawTexture(new Rect(30, y, 10, 10), shockTexture, ScaleMode.ScaleToFit);
		}
		if (arr[i] == 8)
		{
			GUI.DrawTexture(new Rect(30, y, 10, 10), shieldTexture, ScaleMode.ScaleToFit);
		}
		if (arr[i] == 9)
		{
			GUI.DrawTexture(new Rect(30, y, 10, 10), bleedingTexture, ScaleMode.ScaleToFit);
		}
		var pointstr = arr2[i].ToString();
		GUI.Label(new Rect(60, y, 100, 25), pointstr);
		y += 15;
	}
	}
	
}