﻿//#pragma strict
var x : float;
var y : float;
var z : float;
var tr : Quaternion;
var main : boolean;

@RPC
function CoordinatsMessager(x : float, y : float, z : float, name : String)
{
	if (name == gameObject.transform.root.gameObject.name)
	{
		gameObject.transform.position.x = x;
		gameObject.transform.position.y = y;
		gameObject.transform.position.z = z;	
	}
}

@RPC
function RotationMessager(t : Quaternion, name : String)
{
	if (name == gameObject.transform.root.gameObject.name)
	{
		//gameObject.transform.rotation = Quaternion.Euler(Vector3(x, y, z));
		//gameObject.transform.rotation.x = x;
		//gameObject.transform.rotation.y = y;
		//gameObject.transform.rotation.z = z;
		gameObject.transform.rotation = t;
	}
}

function Start () {
	main = gameObject.transform.root.light.enabled;
}

function Update () {
	main = gameObject.transform.root.light.enabled;
	if (main && networkView.isMine)
	{
		x = gameObject.transform.position.x;
		y = gameObject.transform.position.y;
		z = gameObject.transform.position.z;
		networkView.RPC ("CoordinatsMessager", RPCMode.All, x, y, z, gameObject.transform.root.gameObject.name);
		//x = gameObject.transform.rotation.x;
	//	y = gameObject.transform.rotation.y;
		//z = gameObject.transform.rotation.z;
		tr = gameObject.transform.rotation;
		networkView.RPC ("RotationMessager", RPCMode.All, tr, gameObject.transform.root.gameObject.name);
	}
}