﻿public var ind : int;
var tf : boolean = false;
public var shoot : boolean = false;
public var now : boolean = false;
var lastSel : float = -10;
var player : GameObject;
public var anim : boolean;
var lastShot : float;
public var ef : GameObject;

@RPC
function Strike(name : String, tf : boolean)
{
	if (name == gameObject.transform.root.gameObject.name)
	{
		shoot = tf;
	}
}

@RPC
function An(name : String, tf : boolean, ls : float)
{
	if (name == gameObject.transform.root.gameObject.name)
	{
		anim = tf;
		lastShot = ls;
	}
}

function Awake() 
{
	anim = false;
	ind = 0;
	networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
}

function Update()
{
	if (networkView.isMine)
	{
		networkView.RPC ("An", RPCMode.All, gameObject.transform.root.transform.gameObject.name, anim, lastShot);
	}
	tr = false;
	tf = gameObject.transform.root.gameObject.light.enabled;
	if (tf)
	{
		if (Input.GetButton ("Fire1") && networkView.isMine)
		{ 
			tr = true;
			networkView.RPC ("Strike", RPCMode.All, gameObject.transform.root.gameObject.name, tr);
		}
		else
		{
			tr = false;
			networkView.RPC ("Strike", RPCMode.All, gameObject.transform.root.gameObject.name, tr);
		}
	}
	if (Input.GetKeyDown("1") && networkView.isMine)
	{
		ind = 0;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown("2") && networkView.isMine)
	{
		ind = 1;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown("3") && networkView.isMine)
	{
		ind = 2;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown("4") && networkView.isMine)
	{
		ind = 3;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown("5") && networkView.isMine)
	{
		ind = 4;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown("6") && networkView.isMine)
	{
		ind = 5;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown("7") && networkView.isMine)
	{
		ind = 6;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown("8") && networkView.isMine)
	{
		ind = 7;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown("9") && networkView.isMine)
	{
		ind = 8;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown("0") && networkView.isMine)
	{
		ind = 9;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	if (Time.time - lastSel >= 1 && networkView.isMine)
	{
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
		lastSel = Time.time;
	}
}

var slot1 : GameObject;
var slot2 : GameObject;
var slot3 : GameObject;
var slot4 : GameObject;
var slot5 : GameObject;
var slot6 : GameObject;
var slot7 : GameObject; 
var slot8 : GameObject;
var slot9 : GameObject;
var slot10 : GameObject;

@RPC
function SelectWeapon(nickname : String, ind : int)
{
	if (nickname == gameObject.transform.root.transform.gameObject.name)
	{
		slot1.GetComponent(ActiveWeaponRight).activ = false;
		slot2.GetComponent(ActiveWeaponRight).activ = false;
		slot3.GetComponent(ActiveWeaponRight).activ = false;
		slot4.GetComponent(ActiveWeaponRight).activ = false;
		slot5.GetComponent(ActiveWeaponRight).activ = false;
		slot6.GetComponent(ActiveWeaponRight).activ = false;
		slot7.GetComponent(ActiveWeaponRight).activ = false;
		slot8.GetComponent(ActiveWeaponRight).activ = false;
		slot9.GetComponent(ActiveWeaponRight).activ = false;
		slot10.GetComponent(ActiveWeaponRight).activ = false;	
		switch(ind)
		{	
			case 0: slot1.GetComponent(ActiveWeaponRight).activ = true; break;
			case 1:	slot2.GetComponent(ActiveWeaponRight).activ = true; break;
			case 2:	slot3.GetComponent(ActiveWeaponRight).activ = true; break;
			case 3:	slot4.GetComponent(ActiveWeaponRight).activ = true; break;
			case 4:	slot5.GetComponent(ActiveWeaponRight).activ = true; break;
			case 5:	slot6.GetComponent(ActiveWeaponRight).activ = true; break;
			case 6:	slot7.GetComponent(ActiveWeaponRight).activ = true; break;
			case 7:	slot8.GetComponent(ActiveWeaponRight).activ = true; break;
			case 8:	slot9.GetComponent(ActiveWeaponRight).activ = true; break;
			case 9: slot10.GetComponent(ActiveWeaponRight).activ = true; break;
		}		
	}
}
