﻿#pragma strict
public var fire : boolean;
public var frozen : boolean;
public var poison : boolean;
public var darkness : boolean;
public var ligh : boolean;
public var nature : boolean;
public var shock : boolean;
public var wet : boolean;
public var bleeding : boolean;

var wetEf : GameObject;
var lightEf : GameObject;
var darknessEf : GameObject;
var shockEf : GameObject;
var fireEf : GameObject;
var acidEf : GameObject;

function Start () {
	fire = false;
	ligh = false;
	darkness = false;
	shock = false;
	poison = false;
	wet = false;

}

function Update () {
	fireEf.active = fire;
	lightEf.active = ligh;
	darknessEf.active = darkness;
	shockEf.active = shock;
	acidEf.active = poison;
	wetEf.active = wet;
}