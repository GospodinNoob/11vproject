﻿var nowshot : boolean;
var lastShot : float;
var reloadTime = 0.5;
var lefthand : GameObject;
var damage : GameObject;
var damage2 : GameObject;
var tf : boolean;
var player : GameObject;
var help : GameObject;
var anim : boolean;

function Start () {
	anim = false;
	help = gameObject.transform.parent.parent.parent.gameObject;
	player = gameObject.transform.parent.parent.parent.gameObject.GetComponent(PlayerWeaponRight2).player;
}

@RPC
function An(tf:boolean)
{
	lastShot = Time.time;
	anim = tf;
//	if(anim) {Destroy(gameObject);}
}

function Update () 
{	
	tf = gameObject.transform.parent.parent.parent.gameObject.GetComponent(PlayerWeaponRight2).shoot;
	//damage.active = tf;
	damage2.active = false;
	if (tf)
	{
		//damage.active = tf;
		//damage2.active = tf;
		Fire1();
	}
	else
	{
		//damage.GetComponent(AxeAreaDamageRight).tf = false;
		//damage2.GetComponent(AxeAreaDamageRight).tf = false;
	}
	nowshot = false;
	nowshot = damage.active;//.GetComponent(AxeAreaDamageRight).tf;// && damage2.GetComponent(AxeAreaDamageRight).tf;
	//lastShot = help.GetComponent(PlayerWeaponRight2).lastShot;
	//anim = help.GetComponent(PlayerWeaponRight2).anim;
	if (((Time.time - lastShot <= 0.5 * 1.25) && (nowshot || anim)))
	{
	//	if ((player.GetComponent(Player_Stamina).nw))
//		{
			help.GetComponent(PlayerWeaponRight2).anim = true;
//		}
		damage.active = nowshot;
		lefthand.animation["NonMagicAttack1"].speed = 0.8;
		lefthand.animation.Play("NonMagicAttack1");
	}
	else
	{
		damage.active = false;
		//damage.SetActiveRecursively(false);
		lefthand.animation.Play("None");
//		if ((player.GetComponent(Player_Stamina).nw))
//		{
		//	help.GetComponent(PlayerWeaponRight2).anim = false;
//		}
	}

}

function Fire1 ()
{
	if ((player.GetComponent(Player_Stamina).nw) && (Time.time > reloadTime + lastShot) && (player.GetComponent(Player_Stamina).staminaPoints >= gameObject.transform.parent.GetComponent(ActiveWeaponRight).pound))
	{
		player.GetComponent(Player_Stamina).staminaPoints -= gameObject.transform.parent.GetComponent(ActiveWeaponRight).pound;
		player.GetComponent(Player_Stamina).timerStop = Time.time;
		lastShot = Time.time;
		//damage.GetComponent(AxeAreaDamageRight).tf = true;
		//damage2.GetComponent(AxeAreaDamageRight).tf = true;
		damage.active = tf;
		damage2.active = tf;
		tf = false;
		networkView.RPC ("An", RPCMode.All, true);
	//	help.GetComponent(PlayerWeaponRight2).anim = true;
//		help.GetComponent(PlayerWeaponRight2).lastShot = lastShot;
		//damage.SetActiveRecursively(true);
		//Shoot();
	}
}