﻿#pragma strict
public var tf : boolean;
var nickname : String;
var enemy : String;
var enh : int;
var enh2 : int;
var health : int;
var na : String;
var fireDamage = 0;
var firePoint = 0;
var frozenDamage = 0;
var frozenPoint = 0;
var poisonDamage = 0;
var poisonPoint = 0;
var darknessDamage = 0;
var darknessPoint = 0;
var lightDamage = 0;
var natureDamage = 0;
var shockDamage = 0;
var shockPoint = 0;
var wetPoint = 0;
var bleedingPoint = 0;
var manaDestr = 0;
var theifHits = 0;
var theifMana = 0;
var slashDamage = 0;
var hackDamage = 0;
var crushDamage = 0;
var damageForse = 0;
var main : boolean;

function Start () {
	tf = false;
	na = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).name;
	fireDamage = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).fireDamage;
	firePoint = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).firePoint;
	frozenDamage = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).frozenDamage;
	frozenPoint = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).frozenPoint;
	poisonDamage = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).poisonDamage;
	poisonPoint = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).poisonPoint;
	darknessDamage = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).darknessDamage;
	darknessPoint = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).darknessPoint;
	lightDamage = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).lightDamage;
	natureDamage = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).natureDamage;
	shockDamage = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).shockDamage;
	shockPoint = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).shockPoint;
	wetPoint = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).wetPoint;
	bleedingPoint = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).bleedingPoint;
	//manaDestr = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).manaDestr;
	//theifHits = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).theifHits;
	//theifMana = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).theifMana;
	slashDamage = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).slashDamage;
	hackDamage = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).hackDamage;
	crushDamage = gameObject.transform.parent.parent.GetComponent(ActiveWeaponLeft).crushDamage;
	main = gameObject.transform.root.gameObject.light.enabled;
}

function Update () {
	//enemy = "";
	//nickname = "";

}

function OnTriggerStay (collision : Collider) 
{
	//enemy = "";
	//nickname = "";
	//if (tf)
	//{		
		nickname = gameObject.transform.root.gameObject.name;
		if ((collision.rigidbody) && (collision.gameObject.transform.root.gameObject.name != nickname) && (main))// && (tf))
		{
			if (collision.gameObject.layer != "Shield")
			{
			enh = 100;
			enh2 = 100;
			enh = collision.gameObject.transform.root.gameObject.GetComponent(Player_Health).hitPoints;
			//collision.rigidbody.transform.GetComponent(Player_Health).ApplyKiller(nickname);
			//collision.rigidbody.transform.GetComponent(Player_Health).ApplyWeapon(name);
			collision.rigidbody.SendMessageUpwards("ApplyKiller", nickname, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyWeapon", name, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyCrushDamage", crushDamage * (1 + 0.01 * damageForse), SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyHackDamage", hackDamage * (1 + 0.01 * damageForse), SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplySlashDamage", slashDamage * (1 + 0.01 * damageForse), SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyFireDamage", fireDamage, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyFrostDamage", frozenDamage, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyPoisonDamage", poisonDamage, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyShockDamage", shockDamage, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyDarknessDamage", darknessDamage, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyLightDamage", lightDamage, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyNatureDamage", natureDamage, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyFirePoints", firePoint, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyFrozenPoints", frozenPoint, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyDarknessPoints", darknessPoint, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyShockPoints", shockPoint, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyWetPoint", wetPoint, SendMessageOptions.DontRequireReceiver);
			collision.rigidbody.SendMessageUpwards("ApplyBleedingPoints", bleedingPoint, SendMessageOptions.DontRequireReceiver);
			gameObject.active = false;
		//	tf = false;
			}
			else
			{
			gameObject.active = false;
			//	tf = false;
			}
			//if (enh - enh2 >= 35 || enh2 == 0)
		//	{
		//		enemy = "";
		//		nickname = "";
		//		tf = false;
		//	}
			//Destroy(gameObject.transform.root.gameObject);
		}
		//gameObject.active = tf;
		//yield WaitForSeconds(0.1); 
		//tf = false;
		//}
	//}
}