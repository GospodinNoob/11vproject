﻿var maxDamageArea : float;
var curDamageArea : float;
var timer : float;
var told : float;
var help : String;
var deltaDamage : float = maxDamageArea / (1 + 0.5) * 3;
var tf : boolean;
var reloadtime : float = 0.1;
var lastshot : float = -10;
var tstart : float = -10;
var enemy : String;
var nickname : String;

function Start () {
	help = gameObject.transform.root.gameObject.name;
	deltaDamage = maxDamageArea / (1 + 0.5);
}

function Update () {
	timer = tstart + 0.5;
	tf = false;
	tf = gameObject.transform.parent.transform.parent.gameObject.transform.GetComponent(FireConeTFDamage).tf;
	if (!tf)
	{
	//	gameObject.GetComponent(MeshRenderer).enabled = false;
		timer = tstart + 0.5;
		tstart = Time.time;
	}
	else
	{
	//	gameObject.GetComponent(MeshRenderer).enabled = true;
	}
	if ((Time.time - timer) * deltaDamage <= maxDamageArea)
	{
		curDamageArea = (Time.time - timer) * deltaDamage;
	}
	else
	{
		curDamageArea = maxDamageArea;
	}
	if (curDamageArea < 0)
	{
		curDamageArea = 0;
	}
}

function OnTriggerStay (collision : Collider) 
{
	if (tf)
	{
		//var contact : ContactPoint = collision.contacts[0]; 
		//var rotation = Quaternion.FromToRotation(Vector3.up, contact.normal);
		//var colliders : Collider[] = Physics.OverlapSphere (contact.point, 1);
 		//for (var hit in colliders) 
		//{
		//	if (!hit)
		//	continue;			
		enemy = collision.gameObject.transform.root.gameObject.name;
		nickname = gameObject.transform.root.gameObject.name;
			if (collision.rigidbody && enemy != nickname && Time.time > lastshot + reloadtime)
			{
				//hit.rigidbody.AddExplosionForce(explosionPower, explosionPosition, explosionRadius, 3.0);
				//var closestPoint = hit.rigidbody.ClosestPointOnBounds(explosionPosition); 
				//var distance = Vector3.Distance(closestPoint, explosionPosition);
				//var hitPoints = 1.0 - Mathf.Clamp01(distance / explosionRadius); 
				//hitPoints *= explosionDamage;
				lastshot = Time.time;
				var health : int = curDamageArea;
				//health = 1;
				var weapon = "Поток огня";
				collision.rigidbody.SendMessageUpwards("ApplyKiller", help, SendMessageOptions.DontRequireReceiver);
				collision.rigidbody.SendMessageUpwards("ApplyWeapon", weapon, SendMessageOptions.DontRequireReceiver);
				collision.rigidbody.SendMessageUpwards("ApplyFireDamage", health, SendMessageOptions.DontRequireReceiver);
			}
		//}
	}
}