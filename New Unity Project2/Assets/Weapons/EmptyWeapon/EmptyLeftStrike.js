﻿var nowshot : boolean;
var lastShot : float;
var reloadTime = 0.5;
var lefthand : GameObject;
var damage : GameObject;
//var damage2 : GameObject;
var tf : boolean;
var player : GameObject;
var anim : boolean;
var help : GameObject;
//var nickname : string;

function Start () {
	anim = false;
	help = gameObject.transform.parent.parent.parent.gameObject;
	player = gameObject.transform.root.gameObject;//gameObject.transform.parent.parent.parent.gameObject.GetComponent(PlayerWeaponLeft2).player;
}

@RPC
function An(tf:boolean)
{
	lastShot = Time.time;
	anim = tf;
//	if(anim) {Destroy(gameObject);}
}

function Update () 
{
	tf = gameObject.transform.parent.parent.parent.gameObject.GetComponent(PlayerWeaponLeft2).shoot;
	if (tf)
	{
		Fire2();
	}
	nowshot = false;
	nowshot = damage.active;
	//nowshot = true;
	if ((((Time.time - lastShot) * 3 <= 0.4) && (nowshot || anim)))
	{
		help.GetComponent(PlayerWeaponLeft2).anim = true;
		lefthand.animation["MagicAttack1"].speed = 0.75 * 3;
		lefthand.animation.Play("MagicAttack1");
		if ((((Time.time - lastShot) * 3 >= 0.1) && (nowshot || anim)))
		{
			damage.active = nowshot;
		}
	}
	else
	{
		if ((((Time.time - lastShot) * 3 <= 0.8) && (nowshot || anim)))
		{
			help.GetComponent(PlayerWeaponLeft2).anim = true;
			lefthand.animation["MagicAttack1"].speed = -0.75 * 3;
			lefthand.animation.Play("MagicAttack1");
		}
		if ((((Time.time - lastShot) * 3 <= 0.7) && (nowshot || anim)))
		{
			damage.active = nowshot;
		}
		else
		{
			damage.active = false;//false;
		};
		lefthand.animation.Play("None");
	}
}

function Fire2 ()
{
	if ((player.GetComponent(Player_Stamina).nw) && (Time.time > reloadTime + lastShot) && (player.GetComponent(Player_Stamina).staminaPoints >= gameObject.transform.parent.GetComponent(ActiveWeaponLeft).pound))

	{
		player.GetComponentInChildren(Player_Stamina).staminaPoints -= gameObject.transform.parent.GetComponent(ActiveWeaponLeft).pound;
		player.GetComponent(Player_Stamina).timerStop = Time.time;
		lastShot = Time.time;
		damage.active = tf;
		tf = false;
		networkView.RPC ("An", RPCMode.All, true);
	}
}