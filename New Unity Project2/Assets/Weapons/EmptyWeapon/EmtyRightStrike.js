﻿var nowshot : boolean;
var lastShot : float;
var reloadTime = 0.5;
var righthand : GameObject;
var damage : GameObject;
//var damage2 : GameObject;
var tf : boolean;
var player : GameObject;
var help : GameObject;
var anim : boolean;

function Start () {
	anim = false;
	help = gameObject.transform.parent.parent.parent.gameObject;
	player = gameObject.transform.parent.parent.parent.gameObject.GetComponent(PlayerWeaponRight2).player;
}

@RPC
function An(tf:boolean)
{
	lastShot = Time.time;
	anim = tf;
}

function Update () 
{	
	tf = gameObject.transform.parent.parent.parent.gameObject.GetComponent(PlayerWeaponRight2).shoot;
	if (tf)
	{
		Fire1();
	}
	else
	nowshot = false;
	nowshot = damage.active;
	if ((((Time.time - lastShot) * 3 <= 0.4) && (nowshot || anim)))
	{
		help.GetComponent(PlayerWeaponRight2).anim = true;
		righthand.animation["MagicAttack1"].speed = 0.75 * 3;
		righthand.animation.Play("MagicAttack1");
		if ((((Time.time - lastShot) * 3 >= 0.1) && (nowshot || anim)))
		{
			damage.active = nowshot;
		}
//		else
		//{
	//		damage.active = false;
/////		}
	}
	else
	{
		if ((((Time.time - lastShot) * 3 <= 0.8) && (nowshot || anim)))
		{
			help.GetComponent(PlayerWeaponRight2).anim = true;
			righthand.animation["MagicAttack1"].speed = -0.75 * 3;
			righthand.animation.Play("MagicAttack1");
		}
		if ((((Time.time - lastShot) * 3 <= 0.7) && (nowshot || anim)))
		{
			damage.active = nowshot;
		}
		else
		{
			damage.active = false;//false;
		}
		righthand.animation.Play("None");
	}

}

function Fire1 ()
{
	if ((player.GetComponent(Player_Stamina).nw) && (Time.time > reloadTime + lastShot))// && (player.GetComponent(Player_Stamina).staminaPoints >= gameObject.transform.parent.GetComponent(ActiveWeaponRight).pound))
	{
		player.GetComponent(Player_Stamina).staminaPoints -= gameObject.transform.parent.GetComponent(ActiveWeaponRight).pound;
		player.GetComponent(Player_Stamina).timerStop = Time.time;
		lastShot = Time.time;
		damage.active = tf;
		tf = false;
		networkView.RPC ("An", RPCMode.All, true);
	}
}