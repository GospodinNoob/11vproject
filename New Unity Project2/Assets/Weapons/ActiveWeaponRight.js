﻿#pragma strict

public var activ : boolean;
var weapon : GameObject;
var mc : GameObject;
var ind : int;
var type : int;
var type1 : GameObject;
var type0 : GameObject;
var f : boolean;
var na : String;
var fireDamage = 0;
var firePoint = 0;
var frozenDamage = 0;
var frozenPoint = 0;
var poisonDamage = 0;
var poisonPoint = 0;
var darknessDamage = 0;
var darknessPoint = 0;
var lightDamage = 0;
var natureDamage = 0;
var shockDamage = 0;
var shockPoint = 0;
var wetPoint = 0;
var bleedingPoint = 0;
var manaDestr = 0;
var theifHits = 0;
var theifMana = 0;
var slashDamage = 0;
var hackDamage = 0;
var crushDamage = 0;
var speedAttack = 0;
var pound = 0;
var damageForse = 0;
var forse = 0;
var 	actWeapon : GameObject;

function Start () {
	mc = GameObject.FindGameObjectWithTag("MainCamera");
	type = mc.GetComponent(CharacterMenu).rightHand.Han[ind].typeofmodel;
	na = mc.GetComponent(CharacterMenu).rightHand.Han[ind].name;
	fireDamage = mc.GetComponent(CharacterMenu).rightHand.Han[ind].fireDamage;
	firePoint = mc.GetComponent(CharacterMenu).rightHand.Han[ind].firePoint;
	frozenDamage = mc.GetComponent(CharacterMenu).rightHand.Han[ind].frozenDamage;
	frozenPoint = mc.GetComponent(CharacterMenu).rightHand.Han[ind].frozenPoint;
	poisonDamage = mc.GetComponent(CharacterMenu).rightHand.Han[ind].poisonDamage;
	poisonPoint = mc.GetComponent(CharacterMenu).rightHand.Han[ind].poisonPoint;
	darknessDamage = mc.GetComponent(CharacterMenu).rightHand.Han[ind].darknessDamage;
	darknessPoint = mc.GetComponent(CharacterMenu).rightHand.Han[ind].darknessPoint;
	lightDamage = mc.GetComponent(CharacterMenu).rightHand.Han[ind].lightDamage;
	natureDamage = mc.GetComponent(CharacterMenu).rightHand.Han[ind].natureDamage;
	shockDamage = mc.GetComponent(CharacterMenu).rightHand.Han[ind].shockDamage;
	shockPoint = mc.GetComponent(CharacterMenu).rightHand.Han[ind].shockPoint;
	wetPoint = mc.GetComponent(CharacterMenu).rightHand.Han[ind].wetPoint;
	bleedingPoint = mc.GetComponent(CharacterMenu).rightHand.Han[ind].bleedingPoint;
	manaDestr = mc.GetComponent(CharacterMenu).rightHand.Han[ind].manaDestr;
	theifHits = mc.GetComponent(CharacterMenu).rightHand.Han[ind].theifHits;
	theifMana = mc.GetComponent(CharacterMenu).rightHand.Han[ind].theifMana;
	slashDamage = mc.GetComponent(CharacterMenu).rightHand.Han[ind].slashDamage;
	hackDamage = mc.GetComponent(CharacterMenu).rightHand.Han[ind].hackDamage;
	crushDamage = mc.GetComponent(CharacterMenu).rightHand.Han[ind].crushDamage;
	pound = mc.GetComponent(CharacterMenu).rightHand.Han[ind].pound;
	speedAttack = mc.GetComponent(CharacterMenu).totalCharacteristic.speedAttackPers;
	damageForse = mc.GetComponent(CharacterMenu).totalCharacteristic.damageForse;
	forse = mc.GetComponent(CharacterMenu).totalCharacteristic.forse;
	f = true;
	actWeapon = gameObject.transform.parent.parent.gameObject;
	switch (type)
	{
		case 0: f = false; weapon = type0; break;
		case 1: weapon = type1; break;
	}	
}

function Update () {
//	if (f)/
	//{
		weapon.active = activ;
//	}
	if (((firePoint > 0) || (fireDamage > 0)) && (activ && f))
	{
		actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).fire =  activ && f;
	}
	else
	{
	if (f) {	actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).fire =  false;}
	}
	if ((natureDamage > 0) && (activ && f))
	{
		actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).nature =  activ && f;
	}
	else
	{
	if (f) {	actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).nature =  false;}
	}
	if (((shockPoint > 0) || (shockDamage > 0)) && (activ && f))
	{
		actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).shock =  activ && f;
	}
	else
	{
	if (f) {actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).shock =  false;}
	}
	if ((wetPoint > 0) && (activ && f))
	{
		actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).wet =  activ && f;
	}
	else
	{
	if (f) {	actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).wet =  false;}
	}
	if ((lightDamage > 0) && (activ && f))
	{
		actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).ligh =  activ && f;
	}
	else
	{
	if (f) {	actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).ligh =  false;}
	}
	if (((darknessPoint > 0) || (darknessDamage > 0)) && (activ && f))
	{
		actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).darkness =  activ && f;
	}
	else
	{
	if (f) {	actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).darkness =  false;}
	}
	if (((poisonPoint > 0) || (poisonDamage > 0)) && (activ && f))
	{
		actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).poison =  activ && f;
	}
	else
	{
	if (f) {	actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).poison =  false;}
	}
	if ((( frozenPoint > 0) || (frozenDamage > 0)) && (activ && f))
	{
		actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).frozen =  activ && f;
	}
	else
	{
	if (f) {	actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).frozen =  false;}
	}
	if (( bleedingPoint > 0) && (activ && f))
	{
		actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).bleeding =  activ && f;
	}
	else
	{
	if (f) {	actWeapon.GetComponent(PlayerWeaponRight2).ef.GetComponent(WeaponEffects).bleeding =  false;}
	}
}