﻿public var ind : int;
var help : GameObject;
var shield : GameObject;
var tf : boolean = false;
public var shoot : boolean = false;
public var now : boolean = false;
var time : float;
var lastSel : float = -10;

@RPC
function Strike(name : String, tf : boolean)
{
	if (name == gameObject.transform.root.gameObject.name)
	{
		//fireBall.GetComponent(FireballStrikeRight).Fire1();
		shoot = tf;
	}
}

function Awake() 
{
	ind = 0;
	networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
}

function Update()
{
	help.GetComponent(FireConeTFDamage).tf = shoot;
	shield.GetComponent(ShieldLeftStrike).tf = shoot;
	//fireBall.GetComponent(FireballStrikeRight).tf = shoot;
	tr = false;
	tf = gameObject.transform.root.gameObject.light.enabled;
	if (tf)
	{
		if (Input.GetButton ("Fire2") && networkView.isMine)
		{ 
			tr = false;
			if (ind == 1)
			{
				if (gameObject.transform.root.gameObject.GetComponent(Player_Mana).manaPoints > 0)
				{
					tr = true;
				}
			}
			else
			{
				tr = true;
			}
			networkView.RPC ("Strike", RPCMode.All, gameObject.transform.root.gameObject.name, tr);
		}
		else
		{
			networkView.RPC ("Strike", RPCMode.All, gameObject.transform.root.gameObject.name, false);
	//	if (ind == 2 || ind == 1 || ind == 0)
	//	{
	//		fa = true;
	//			FireConeShoot(false);
	//	}
		}
	}
	if (Input.GetKeyDown(KeyCode.F1) && networkView.isMine)
	{
		ind = 0;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown(KeyCode.F2) && networkView.isMine)
	{
		ind = 1;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown(KeyCode.F3) && networkView.isMine)
	{
		ind = 2;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown(KeyCode.F4) && networkView.isMine)
	{
		ind = 3;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown(KeyCode.F4) && networkView.isMine)
	{
		ind = 3;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown(KeyCode.F5) && networkView.isMine)
	{
		ind = 4;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	else if (Input.GetKeyDown(KeyCode.F6) && networkView.isMine)
	{
		ind = 5;
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
	}
	if (Time.time - lastSel >= 1 && networkView.isMine)
	{
		networkView.RPC ("SelectWeapon", RPCMode.All, gameObject.transform.root.transform.gameObject.name, ind);
		lastSel = Time.time;
	}
}

@RPC
function SelectWeapon(nickname : String, ind : int)
{
	//ind index;
	if (nickname == gameObject.transform.root.transform.gameObject.name)
	{
		for (var i=0;i<transform.childCount;i++)
		{
			if (i == ind)
				transform.GetChild(i).gameObject.SetActiveRecursively(true); 
			else
				transform.GetChild(i).gameObject.SetActiveRecursively(false);
		}
	}
}
