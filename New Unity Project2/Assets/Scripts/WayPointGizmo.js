/*
var text  : Texture;
function OnDrawGizmos ()
{
	Gizmos.DrawIcon(transform.position,"");
}
*/
var waypoints : Transform[];
function OnDrawGizmos(){
	
	Gizmos.color = Color.white;	
	for(var i : int = 0; i< waypoints.Length;i++)
	{
		var pos : Vector3 = waypoints[i].position;
		if(i>0)
		{
			var prev : Vector3 = waypoints[i-1].position;
			Gizmos.DrawLine(prev,pos);
		}
	}
}
