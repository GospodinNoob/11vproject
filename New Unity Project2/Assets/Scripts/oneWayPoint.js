//---------------------------------------------------
// Working Waypoint
//---------------------------------------------------
var Waypoint  : Transform[];
var speed  : float = 4;
private var currentWaypoint  : int;
var loop  : boolean = true;
var distance = 5;

private var other : Transform;

function Awake()
{
	Waypoint[0] = transform;
}




function Start (){
other = GameObject.FindWithTag ("Player").transform;
}

function Update (){

	var dist : int = Vector3.Distance(other.position, transform.position);
	
	if(currentWaypoint < Waypoint.length)
	{
		var target : Vector3 = Waypoint[currentWaypoint].position;
		var moveDirection  : Vector3 = target - transform.position;
		
		var velocity = rigidbody.velocity;
		if(moveDirection.magnitude < 1){
			currentWaypoint++;
		}
		else
		{
			velocity = moveDirection.normalized * speed;

		}
	}
	else
	{
		if(loop)
		{
			currentWaypoint = 0;
		}
		else
		{
			velocity = Vector3.zero;
		}
	}
	rigidbody.velocity = velocity;
	//transform.LookAt(target);//Facing target, by see the Arrow facing
		
	
	if (dist > distance)
	{
		rigidbody.velocity = velocity;
		transform.LookAt(target);
		animation.CrossFade("walk");
	}
	
	if (dist <= distance)
	{
		transform.LookAt(other);
		rigidbody.velocity = Vector3.zero;
		animation.CrossFade("idle");
	}
	//if (dist < 7)
	//{
//		transform.LookAt(other);
	//	velocity = moveDirection.normalized * speed * 3;
	//	rigidbody.velocity = velocity;
	//	animation.CrossFade("run");
	//}
}