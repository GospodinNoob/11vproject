﻿var explosion : GameObject; 
var expl : GameObject;
public static var help : String;
public static var help2 : int;
//var timer : float = 0.0;

///function Start ()
//{
//	timer = 0.0;
//}

function OnCollisionEnter (collision : Collision) 
{
	var contact : ContactPoint = collision.contacts[0]; 
	var rotation = Quaternion.FromToRotation(Vector3.up, contact.normal);
 	expl = Instantiate (explosion, contact.point, rotation);
 	expl.GetComponent(FireballExplosion).help = help;
 	expl.GetComponent(FireballExplosion).help2 = help2;
	Kill ();
}

//function Update()
//{
//	timer = Time.time;
//	if (timer >= 10)
//	{
//		Kill();
//	}
//}

function Kill ()
{
var emitter : ParticleEmitter= GetComponentInChildren(ParticleEmitter); 
	if (emitter)
		emitter.emit = false;


	transform.DetachChildren();

	Destroy(gameObject);
}
