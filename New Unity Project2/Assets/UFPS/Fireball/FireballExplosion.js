﻿var explosionRadius = 1.0; 
var explosionPower = 3.0; 
var explosionDamage = 1000.0;
var explosionTime = 1.0;
public static var help : String;
public  static var help2 : int;

function Start () 
{
	var explosionPosition = transform.position;
	var colliders : Collider[] = Physics.OverlapSphere (explosionPosition, explosionRadius);
	for (var hit in colliders) 
	{
		if (!hit)
		continue;

		if (hit.rigidbody)
		{
			//hit.rigidbody.AddExplosionForce(explosionPower, explosionPosition, explosionRadius, 3.0);
			//var closestPoint = hit.rigidbody.ClosestPointOnBounds(explosionPosition); 
			//var distance = Vector3.Distance(closestPoint, explosionPosition);
			//var hitPoints = 1.0 - Mathf.Clamp01(distance / explosionRadius); 
			//hitPoints *= explosionDamage;
			var health : int = explosionDamage;
			var weapon = "Огненный шар";
			hit.rigidbody.SendMessageUpwards("ApplyKiller", help, SendMessageOptions.DontRequireReceiver);
			hit.rigidbody.SendMessageUpwards("ApplyWeapon", weapon, SendMessageOptions.DontRequireReceiver);
			hit.rigidbody.SendMessageUpwards("ApplyFireDamage", health, SendMessageOptions.DontRequireReceiver);
		//	hit.rigidbody.SendMessageUpwards("ApplyTeam", help2, SendMessageOptions.DontRequireReceiver);
		}
	}

	if (particleEmitter) 
	{
		particleEmitter.emit = true;
		//yield WaitForSeconds(0.5);
		particleEmitter.emit = false;
	}

	Destroy(gameObject, explosionTime);
}
