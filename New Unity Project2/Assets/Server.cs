﻿using UnityEngine;
using System.Collections;
using System;
//using System.math;

public class Server : MonoBehaviour {
	
	public GameObject PlayerPrefab;	// Персонаж игрока
	public string ip = "127.0.0.1";	// ip для создания или подключения к серверу
	public string port = "5300";
	public string nickname = "Unknown";// Порт
	public bool connected;			// Статус подключения
	private GameObject _go;			// Объект для ссылки на игрока
	public bool visible = false;	// Статус показа меню
	private int cou_t1;
	private int cou_t2;
	public bool inplay;
	public GameObject respawn;
	public bool inrespawn;
	public int team;
	public GameObject help;
	public GameObject dopgo;
	private GameObject dop2;
	private int ranspawnx;
	private int ranspawnz;
	public GameObject timer;
	private GameObject timerObj;
	private bool host;
	private GameObject g1;
	private GameObject g2;
	public GameObject map;
	private GameObject mGO;
	private Vector3 pMap;
	private Quaternion qMap;
	
	
	void Start () {
		host = false;
		help = GameObject.Find("ShitHelpMagicObject"); 
	//	for (int i; i < 10; i++)
//		{
	//		playerst1[i] = "";
//			playerst2[i] = "";
	//	}
//		for (int i; i < 20; i++)
	//	{
///			players[i] = "";
	//		playerssleep[i] = "";
	//	}
	}
	
	// На каждый кадр
	void Update () {
		if (help.rigidbody.drag != 3)
		{
			if (connected)
			{
				help.rigidbody.drag = 1;
			}
			else
			{
				help.rigidbody.drag = 2;
			}
		if(Input.GetKeyUp(KeyCode.Escape)) 
			visible = !visible;
		}
	}
	
	// На каждый кадр для прорисовки кнопок
	void OnGUI () {
		if (help.rigidbody.drag != 3)
		{
		//GUI.Label(new Rect((Screen.width)/2, Screen.height / 2, 5, 5), "#");
		if (help.rigidbody.mass == 2)
		{
			inplay = false;
			g1 = GameObject.Find("Gate1");
			g2 = GameObject.Find("Gate2");
			g1.active = true;
			g2.active = true;	
		}
		else
		{
			inplay = true;
		}
		// Если мы на сервере
		if(connected) {
			if(inplay)
			{
				if(visible) {
				//	if (inrespawn)
				//	{
				//		if(GUI.Button(new Rect((Screen.width)/2 - 50, Screen.height/2 - 35, 100, 30), "Возродиться"))
				//			Respawn();
				//		if(GUI.Button(new Rect((Screen.width)/2 - 50, Screen.height/2, 100, 30), "Назад"))
				//		{
				//			inrespawn = false;
				//		}
				//	}
					GUI.Label(new Rect((Screen.width - 120)/2, Screen.height/2, 120, 30), "Присоединились: " + Network.connections.Length);
					if(GUI.Button(new Rect((Screen.width - 100)/2, Screen.height/2 + 35, 100, 30), "Отключиться"))
					{
						if (host)
						{
								Network.Destroy(timerObj);
						}
						Network.Disconnect(200);
					}
					//if(GUI.Button(new Rect((Screen.width - 100)/2, Screen.height/2 + 35, 100, 30), "Выход"))
					//	Application.Quit();
				}
				
			}
			else {
				if (inrespawn)
				{
					if(GUI.Button(new Rect((Screen.width)/2 - 50, Screen.height/2, 100, 30), "Возродиться"))
					{
						Respawn();
						if (host)
						{
							timerObj = (GameObject)Network.Instantiate(timer, respawn.transform.position, respawn.transform.rotation, team);
						}
					}
					if(GUI.Button(new Rect((Screen.width)/2 - 50, Screen.height/2 + 35, 100, 30), "Выход"))
					{
						inrespawn = false;
						inplay = false;
						visible = false;
	//					DeletePlayer(team);
					}
				}
				else
				{
					if(GUI.Button(new Rect((Screen.width)/2 - 105, Screen.height/2, 100, 30), "Команда 1")) 
							Inteam(1);
					//GUI.Label(new Rect((Screen.width)/2 - 55, Screen.height/2 - 35, 180, 30), cou_t1.ToString());
					if(GUI.Button(new Rect((Screen.width)/2 + 5, Screen.height/2, 100, 30), "Команда 2"))
							Inteam(2);
					//GUI.Label(new Rect((Screen.width)/2 + 55, Screen.height/2 - 35, 180, 30), cou_t2.ToString());
					if(GUI.Button(new Rect((Screen.width)/2 - 50, Screen.height/2 + 35, 100, 30), "Выход"))
					{
						connected = false;
						host = false;
						gameObject.transform.rigidbody.mass = 1;
						Network.Disconnect(200);
					}
				}
			}
		}
		//Если мы в главном меню
		 else {
			GUI.Label(new Rect((Screen.width - 100)/2 - 25, Screen.height/2-60, 100, 20), "Ip");
			GUI.Label(new Rect((Screen.width - 100)/2 - 25, Screen.height/2-30, 100, 20), "Порт");
			GUI.Label(new Rect((Screen.width - 100)/2 - 25, Screen.height/2-90, 100, 20), "Никнейм");
			nickname = GUI.TextField(new Rect((Screen.width - 100)/2+35, Screen.height/2-90, 100, 20), nickname);
			ip = GUI.TextField(new Rect((Screen.width - 100)/2+35, Screen.height/2-60, 100, 20), ip);
			port = GUI.TextField(new Rect((Screen.width - 100)/2+35, Screen.height/2-30, 50, 20), port);
			
			if(GUI.Button(new Rect((Screen.width - 110)/2, Screen.height/2, 110, 30), "Присоединиться"))
			{
		//		errorname = "";
	//			if (nickname == "")
//				{
				//	errorname = "Пустое имя";
			//	}	
		//		if (nickname != "" && FindByNickname(nickname))
	///			{
//					errorname = "Имя уже занято на этом сервере";
			//	}
		//		if (errorname == "")
	//			{		
				Network.Connect(ip, Convert.ToInt32(port));
				//help.tag = nickname;
	//			}
			}
			
			if(GUI.Button(new Rect((Screen.width - 110)/2, Screen.height/2 + 35, 110, 30), "Создать сервер"))
			{
				//errorname = "";
				//if (nickname != "")
				//{
					host = true;
					Network.InitializeServer(10, Convert.ToInt32(port), false);
					gameObject.transform.rigidbody.mass = 2;
					pMap.x = 0;
					pMap.y = 0;
					pMap.z = 0;
					qMap.x = 0;
					qMap.y = 0;
					qMap.z = 0;
					mGO = (GameObject)Network.Instantiate(map, pMap , qMap, team);
				//}
				//else
				//{
			//		errorname = "Пустое имя";
		//		}
			}
			
			if(GUI.Button(new Rect((Screen.width - 110)/2, Screen.height/2 + 70, 110, 30), "Персонаж"))
			{
					help.rigidbody.drag = 3;
			}
			
			if(GUI.Button(new Rect((Screen.width - 110)/2, Screen.height/2 + 105, 110, 30), "Выход"))
				Application.Quit();
		}
	}
}
	
	void Respawn() {
		CreatePlayer(respawn, team);
	}
	// Вызывается когда мы подключились к серверу
	void OnConnectedToServer () {
		inplay = false;
		help.rigidbody.mass = 2;
		connected = true;
		//NewPlayer(0);
		//CreatePlayer();
	}
	
	// Когда мы создали сервер
	void OnServerInitialized () {
		inplay = false;
		help.rigidbody.mass = 2;
		connected = true;
		//NewPlayer(0);
		//CreatePlayer();
	}
	
	void Inteam(int a) {
		if (a == 1) {
			respawn = GameObject.Find("Spawn_1");
		//	cou_t1 += 1;
			team = 1;
		}
		else {
			respawn = GameObject.Find("Spawn_2");
		//	cou_t2 += 1;
			team = 2;
		}
		inrespawn = true;
	}
	
	// Создание игрока
	void CreatePlayer (GameObject respawn, int team) {
		//var objectList = GameObject.FindGameObjectsWithTag("MyTag");
		//for (int i; i < objectList.size(); i++)
		//{
	//		objectList[i].networkView.RPC ("ApplyNick", RPCMode.All, objectList[i].GetComponent); 
//		}
		inplay = true;
		help.rigidbody.mass = 1;
		camera.enabled = false;
		//dopgo = (GameObject)Instantiate(PlayerPrefab, respawn.transform.position, respawn.transform.rotation);
		//dopgo.name = nickname;
		//dopgo = PlayerPrefab;
		//dopgo.name = nickname;
		//dopgo.GetComponent<Client>().nickname = "a";
		//if (networkView.isMine)
		//{
		//	dopgo.name = nickname;
		//}
		//dop2.transform.parent = PlayerPrefab.transform;
		camera.gameObject.GetComponent<AudioListener>().enabled = false;
		//ranspawnx = Random(4) - 2;
		//ranspawnz = Random(4) - 2;
		//dopgo = respawn;
		//dopgo.transform.position = transform.TransformDirection(Vector3 (ranspawnx, 0, ranspawnz));
		_go = (GameObject)Network.Instantiate(PlayerPrefab, respawn.transform.position, respawn.transform.rotation, team);
		//Destroy(dopgo);
		_go.transform.GetComponentInChildren<Camera>().camera.enabled = true;
		_go.transform.GetComponentInChildren<AudioListener>().enabled = true;
		_go.transform.GetComponent<NicnameMessager>().nick = nickname + "[" + team.ToString() + "]";
		_go.transform.GetComponent<MouseLook>().enabled = true;
		//_go.transform.GetComponent<H>().GetComponent<MouseLook>().enabled = true;
		//_go.transform.GetChild(1).transform.GetComponent<MouseLook>().enabled = true;
		_go.light.enabled = true;
		//_go.transform.GetComponent<NicnameMessager>().team = team;
		//networkView.RPC ("ApplyNick", RPCMode.All, nick); 
		//dop2 = (GameObject)Instantiate(dopgo, respawn.transform.position, respawn.transform.rotation);
		//dop2.name = nickname;
//		dop2 = (GameObject)Network.Instantiate(dopgo, respawn.transform.position, respawn.transform.rotation, team);
		//Destroy(dop2);
		//_go.transform.GetComponentInChildren<Client>().nickname = nickname;
		//dop2.transform.parent = _go.transform;
		//_go.GetComponent<Client>().nickname = nickname;
	}
	
	// При отключении от сервера
	void OnDisconnectedFromServer (NetworkDisconnection info) {
		connected = false;
		inplay = false;
		camera.enabled = true;
		camera.gameObject.GetComponent<AudioListener>().enabled = true;
		Application.LoadLevel(Application.loadedLevel);
	}
	
	// Вызывается каждый раз когда игрок отсоеденяется от сервера
	void OnPlayerDisconnected (NetworkPlayer pl) {
		Network.RemoveRPCs(pl);
		Network.DestroyPlayerObjects(pl);
	}
}
