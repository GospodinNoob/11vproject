﻿#pragma strict

var startTime = 0.0;
var timer = 0.0;
var waitTimer = 0.0;
var waitTime = 60 * 0.5;
var playTime = 60 * 1.0;
var g1 : GameObject;
var g2 : GameObject;
var f : boolean;
var ho : int;
var host : boolean;
var help : GameObject;
//var team1 : [GameObject] * 10;
//var team2 : [GameObject] * 10;

function Start () {
    var waitTime = 60 * 0.5;
	var playTime = 60 * 5.0;
	f = true;
	tf = true;
	help = GameObject.Find("Main Camera");
	ho = help.transform.rigidbody.mass;
	host = false;
	if (ho == 2)
	{
		host = true;
		startTime = Time.time;
	}
}

@RPC
function ApplyTime(t: float)
{
	timer = t;
	waitTimer = t;
}
@RPC
function ApplySt(t:float)
{
	startTime = t;
}

function EndRound()
{
//	var main =
//	if (main)
	{
		var team = GameObject.Find("Main Camera");
		team.transform.GetComponent(TeamMenu).EndGame(host);
	}
}

@RPC
function DestroyGate()
{
	f = false;
	g1 = GameObject.Find("Gate1");
	g2 = GameObject.Find("Gate2");
	g1.active = false;
	g2.active = false;
}

var tf : boolean;


function OnGUI () {
	if (host)
	{
//		timer = Time.time;
//		waitTimer = Time.time;
		networkView.RPC ("ApplyTime", RPCMode.All, (Time.time));
		networkView.RPC ("ApplySt", RPCMode.All, startTime);
	}
	if (Time.time - startTime < waitTime)
	{
		var min = Mathf.FloorToInt((waitTimer - startTime) / 60);
		GUI.Label(new Rect((Screen.width - 4)/2 - 1, 25, 1000, 1000), min.ToString() + ' : ' + ((waitTime - Mathf.FloorToInt(waitTimer - startTime)) % 60).ToString());
		//Debug.Log(Time.time - startTime);
		//Debug.Log(waitTime);
	}
	else
	if ((Time.time - startTime > waitTime) && (Time.time - startTime - waitTime < playTime))
	{
		if (f)
		{
			networkView.RPC ("DestroyGate", RPCMode.All);
		}
		min = Mathf.FloorToInt((timer - startTime - waitTime) / 60);
		GUI.Label(new Rect((Screen.width - 4)/2 - 1, 25, 1000, 1000), min.ToString() + ' : ' + ((playTime - Mathf.FloorToInt(timer - startTime - waitTime)) % 60).ToString());
	}
	else
	{
	 if (f)
	 {
	 	networkView.RPC ("DestroyGate", RPCMode.All);
	 }
	 if (Time.time - startTime - waitTime > playTime)
	 {
	 	//Debug.Log(0);
	 	min = Mathf.FloorToInt((timer - startTime - waitTime - playTime) / 60);
		GUI.Label(new Rect((Screen.width - 4)/2 - 1, 25, 1000, 1000), min.ToString() + ' : ' + ((Mathf.FloorToInt(timer - startTime - waitTime - playTime)) % 60).ToString());
	 	if (tf)
	 	{
	 		EndRound();
	 		tf = false;
	 	}
	 }
	}
}