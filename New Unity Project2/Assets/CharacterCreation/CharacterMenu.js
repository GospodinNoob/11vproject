﻿#pragma strict
import System.IO;
//import Math;
var level : int;
var experianse : int;
var gold : int;

@System.Serializable
class Item extends System.Object
{

	function equal(it : Item)
	{
		var f : boolean;
		f = true;
		if (this.slashingArmour != it.slashingArmour) { f = false;}
		if (this.hackingArmour != it.hackingArmour) { f = false;}
		if (this.crushingArmour != it.crushingArmour) { f = false;}
		if (this.fireResistance != it.fireResistance) { f = false;}
		if (this.frozenResistance != it.frozenResistance) { f = false;}
		if (this.poisonResistance != it.poisonResistance) { f = false;}
		if (this.darknessResistance != it.darknessResistance) { f = false;}
		if (this.lightResistance != it.lightResistance) { f = false;}
		if (this.natureResistance != it.natureResistance) { f = false;}
		if (this.shockResistance != it.shockResistance) { f = false;}
		if (this.bleedingResistance != it.bleedingResistance) { f = false;}
		if (this.wetPoint != it.wetPoint) { f = false;}
		if (this.bleedingPoint != it.bleedingPoint) { f = false;}
		if (this.healthRegen != it.healthRegen) { f = false;}
		if (this.manaRegen != it.manaRegen) { f = false;}
		if (this.maxHits != it.maxHits) { f = false;}
		if (this.maxHitsPers != it.maxHitsPers) { f = false;}
		if (this.maxMana != it.maxMana) { f = false;}
		if (this.maxManaPers != it.maxManaPers) { f = false;}
		if (this.experianse != it.experianse) { f = false;}
		if (this.forse != it.forse) { f = false;}
		if (this.forsePers != it.forsePers) { f = false;}
		if (this.lovk != it.lovk) { f = false;}
		if (this.lovkPers != it.lovkPers) { f = false;}
		if (this.telo != it.telo) { f = false;}
		if (this.teloPers != it.teloPers) { f = false;}
		if (this.intelect != it.intelect) { f = false;}
		if (this.intelectPer != it.intelectPer) { f = false;}
		if (this.mindForse != it.mindForse) { f = false;}
		if (this.mindForsePers != it.mindForsePers) { f = false;}
		if (this.manaDestr != it.manaDestr) { f = false;}
		if (this.manaDestrResistanse != it.manaDestrResistanse) { f = false;}
		if (this.speedAttack != it.speedAttack) { f = false;}
		if (this.speedAttackPers != it.speedAttackPers) { f = false;}
		if (this.theifHitResistance != it.theifHitResistance) { f = false;}
		if (this.theifManaResistance != it.theifManaResistance) { f = false;}	
		if (this.stamina != it.stamina) { f = false;}
		if (this.staminaPers != it.staminaPers) { f = false;}
		if (this.staminaRegen != it.staminaRegen) { f = false;}
		if (this.healthRegenPers != it.healthRegenPers) { f = false;}
		if (this.manaRegenPers != it.manaRegenPers) { f = false;}
		if (this.staminaRegenPers != it.staminaRegenPers) { f = false;}
		return f;
	}
	function clear()
	{
		this.orientation = 0;
		this.name = '';
	   	this.rarity = 0;
		this.type = 0;
		this.stamina = 0;
		this.staminaPers = 0;
		this.typeofmodel = 0;
		this.slashingArmour = 0;
		this.hackingArmour = 0;
		this.crushingArmour = 0;
		this.fireResistance = 0;
		this.frozenResistance = 0;
		this.poisonResistance = 0;
		this.darknessResistance = 0;
		this.lightResistance = 0;
		this.natureResistance = 0;
		this.shockResistance = 0;
		this.bleedingResistance = 0;
		this.fireDamage = 0;
		this.firePoint = 0;
		this.frozenDamage = 0;
		this.frozenPoint = 0;
		this.poisonDamage = 0;
		this.poisonPoint = 0;
		this.darknessDamage = 0;
		this.darknessPoint = 0;
		this.lightDamage = 0;
		this.natureDamage = 0;
		this.shockDamage = 0;
		this.shockPoint = 0;
		this.wetPoint = 0;
		this.bleedingPoint = 0;	
		this.slashDamage = 0;
		this.hackDamage = 0;
		this.crushDamage = 0;
		this.pound = 0;
		this.healthRegen = 0;
		this.manaRegen = 0;
		this.maxHits = 0;
		this.maxHitsPers = 0;
		this.maxMana = 0;
		this.maxManaPers = 0;
		this.experianse = 0;
		this.forse = 0;
		this.forsePers = 0;
		this.lovk = 0;
		this.lovkPers = 0;
		this.telo = 0;
		this.teloPers = 0;
		this.intelect = 0;
		this.intelectPer = 0;
		this.mindForse = 0;
		this.mindForsePers = 0;
		this.manaDestr = 0;
		this.manaDestrResistanse = 0;
		this.speedAttack = 0;
		this.speedAttackPers = 0;
		this.theifHits = 0;
		this.theifMana = 0;
		this.theifHitResistance = 0;
		this.theifManaResistance = 0;
		this.magicDamage = 0;
		this.staminaRegen = 0;
		this.healthRegenPers = 0;
		this.manaRegenPers = 0;
		this.staminaRegenPers = 0;
		this.numItem = 0;
	};
	function plus(it : Item) 
	{
		this.slashingArmour += it.slashingArmour;
		this.hackingArmour += it.hackingArmour;
		this.crushingArmour += it.crushingArmour;
		this.fireResistance += it.fireResistance;
		this.frozenResistance += it.frozenResistance;
		this.poisonResistance += it.poisonResistance;
		this.darknessResistance += it.darknessResistance;
		this.lightResistance += it.lightResistance;
		this.natureResistance += it.natureResistance;
		this.shockResistance += it.shockResistance;
		this.bleedingResistance += it.bleedingResistance;
		this.wetPoint += it.wetPoint;
		this.bleedingPoint += it.bleedingPoint;
		this.healthRegen += it.healthRegen;
		this.manaRegen += it.manaRegen;
		this.maxHits += it.maxHits;
		this.maxHitsPers += it.maxHitsPers;
		this.maxMana += it.maxMana;
		this.maxManaPers += it.maxManaPers;
		this.experianse += it.experianse;
		this.forse += it.forse;
		this.forsePers += it.forsePers;
		this.lovk += it.lovk;
		this.lovkPers += it.lovkPers;
		this.telo += it.telo;
		this.teloPers += it.teloPers;
		this.intelect += it.intelect;
		this.intelectPer += it.intelectPer;
		this.mindForse += it.mindForse;
		this.mindForsePers += it.mindForsePers;
		this.manaDestr += it.manaDestr;
		this.manaDestrResistanse += it.manaDestrResistanse;
		this.speedAttack += it.speedAttack;
		this.speedAttackPers += it.speedAttackPers;
		this.theifHitResistance += it.theifHitResistance;
		this.theifManaResistance += it.theifManaResistance;	
		this.stamina += it.stamina;
		this.staminaPers += it.staminaPers;
		this.staminaRegen += it.staminaRegen;
		this.healthRegenPers += it.healthRegenPers;
		this.manaRegenPers += it.manaRegenPers;
		this.staminaRegenPers += it.staminaRegenPers;
		this.damageForse += it.damageForse;
	};
	
	var numItem = 0;
	var orientation = 0;
	var name : String;
    var rarity = 0;
	var type = 0;
	var stamina = 0;
	var staminaPers = 0;
	var typeofmodel = 0;
	var slashingArmour = 0;
	var hackingArmour = 0;
	var crushingArmour = 0;
	var fireResistance = 0;
	var frozenResistance = 0;
	var poisonResistance = 0;
	var darknessResistance = 0;
	var lightResistance = 0;
	var natureResistance = 0;
	var shockResistance = 0;
	var bleedingResistance = 0;
	var fireDamage = 0;
	var firePoint = 0;
	var frozenDamage = 0;
	var frozenPoint = 0;
	var poisonDamage = 0;
	var poisonPoint = 0;
	var darknessDamage = 0;
	var darknessPoint = 0;
	var lightDamage = 0;
	var natureDamage = 0;
	var shockDamage = 0;
	var shockPoint = 0;
	var wetPoint = 0;
	var bleedingPoint = 0;
	var slashDamage = 0;
	var hackDamage = 0;
	var crushDamage = 0;
	var pound = 0.0;
	var healthRegen = 0;
	var manaRegen = 0;
	var maxHits = 0;
	var maxHitsPers = 0;
	var maxMana = 0;
	var maxManaPers = 0;
	var experianse = 0;
	var forse = 0;
	var forsePers = 0;
	var lovk = 0;
	var lovkPers = 0;
	var telo = 0;
	var teloPers = 0;
	var intelect = 0;
	var intelectPer = 0;
	var mindForse = 0;
	var mindForsePers = 0;
	var manaDestr = 0;
	var manaDestrResistanse = 0;
	var speedAttack = 0;
	var speedAttackPers = 0;
	var theifHits = 0;
	var theifMana = 0;
	var theifHitResistance = 0;
	var theifManaResistance = 0;
	var magicDamage = 0;
	var staminaRegen = 0;
	var healthRegenPers = 0;
	var manaRegenPers = 0;
	var staminaRegenPers = 0;
	var damageForse = 0;
}

@System.Serializable
class Items2 extends System.Object
{
	var It = new Item[11];
}

@System.Serializable
class Items extends System.Object
{
	var It = new Item[11];
}

//	rarity 0
//	type 1
//	texture 2
 //	slashingArmour 3
//	hackingArmour 4
//	crushingArmour 5
//	fireResistance 6
//	frozenResistance 7 
//	poisonResistance 8
//	darknessResistance 9 
//	lightResistance 10
//	natureResistance 11
//	shockResistance 12
///	bleedingResistance 13 
//	fireDamage 14
//	firePoint 15
//	frozenDamage 16 
//	frozenPoint 17
//	poisonDamage 18
//	poisonPoint 19
///	darknessDamage 20 
//	darknessPoint 21
//	lightDamage 22
//	natureDamage 23
//	shockDamage 24
//	shockPoint 25
//	wetPoint 26
//	bleedingPoint 27
//	slashDamage 28
//	hackDamage 29
//	crushDamage 30
//	pound 31
//	healthRegen 32
//	manaRegen 33
//	maxHits 34
//	maxHitsPers 35 
//	maxMana 36
//	maxManaPers 37
//	experianse 38
//	forse 39
//	forsePers 40
//	lovk 41
//	lovkPers 42
//	telo 43
//	teloPers 44
//	intelect 45
//	intelectPer 46
//	mindForse 47
//	mindForsePers 48
//	manaDestr 49
//	manaDestrReistanse 50
//	speedAttack 51
//	speedAttackPers 52
//	theifHits 53
//	theifMana 54
//	theifHitReistance 55
///	theifManaResitance 56
//  typeofmodel 57

//type of intems
// 0 - all
// 1 - microweapon 
// 2 - middleweapon
// 3 - maxweapon
// 4 - rings
@System.Serializable
class Rings extends System.Object
{
	var Ring = new Item[5];
}
var ringsLeft : Rings;
var ringsRight : Rings;
// 5 - amulets
class Amulets extends System.Object
{
	var Amul = new Item[6];
}
var amulets : Amulets;
// 6 - brosh

class Broshi2 extends System.Object
{
	var Br = new Item[9];
}

var brosh : Broshi2;
// 7 - perchatki
var leftPerch : Item;
var rightPerch : Item;
// 8 - handdown
var handDownLeft : Item;
var handDownRight : Item;
// 9 - handmiddle
var handMiddleLeft : Item;
var handMiddleRight : Item;
// 10 - handup
var handUpLeft : Item;
var handUpRight : Item;
// 11 - shoulder
var shoulderLeft : Item;
var shoulderRight : Item;
// 12 - rubaha
var rubaha : Item;
// 13 - pododcpeh
var poddocpeh : Item;
// 14 - upbodyarmour
var upBodyArmour : Item;
// 15 - middlebodyarmour
var middleBodyArmour : Item;
// 16 - downbodyarmour
var downBodyArmour : Item;
// 17 - legsup
var legUpLeft : Item;
var legUpRight : Item;
// 18 - legsmiddle
var legMiddleLeft : Item;
var legMiddleRight : Item;
// 19 - legsdown
var legDownLeft : Item;
var legDownRight : Item;
// 20 - sapog
var sapogLeft : Item;
var sapogRight : Item;
// 21 - poas
var poas : Item;
// 22 - plash
var plash : Item;
// 23 - helmet
var helmet : Item;
// 24 - underhelmet
var underHelmet : Item;
// 25 - ochki
var glaza : Item;
// 26 - chelust
var chelust : Item;
// 27 - godcowshoulder
var shoulderUpHandLeft : Item;
var shoulderUpHandRight : Item;
// 28 - upmiddlebody
var upMiddleBody : Item;
// 29 - body
var body : Item;
// 30 - middledownbody
var downMiddleBody : Item;
// 31 - perchatki+downhand
var perchDownHandLeft : Item;
var perchDownHandRight : Item;
// 32 - gorjet
var gorjet : Item;
var leftHandWeapon : Item;
var rightHandWeapon : Item;
var rightLegWeapon : Item;
var leftLegWeapon : Item;
var neckLeft : Item;
var neckRight : Item;
var neckMiddle1 : Item;
var neckMiddle2 : Item;
var weapons = 10; 

class PoasW3 extends System.Object
{
	var Rash = new Item[20];
}
var poasWeapon : PoasW3;
// 33
var shtani : Item;
// 34
var poddocpehLeg : Item;

//typeofmodels
// 0 - пусто
// 1 - нож
// 2 - катана
// 3 - геральдический щит
// 4 - cекира

@System.Serializable
class Hand2 extends System.Object
{
	var Han = new Item[10];
}

@System.Serializable
class Invent extends System.Object
{
	var Inv = new Items2[8];
}

@System.Serializable
class Invent2 extends System.Object
{
	var Inv = new Items[8];
}

var help : GameObject;
var winMenu : int = 1;
var winMenu2 : int = 1;
var equpMenu : int = 1;
var magicMenu : int = 1;
//var rightHand : Item[];
public var rightHand : Hand2;
public var leftHand : Hand2;
var inventory : Invent2;
var cursor : Item;

var emptyTexture : Texture2D;

var styles : GUIStyle[];
var inventoryButtonStyle : GUIStyle;

var weaponStyles = [1];
//1
var sekiraStyle : GUIStyle;
//2
var perchStyles = [2];
var perchStyle1 : GUIStyle;
//3
var sapogStyles = [3];
var sapogStyle1 : GUIStyle;
//4
var upBodyStyles = [4, 5];
var upBodyStyle1: GUIStyle;
//5
var upMiddleBodyStyle1: GUIStyle;
//6
var shoulderStyles = [6];
var shoulderStyle1: GUIStyle;
//7
var lokotStyles = [7];
var lokotStyle1: GUIStyle;
//8
var middleLegStyles = [8, 9];
var middleLegStyle1: GUIStyle;
//9

var middleLegStyle2 : GUIStyle;
//9
var downLegStyles = [10];
var downLegStyle1:GUIStyle;
//11
var upLegStyles = [11];
var upLegStyle1 : GUIStyle;
//12
var helmetStyles = [12, 43];
var helmetStyle1 : GUIStyle;
//13
var gorjetStyles = [13];
var gorjetStyle1 : GUIStyle;
//14
var narHandStyles = [14];
var narHandStyle1 : GUIStyle;
//15
var middleBodyStyles = [15];
var middleBodyStyle1 : GUIStyle;
//16
var downBodyStyles = [16];
var downBodyStyle1 : GUIStyle;
//17
var ringsStyles = [17, 18, 19, 20, 21, 22, 23, 24];
var ringStyle1 : GUIStyle;
//18
var ringStyle2 : GUIStyle;
//19
var ringStyle3 : GUIStyle;
//20
var ringStyle4 : GUIStyle;
//21
var ringStyle5 : GUIStyle;
//22
var ringStyle6 : GUIStyle;
//23
var ringStyle7 : GUIStyle;
//24
var ringStyle8 : GUIStyle;
//25
var amuletsStyles = [25, 26, 27, 28, 29, 30, 31, 32, 33, 34];
var amuletStyle1 : GUIStyle;
//26
var amuletStyle2 : GUIStyle;
//27
var amuletStyle3: GUIStyle;
//28
var amuletStyle4 : GUIStyle;
//29
var amuletStyle5 : GUIStyle;
//30
var amuletStyle6 : GUIStyle;
//31
var amuletStyle7 : GUIStyle;
//32
var amuletStyle8 : GUIStyle;
//33
var amuletStyle9 : GUIStyle;
//34
var amuletStyle10 : GUIStyle;

//34
var broshStyles = [35, 36, 37, 38];
var broshStyle1 : GUIStyle;
//35
var broshStyle2 : GUIStyle;
//36
var broshStyle3 : GUIStyle;
//37
var broshStyle4 : GUIStyle;
//38
var plashStyles = [39];
var plashStyle1 : GUIStyle;

var poddocpehLegStyles = [40];
var poddocpehLeg1 : GUIStyle;

var poddospehStyles = [41];
var poddospeh1 : GUIStyle;

var shtaniStyles = [42];
var shtani1 : GUIStyle;

var helmetStyle2 : GUIStyle;

var podshlemStyles = [44];
var podslemStyle1 : GUIStyle;

var poasStyles = [45];
var poasStyle1 : GUIStyle;

var rubahaStyles = [46];
var rubahaStyle1 : GUIStyle;

var maskStyles = [47];
var maskStyle1 : GUIStyle;

var glazaStyles = [48];
var glazStyle1 : GUIStyle;

var wallWeaonStyle : GUIStyle;
var wallNarStyle : GUIStyle;
var wallPonStyle : GUIStyle;
var wallLegMidStyle : GUIStyle;
var wallHandMidStyle : GUIStyle;
var wallPerchStyle : GUIStyle;
var wallSapogStyle : GUIStyle;
var wallRingStyle : GUIStyle;
var wallBroshStyle : GUIStyle;
var wallAmulStuyle : GUIStyle;
var wallShoulderStyle : GUIStyle;
var wallHelmetStyle : GUIStyle;
var wallUnderHeletStyle : GUIStyle;
var wallUnderArmou : GUIStyle;
var wallRubahaStyle : GUIStyle;
var wallStaniStyle : GUIStyle;
var wallUpArmourStyle : GUIStyle;
var wallMidArmourStyle : GUIStyle;
var wallDownArmourStyle : GUIStyle;
var wallPlashStyle : GUIStyle;
var wallPoasStyle : GUIStyle;
var wallGrogStyle : GUIStyle;
var wallGlazStyle : GUIStyle;
var wallChelustStyle : GUIStyle;
var wallPonDownStyle : GUIStyle;
var wallUnderArmourLeg : GUIStyle;
var itemBlockStyle : GUIStyle;
//function pushToItem (object : Array)
//{
//	object.Push(0);
//	object.Push(0);
//	object.Push(0);
//	for (var i = 3; i <= 57; i++)
//	{
//		object.Push(0);
//	}
//}

function SaveItem (item : Item)
{
	sw.WriteLine(item.name);
	sw.WriteLine(item.orientation);
	sw.WriteLine(item.rarity);
	sw.WriteLine(item.type);
	sw.WriteLine(item.typeofmodel);
	sw.WriteLine(item.slashingArmour);
	sw.WriteLine(item.hackingArmour);
	sw.WriteLine(item.crushingArmour);
	sw.WriteLine(item.fireResistance);
	sw.WriteLine(item.frozenResistance);
	sw.WriteLine(item.poisonResistance);
	sw.WriteLine(item.darknessResistance);
	sw.WriteLine(item.lightResistance);
	sw.WriteLine(item.natureResistance);
	sw.WriteLine(item.shockResistance);
	sw.WriteLine(item.bleedingResistance);
	sw.WriteLine(item.fireDamage);
	sw.WriteLine(item.firePoint);
	sw.WriteLine(item.frozenDamage);
	sw.WriteLine(item.frozenPoint);
	sw.WriteLine(item.poisonDamage);
	sw.WriteLine(item.poisonPoint);
	sw.WriteLine(item.darknessDamage);
	sw.WriteLine(item.darknessPoint);
	sw.WriteLine(item.lightDamage);
	sw.WriteLine(item.natureDamage);
	sw.WriteLine(item.shockDamage);
	sw.WriteLine(item.shockPoint);
	sw.WriteLine(item.wetPoint);
	sw.WriteLine(item.bleedingPoint);
	sw.WriteLine(item.slashDamage);
	sw.WriteLine(item.hackDamage);
	sw.WriteLine(item.crushDamage);
	sw.WriteLine(item.pound * 10);
	sw.WriteLine(item.healthRegen);
	sw.WriteLine(item.manaRegen);
	sw.WriteLine(item.maxHits);
	sw.WriteLine(item.maxHitsPers);
	sw.WriteLine(item.experianse);
	sw.WriteLine(item.forse);
	sw.WriteLine(item.forsePers);
	sw.WriteLine(item.lovk);
	sw.WriteLine(item.lovkPers);
	sw.WriteLine(item.telo);
	sw.WriteLine(item.teloPers);
	sw.WriteLine(item.intelect);
	sw.WriteLine(item.intelectPer);
	sw.WriteLine(item.mindForse);
	sw.WriteLine(item.mindForsePers);
	sw.WriteLine(item.manaDestr);
	sw.WriteLine(item.manaDestrResistanse);
	sw.WriteLine(item.speedAttack);
	sw.WriteLine(item.speedAttackPers);
	sw.WriteLine(item.theifHits);
	sw.WriteLine(item.theifMana);
	sw.WriteLine(item.theifHitResistance);
	sw.WriteLine(item.theifManaResistance);
	sw.WriteLine(item.magicDamage);
	sw.WriteLine(item.healthRegenPers);
	sw.WriteLine(item.manaRegenPers);
	sw.WriteLine(item.staminaRegenPers);
	sw.WriteLine(item.stamina);
	sw.WriteLine(item.staminaPers);
	sw.WriteLine(item.staminaRegen);
	sw.WriteLine(item.damageForse);
	sw.WriteLine(item.numItem);
}

function LoadItem (item : Item)
{
	item.name = streamReader.ReadLine();
	item.orientation = System.Convert.ToSingle(streamReader.ReadLine());
	item.rarity = System.Convert.ToSingle(streamReader.ReadLine());
	item.type = System.Convert.ToSingle(streamReader.ReadLine());
	item.typeofmodel = System.Convert.ToSingle(streamReader.ReadLine());
	item.slashingArmour = System.Convert.ToSingle(streamReader.ReadLine());
	item.hackingArmour = System.Convert.ToSingle(streamReader.ReadLine());
	item.crushingArmour = System.Convert.ToSingle(streamReader.ReadLine());
	item.fireResistance = System.Convert.ToSingle(streamReader.ReadLine());
	item.frozenResistance = System.Convert.ToSingle(streamReader.ReadLine());
	item.poisonResistance = System.Convert.ToSingle(streamReader.ReadLine());
	item.darknessResistance = System.Convert.ToSingle(streamReader.ReadLine());
	item.lightResistance = System.Convert.ToSingle(streamReader.ReadLine());
	item.natureResistance = System.Convert.ToSingle(streamReader.ReadLine());
	item.shockResistance = System.Convert.ToSingle(streamReader.ReadLine());
	item.bleedingResistance = System.Convert.ToSingle(streamReader.ReadLine());
	item.fireDamage = System.Convert.ToSingle(streamReader.ReadLine());
	item.firePoint = System.Convert.ToSingle(streamReader.ReadLine());
	item.frozenDamage = System.Convert.ToSingle(streamReader.ReadLine());
	item.frozenPoint = System.Convert.ToSingle(streamReader.ReadLine());
	item.poisonDamage = System.Convert.ToSingle(streamReader.ReadLine());
	item.poisonPoint = System.Convert.ToSingle(streamReader.ReadLine());
	item.darknessDamage = System.Convert.ToSingle(streamReader.ReadLine());
	item.darknessPoint = System.Convert.ToSingle(streamReader.ReadLine());
	item.lightDamage = System.Convert.ToSingle(streamReader.ReadLine());
	item.natureDamage = System.Convert.ToSingle(streamReader.ReadLine());
	item.shockDamage = System.Convert.ToSingle(streamReader.ReadLine());
	item.shockPoint = System.Convert.ToSingle(streamReader.ReadLine());
	item.wetPoint = System.Convert.ToSingle(streamReader.ReadLine());
	item.bleedingPoint = System.Convert.ToSingle(streamReader.ReadLine());
	item.slashDamage = System.Convert.ToSingle(streamReader.ReadLine());
	item.hackDamage = System.Convert.ToSingle(streamReader.ReadLine());
	item.crushDamage = System.Convert.ToSingle(streamReader.ReadLine());
	item.pound = System.Convert.ToSingle(streamReader.ReadLine()) / 10;
	item.healthRegen = System.Convert.ToSingle(streamReader.ReadLine());
	item.manaRegen = System.Convert.ToSingle(streamReader.ReadLine());
	item.maxHits = System.Convert.ToSingle(streamReader.ReadLine());
	item.maxHitsPers = System.Convert.ToSingle(streamReader.ReadLine());
	item.experianse = System.Convert.ToSingle(streamReader.ReadLine());
	item.forse = System.Convert.ToSingle(streamReader.ReadLine());
	item.forsePers = System.Convert.ToSingle(streamReader.ReadLine());
	item.lovk = System.Convert.ToSingle(streamReader.ReadLine());
	item.lovkPers = System.Convert.ToSingle(streamReader.ReadLine());
	item.telo = System.Convert.ToSingle(streamReader.ReadLine());
	item.teloPers = System.Convert.ToSingle(streamReader.ReadLine());
	item.intelect = System.Convert.ToSingle(streamReader.ReadLine());
	item.intelectPer = System.Convert.ToSingle(streamReader.ReadLine());
	item.mindForse = System.Convert.ToSingle(streamReader.ReadLine());
	item.mindForsePers = System.Convert.ToSingle(streamReader.ReadLine());
	item.manaDestr = System.Convert.ToSingle(streamReader.ReadLine());
	item.manaDestrResistanse = System.Convert.ToSingle(streamReader.ReadLine());
	item.speedAttack = System.Convert.ToSingle(streamReader.ReadLine());
	item.speedAttackPers = System.Convert.ToSingle(streamReader.ReadLine());
	item.theifHits = System.Convert.ToSingle(streamReader.ReadLine());
	item.theifMana = System.Convert.ToSingle(streamReader.ReadLine());
	item.theifHitResistance = System.Convert.ToSingle(streamReader.ReadLine());
	item.theifManaResistance = System.Convert.ToSingle(streamReader.ReadLine());
	item.magicDamage = System.Convert.ToSingle(streamReader.ReadLine());
	item.healthRegenPers = System.Convert.ToSingle(streamReader.ReadLine());
	item.manaRegenPers = System.Convert.ToSingle(streamReader.ReadLine());
	item.staminaRegenPers = System.Convert.ToSingle(streamReader.ReadLine());
	item.stamina = System.Convert.ToSingle(streamReader.ReadLine());
	item.staminaPers = System.Convert.ToSingle(streamReader.ReadLine());
	item.staminaRegen = System.Convert.ToSingle(streamReader.ReadLine());
	item.damageForse = System.Convert.ToSingle(streamReader.ReadLine());
	item.numItem = System.Convert.ToSingle(streamReader.ReadLine());
}

var fileName : String;
var sw : StreamWriter;

function Save()
{
	buffForse = 0;
	buffIntelect = 0;
	buffLovk = 0;
	buffTelo = 0;
	buffMindForse = 0;
	skillPoints = teorSkillPoint;
	fileName = "Data/Save/Save.sg";
	sw = System.IO.StreamWriter(fileName); 
	for (var i = 0; i < 10; i++)
	{
		SaveItem(leftHand.Han[i]);
	//	Debug.Log(leftHand.Han[i].typeofmodel);
	}
	for (var j = 0; j < 10; j++)
	{
		SaveItem(rightHand.Han[j]);
	}
	for (var k = 0; k < 8; k++)
	{
		for (var l = 0; l < 11; l++)
		{
			SaveItem(inventory.Inv[k].It[l]);
		}
	}
	for (var t = 0; t < 5; t++)
	{
		SaveItem(ringsLeft.Ring[t]);
	}
	for (var w = 0; w < 5; w++)
	{
		SaveItem(ringsRight.Ring[w]);
	}
	for (var o = 0; o < 6; o++)
	{
		SaveItem(amulets.Amul[o]);
	}
	for (var r = 0; r < 8; r++)
	{
		SaveItem(brosh.Br[r]);
	}
	SaveItem( leftPerch);
	SaveItem( rightPerch);
	SaveItem( handDownLeft);
	SaveItem( handDownRight);
	SaveItem( handMiddleLeft);
	SaveItem( handMiddleRight);
	SaveItem( handUpLeft);
	SaveItem( handUpRight);
	SaveItem( shoulderLeft);
	SaveItem( shoulderRight);
	SaveItem( rubaha);
	SaveItem( poas);
	SaveItem( plash);
	SaveItem( legDownLeft);
	SaveItem( legDownRight);
	SaveItem( legMiddleLeft);
	SaveItem( legMiddleRight);
	SaveItem( legUpLeft);
	SaveItem( legUpRight);
	SaveItem( sapogLeft);
	SaveItem( sapogRight);
	SaveItem( poddocpeh);
	SaveItem( poddocpehLeg);
	SaveItem( helmet);
	SaveItem( underHelmet);
	SaveItem( gorjet);
	SaveItem( upBodyArmour);
	SaveItem( middleBodyArmour);
	SaveItem( downBodyArmour);
	SaveItem( glaza);
	SaveItem( chelust);
	for (var b = 0; b < 10; b++)
	{
		SaveItem( poasWeapon.Rash[b]);
	}
	SaveItem( leftHandWeapon);
	SaveItem( rightHandWeapon);
	SaveItem( neckLeft);
	SaveItem( neckRight);
	SaveItem( neckMiddle1);
	SaveItem( neckMiddle2);
	for (var y = 0; y < 11; y++)
	{
		sw.WriteLine( activeActiveWeapon[y]);
	}
	SaveItem( buffs);
	SaveItem( PersonCharacteristic);
	Total();
	SaveItem( totalCharacteristic);
	sw.WriteLine( teorSkillPoint);
	sw.WriteLine(level);
	sw.WriteLine(experianse);
	sw.WriteLine(gold);
    sw.Close(); 
    Load();
}

var streamReader : StreamReader;

function Load()
{
	if(System.IO.File.Exists("Data/Save/Save.sg")) 
	{
	streamReader = new StreamReader("Data/Save/Save.sg");
	for (var i = 0; i < 10; i++)
	{
		LoadItem(leftHand.Han[i]);
	//	Debug.Log(leftHand.Han[i].typeofmodel);
	}
	for (var j = 0; j < 10; j++)
	{
		LoadItem(rightHand.Han[j]);
	}
	for (var k = 0; k < 8; k++)
	{
		for (var l = 0; l < 11; l++)
		{
			LoadItem(inventory.Inv[k].It[l]);
		}
	}
	for (var t = 0; t < 5; t++)
	{
		LoadItem(ringsLeft.Ring[t]);
	}
	for (var w = 0; w < 5; w++)
	{
		LoadItem(ringsRight.Ring[w]);
	}
	for (var o = 0; o < 6; o++)
	{
		LoadItem(amulets.Amul[o]);
	}
	for (var r = 0; r < 8; r++)
	{
		LoadItem(brosh.Br[r]);
	}
	LoadItem( leftPerch);
	LoadItem( rightPerch);
	LoadItem( handDownLeft);
	LoadItem( handDownRight);
	LoadItem( handMiddleLeft);
	LoadItem( handMiddleRight);
	LoadItem( handUpLeft);
	LoadItem( handUpRight);
	LoadItem( shoulderLeft);
	LoadItem( shoulderRight);
	LoadItem( rubaha);
	LoadItem( poas);
	LoadItem( plash);
	LoadItem( legDownLeft);
	LoadItem( legDownRight);
	LoadItem( legMiddleLeft);
	LoadItem( legMiddleRight);
	LoadItem( legUpLeft);
	LoadItem( legUpRight);
	LoadItem( sapogLeft);
	LoadItem( sapogRight);
	LoadItem( poddocpeh);
	LoadItem( poddocpehLeg);
	LoadItem( helmet);
	LoadItem( underHelmet);
	LoadItem( gorjet);
	LoadItem( upBodyArmour);
	LoadItem( middleBodyArmour);
	LoadItem( downBodyArmour);
	LoadItem( glaza);
	LoadItem( chelust);
	for (var b = 0; b < 10; b++)
	{
		LoadItem( poasWeapon.Rash[b]);
	}
	LoadItem( leftHandWeapon);
	LoadItem( rightHandWeapon);
	LoadItem( neckLeft);
	LoadItem( neckRight);
	LoadItem( neckMiddle1);
	LoadItem( neckMiddle2);
	for (var y = 0; y < 11; y++)
	{
		activeActiveWeapon[y] = System.Convert.ToInt16(streamReader.ReadLine());
	}
	LoadItem( buffs);
	LoadItem( PersonCharacteristic);
	LoadItem( totalCharacteristic);
	teorSkillPoint = System.Convert.ToSingle(streamReader.ReadLine());
	level = System.Convert.ToSingle(streamReader.ReadLine());
	experianse = System.Convert.ToSingle(streamReader.ReadLine());
	gold = System.Convert.ToSingle(streamReader.ReadLine());
	skillPoints = teorSkillPoint;
	streamReader.Close();
	Total();
	}
	else
	{
		gold = 0;
		experianse = 0;
		level = 0;
	}
}

function getRandomInt(min : int, max : int)
{
  return Mathf.Floor(Random.Range(min, max));
}

var WallParametres : Texture2D;

@System.Serializable
class StrMas extends System.Object
{
	var St = new String[100];
}

var array : StrMas;
var FieldTextStyle : GUIStyle;

function ShowParametres(item : Item)
{
	if (item.typeofmodel != 0)
	{
		var i = 0;
		var max = 0;
		var dopper = 0;
		array.St[i] = item.name;
		i += 1;
		max = item.name.Length;
		dopper = item.rarity;
		if(item.name != "")
		{
			//var s : string;
			switch (dopper)
			{
				case 0 : array.St[i] = "Качество: Ужасный"; break;
				case 1 : array.St[i] = "Качество: Обычный"; break;
				case 2 : array.St[i] = "Качество: Редкий"; break;
				case 3 : array.St[i] = "Качество: Уникальный"; break;
				case 4 : array.St[i] = "Качество: Эпический"; break;
				case 5 : array.St[i] = "Качество: Легендарный"; break;
			}			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.pound;
		if(dopper != 0)
		{
			array.St[i] = "Вес: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.orientation;
		if(dopper != -1)
		{
			if ((item.type == 23) || (item.type == 26))
			{
				if (dopper == 0)
				{
					array.St[i] = "Открытый";
				}
				else
				{
					array.St[i] = "Закрытый";
				}
			}
			else
			{
				if (dopper == 0)
				{
					array.St[i] = "Правый";
				}
				else
				{
					array.St[i] = "Левый";
				}
			}
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.slashingArmour;
		if(dopper != 0)
		{
			array.St[i] = "Защита от режущего оружия: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.hackingArmour;
		if(dopper != 0)
		{
			array.St[i] = "Защита от рубящего оружия: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.crushingArmour;
		if(dopper != 0)
		{
			array.St[i] = "Защита от дробящего оружия: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.fireResistance;
		if(dopper != 0)
		{
			array.St[i] = "Сопр огню: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.frozenResistance;
		if(dopper != 0)
		{
			array.St[i] = "Сопр холоду: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.poisonResistance;
		if(dopper != 0)
		{
			array.St[i] = "Сопр ядам и кислотам: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.darknessResistance;
		if(dopper != 0)
		{
			array.St[i] = "Сопр тьме: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.lightResistance;
		if(dopper != 0)
		{
			array.St[i] = "Сопр свету: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.natureResistance;
		if(dopper != 0)
		{
			array.St[i] = "Сопр природе: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.shockResistance;
		if(dopper != 0)
		{
			array.St[i] = "Сопр электричеству: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.bleedingResistance;
		if(dopper != 0)
		{
			array.St[i] = "Сопр кровотечению: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.fireDamage;
		if(dopper != 0)
		{
			array.St[i] = "Урон огнём: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.firePoint;
		if(dopper != 0)
		{
			array.St[i] = "Воспламенение: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.frozenDamage;
		if(dopper != 0)
		{
			array.St[i] = "Урон холодом" + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.frozenPoint;
		if(dopper != 0)
		{
			array.St[i] = "Обморожение" + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.poisonDamage;
		if(dopper != 0)
		{
			array.St[i] = "Урон кислотой: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.poisonPoint;
		if(dopper != 0)
		{
			array.St[i] = "Разъедание: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.darknessDamage;
		if(dopper != 0)
		{
			array.St[i] = "Урон тьмой: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.darknessPoint;
		if(dopper != 0)
		{
			array.St[i] = "Очки тьмы: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.lightDamage;
		if(dopper != 0)
		{
			array.St[i] = "Урон светом: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.natureDamage;
		if(dopper != 0)
		{
			array.St[i] = "Урон природой: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.shockDamage;
		if(dopper != 0)
		{
			array.St[i] = "Урон электричеством: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.shockPoint;
		if(dopper != 0)
		{
			array.St[i] = "Статическое напряжение: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.wetPoint;
		if(dopper != 0)
		{
			array.St[i] = "Промокание: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.bleedingPoint;
		if(dopper != 0)
		{
			array.St[i] = "Кровотечение: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.slashDamage;
		if(dopper != 0)
		{
			array.St[i] = "Режущий урон: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.hackDamage;
		if(dopper != 0)
		{
			array.St[i] = "Рубящий урон: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.crushDamage;
		if(dopper != 0)
		{
			array.St[i] = "Дробящий урон: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.healthRegen;
		if(dopper != 0)
		{
			array.St[i] = "Регенерация здоровья: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.manaRegen;
		if(dopper != 0)
		{
			array.St[i] = "Регенерация маны: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.maxHits;
		if(dopper != 0)
		{
			array.St[i] = "Здоровье: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.maxManaPers;
		if(dopper != 0)
		{
			array.St[i] = "Мана: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.maxMana;
		if(dopper != 0)
		{
			array.St[i] = "Мана: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.maxHitsPers;
		if(dopper != 0)
		{
			array.St[i] = "Здоровье: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.experianse;
		if(dopper != 0)
		{
			array.St[i] = "Опыт: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.forse;
		if(dopper != 0)
		{
			array.St[i] = "Сила: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.forsePers;
		if(dopper != 0)
		{
			array.St[i] = "Сила: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.lovk;
		if(dopper != 0)
		{
			array.St[i] = "Ловкость: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.lovkPers;
		if(dopper != 0)
		{
			array.St[i] = "Ловкость: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.telo;
		if(dopper != 0)
		{
			array.St[i] = "Выносливость: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.teloPers;
		if(dopper != 0)
		{
			array.St[i] = "Вносливость: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.intelect;
		if(dopper != 0)
		{
			array.St[i] = "Интелект: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.intelectPer;
		if(dopper != 0)
		{
			array.St[i] = "Интелект: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.mindForse;
		if(dopper != 0)
		{
			array.St[i] = "Сила воли: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.mindForsePers;
		if(dopper != 0)
		{
			array.St[i] = "Сила воли: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.manaDestr;
		if(dopper != 0)
		{
			array.St[i] = "Сгорание маны: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.manaDestrResistanse;
		if(dopper != 0)
		{
			array.St[i] = "Сопр сжиганию маны: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.speedAttack;
		if(dopper != 0)
		{
			array.St[i] = "Скорость: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.speedAttackPers;
		if(dopper != 0)
		{
			array.St[i] = "Скорость: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.theifHits;
		if(dopper != 0)
		{
			array.St[i] = "Кража здоровья: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.theifMana;
		if(dopper != 0)
		{
			array.St[i] = "Кража маны: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.theifHitResistance;
		if(dopper != 0)
		{
			array.St[i] = "Сопр краже жизни: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.theifManaResistance;
		if(dopper != 0)
		{
			array.St[i] = "Сопр краже маны: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.stamina;
		if(dopper != 0)
		{
			array.St[i] = "Выносливость: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.staminaPers;
		if(dopper != 0)
		{
			array.St[i] = "Выносливость: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.staminaRegen;
		if(dopper != 0)
		{
			array.St[i] = "Регенерация выносливости: " + dopper.ToString();
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.magicDamage;
		if(dopper != 0)
		{
			array.St[i] = "Сила магии: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.healthRegenPers;
		if(dopper != 0)
		{
			array.St[i] = "Регенеация жизни: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.manaRegenPers;
		if(dopper != 0)
		{
			array.St[i] = "Регенеация маны: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.staminaRegenPers;
		if(dopper != 0)
		{
			array.St[i] = "Регенерация вносливости: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		dopper = item.damageForse;
		if(dopper != 0)
		{
			array.St[i] = "Увеличение урона: " + dopper.ToString() + "%";
			
			max = Mathf.Max(max, array.St[i].Length); i += 1;
		}
		GUI.DrawTexture(new Rect(Input.mousePosition.x, Screen.height - Input.mousePosition.y, max * 7, i * 15), WallParametres);
		for(var j = 0; j <= i; j++)
		{
//			GUI.Label(new Rect(Input.mousePosition.x, Screen.height - Input.mousePosition.y + j * 15, max, i * 15));//, array.St[j]);
			GUI.Label(new Rect(Input.mousePosition.x, Screen.height - Input.mousePosition.y + j * 15, max * 7, i * 15), array.St[j], FieldTextStyle);
			array.St[j] = '';
		}
		i = 0;
	}	
}

public static var personHits : int;
public static var personHRegen : int;

public var totalCharacteristic : Item;

var teorSkillPoint : int;

var buffForse : int;
var buffLovk : int;
var buffIntelect : int;
var buffMindForse : int;
var buffTelo : int;
var middleArmourTF : boolean;
var downArmourTF : boolean;
var maskTF : boolean;


function Total()
{
	PlayerDefaultStamina = 10;
	PlayerDefaultStaminaRegen = 2;
	PlayerHealthDefault = 10;
	totalCharacteristic.forse = Mathf.Round((PersonCharacteristic.forse + buffs.forse + buffForse) * (1 + 0.01 * buffs.forsePers));
	totalCharacteristic.lovk = Mathf.Round((PersonCharacteristic.lovk + buffs.lovk + buffLovk) * (1 + 0.01 * buffs.lovkPers));
	totalCharacteristic.intelect = Mathf.Round((PersonCharacteristic.intelect + buffs.intelect + buffIntelect) * (1 + 0.01 * buffs.intelectPer));
	totalCharacteristic.mindForse = Mathf.Round((PersonCharacteristic.mindForse + buffs.mindForse + buffMindForse) * (1 + 0.01 * buffs.mindForsePers));
	totalCharacteristic.telo = Mathf.Round((PersonCharacteristic.telo + buffs.telo + buffTelo) * (1 + 0.01 * buffs.telo));
	totalCharacteristic.stamina = Mathf.Round((PlayerDefaultStamina + PersonCharacteristic.stamina + buffs.stamina + 0.05 * (totalCharacteristic.forse + totalCharacteristic.lovk) + totalCharacteristic.telo) * (1 + 0.01 * buffs.staminaPers));
	totalCharacteristic.maxHits = Mathf.Round((PlayerHealthDefault + PersonCharacteristic.maxHits + totalCharacteristic.telo + buffs.maxHits + 0.05 * (totalCharacteristic.forse)) * (1 + 0.01 * buffs.maxHitsPers));
	totalCharacteristic.maxMana = Mathf.Round((PersonCharacteristic.maxMana + buffs.maxMana + 0.05 * (totalCharacteristic.intelect) + totalCharacteristic.mindForse) * (1 + 0.01 * buffs.maxManaPers));
	totalCharacteristic.manaRegen = Mathf.Round( PersonCharacteristic.manaRegen + buffs.manaRegen + 0.05 * (totalCharacteristic.intelect) + 0.01 * totalCharacteristic.mindForse) * (1 + 0.01 *buffs.manaRegenPers);
	totalCharacteristic.staminaRegen = Mathf.Round((PlayerDefaultStaminaRegen + PersonCharacteristic.staminaRegen + buffs.staminaRegen + 0.05 * totalCharacteristic.telo) * (1 + 0.01 *buffs.staminaRegenPers));
	totalCharacteristic.healthRegen = Mathf.Round(( PersonCharacteristic.healthRegen + buffs.healthRegen + 0.02 * totalCharacteristic.telo) * buffs.healthRegenPers);
	totalCharacteristic.fireResistance = Mathf.Round( PersonCharacteristic.fireResistance + buffs.fireResistance + 0.33333 * totalCharacteristic.telo);
	totalCharacteristic.frozenResistance = Mathf.Round( PersonCharacteristic.frozenResistance + buffs.frozenResistance + 0.33333 * totalCharacteristic.telo);
	totalCharacteristic.poisonResistance = Mathf.Round( PersonCharacteristic.poisonResistance + buffs.poisonResistance + 0.33333 * totalCharacteristic.telo);
	totalCharacteristic.bleedingResistance = Mathf.Round( PersonCharacteristic.bleedingResistance + buffs.bleedingResistance + 0.33333 * totalCharacteristic.telo);
	totalCharacteristic.lightResistance = Mathf.Round( PersonCharacteristic.lightResistance + buffs.lightResistance + 0.33333 * totalCharacteristic.telo);
	totalCharacteristic.darknessResistance = Mathf.Round( PersonCharacteristic.darknessResistance + buffs.darknessResistance + 0.33333 * totalCharacteristic.telo);
	totalCharacteristic.natureResistance = Mathf.Round( PersonCharacteristic.natureResistance + buffs.natureResistance + 0.33333 * totalCharacteristic.telo);
	totalCharacteristic.shockResistance = Mathf.Round( PersonCharacteristic.shockResistance + buffs.shockResistance + 0.33333 * totalCharacteristic.telo);
	totalCharacteristic.magicDamage = Mathf.Round( PersonCharacteristic.magicDamage + buffs.magicDamage + 0.1 * totalCharacteristic.intelect + 0.01 * totalCharacteristic.mindForse);
	totalCharacteristic.speedAttackPers = Mathf.Round( buffs.speedAttackPers + 0.3 * totalCharacteristic.lovk);
	totalCharacteristic.damageForse = Mathf.Round(0.3 * totalCharacteristic.forse) + buffs.damageForse;
	
}


public function GenerateItem()
{
	Debug.Log(1);
	var it : int;
	var jt : int;
	var f = false;
	it = 0;
	jt = 0;
	var i : int;
	var j : int;
	//inventory.Inv[0].It[0].typeofmodel = 0;
	for(it = 0; it < 8; it++)
	{
		if (f)
		{
			break;
		}
		for(jt = 0; jt < 11; jt++)
		{
			if (inventory.Inv[it].It[jt].typeofmodel == 0)
			{
				f = true;
				i = it;
				j = jt;
				break;
			}
		}
	}
	//inventory.Inv[i].It[j].maxHits = 1;
	var genType = getRandomInt(-1, 101);
	var genTypeItem : int;
	var modifictaors : int;
	if (genType < 20)
	{
		genTypeItem = 0;
		modifictaors = 0;
	}
	else if (genType < 40)
	{
		genTypeItem = 1;
		modifictaors = 0;
	}
	else if (genType < 80)
	{
		genTypeItem = 2;
		modifictaors = getRandomInt(1, 3);
	}
	else if (genType < 90)
	{
		genTypeItem = 3;
		modifictaors = getRandomInt(2, 4);
	}
	else if (genType < 98)
	{
		genTypeItem = 4;
		modifictaors = getRandomInt(3, 5);
	}
	else
	{
		genTypeItem = 5;
		modifictaors = getRandomInt(4, 9);
	}
	inventory.Inv[i].It[j].rarity = genTypeItem;	
	var genModel = getRandomInt(1, 27);
	var itemNum : int;
	switch (genModel)
	{
		case 1:		
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].hackDamage = Mathf.Round(18 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushDamage = Mathf.Round(18 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;
					inventory.Inv[i].It[j].speedAttack = 2;
					inventory.Inv[i].It[j].type = 2;
					inventory.Inv[i].It[j].typeofmodel = weaponStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Секира";
					break;
			}
			break;
		case 2:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 0.3;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = getRandomInt(0, 2);
					inventory.Inv[i].It[j].type = 7;
					inventory.Inv[i].It[j].typeofmodel = perchStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Латная перчатка";
					break;
			}
			break;
		case 3:
			itemNum = getRandomInt(1, 3);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(15 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(9 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(11 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 6;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 14;
					inventory.Inv[i].It[j].typeofmodel = upBodyStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Стальной нагрудник";
					break;
				case 2:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(21 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(12 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(13 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 9;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 28;
					inventory.Inv[i].It[j].typeofmodel = upBodyStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Стальной доспех";
					break;
			}
			break;
		case 4:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(6 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(3 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 2;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = getRandomInt(0, 2);
					inventory.Inv[i].It[j].type = 11;
					inventory.Inv[i].It[j].typeofmodel = shoulderStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Стальной наплечник";
					break;
			}
			break;
		case 5:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(1 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 0.3;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = getRandomInt(0, 2);
					inventory.Inv[i].It[j].type = 9;
					inventory.Inv[i].It[j].typeofmodel = lokotStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Cтальной налокотник";
					break;
			}
			break;
		case 6:
			itemNum = getRandomInt(1, 3);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(3 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 0.3;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = getRandomInt(0, 2);
					inventory.Inv[i].It[j].type = 18;
					inventory.Inv[i].It[j].typeofmodel = middleLegStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Стальной наколленник";
					break;
				case 2:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(7 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(3 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 0.3;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = getRandomInt(0, 2);
					inventory.Inv[i].It[j].type = 18;
					inventory.Inv[i].It[j].typeofmodel = middleLegStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Стальной наколленник";
					break;
			}
			break;
		case 7:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(15 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(7 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 2;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = getRandomInt(0, 2);
					inventory.Inv[i].It[j].type = 19;
					inventory.Inv[i].It[j].typeofmodel = downLegStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Стальной наголенник";
					break;
				}
			break;
		case 8:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(17 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(6 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(7 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 2;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = getRandomInt(0, 2);
					inventory.Inv[i].It[j].type = 17;
					inventory.Inv[i].It[j].typeofmodel = upLegStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Стальной набедренник";
					break;
			}
			break;
		case 9:
			itemNum = getRandomInt(1, 3);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(13 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 3;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = 1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 23;
					inventory.Inv[i].It[j].typeofmodel = helmetStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Стальной шлем";
					break;
				case 2:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(1 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(1 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 0.3;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = 0;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 23;
					inventory.Inv[i].It[j].typeofmodel = helmetStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Шапка ушанка";
					break;
			}
			break;
		case 11:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(7 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(3 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 1;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 32;
					inventory.Inv[i].It[j].typeofmodel = gorjetStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Стальной горжет";
					break;
			}
			break;
		case 12:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(3 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 1;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 8;
					inventory.Inv[i].It[j].typeofmodel = narHandStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Пластнчатый наруч";
					break;
			}
			break;
		case 13:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(3 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 1;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 15;
					inventory.Inv[i].It[j].typeofmodel = middleBodyStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Кожаный поясной доспех";
					break;
			}
			break;
		case 14:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(3 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 1;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 16;
					inventory.Inv[i].It[j].typeofmodel = downBodyStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Кожанный гессан";
					break;
			}
			break;
		case 15:
			itemNum = getRandomInt(1, 9);
			inventory.Inv[i].It[j].pound = 0;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
			inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
			inventory.Inv[i].It[j].type = 4;
			inventory.Inv[i].It[j].typeofmodel = ringsStyles[itemNum - 1];
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].name = "Золотое кольцо с бирюзой";
					break;
				case 2:
					inventory.Inv[i].It[j].name = "Серебряное инкрустированное кольцо";
					break;
				case 3:
					inventory.Inv[i].It[j].name = "Серебряное кольцо с аметистом";
					break;
				case 4:
					inventory.Inv[i].It[j].name = "Богато инкрустированное кольцо с рубинами";
					break;
				case 5:
					inventory.Inv[i].It[j].name = "Серебряное кольцо с рубином";
					break;
				case 6:
					inventory.Inv[i].It[j].name = "Золотое кольцо";
					break;
				case 7:
					inventory.Inv[i].It[j].name = "Перстень с изумрудом";
					break;
				case 8:
					inventory.Inv[i].It[j].name = "Серебряное кольцо с гравировкой";
					break;		
			}
			break;
		case 16:
			itemNum = getRandomInt(1, 11);
			inventory.Inv[i].It[j].pound = 0;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
			inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
			inventory.Inv[i].It[j].type = 5;
			inventory.Inv[i].It[j].typeofmodel = amuletsStyles[itemNum - 1];
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].name = "Кулон с алмазом";
					break;
				case 2:
					inventory.Inv[i].It[j].name = "Кулон с сапфиром";
					break;
				case 3:
					inventory.Inv[i].It[j].name = "Кристаллическая подвеска";
					break;
				case 4:
					inventory.Inv[i].It[j].name = "Кулон с аметистом";
					break;
				case 5:
					inventory.Inv[i].It[j].name = "Кулон 'Голова волка'";
					break;
				case 6:
					inventory.Inv[i].It[j].name = "Крест";
					break;
				case 7:
					inventory.Inv[i].It[j].name = "Крест Анх";
					break;
				case 8:
					inventory.Inv[i].It[j].name = "Изумрудное ожерелье";
					break;
				case 9:
					inventory.Inv[i].It[j].name = "Богатое аметистовое ожерелье";
					break;
				case 10:
					inventory.Inv[i].It[j].name = "Жемчужное ожерелье";
					break;			
			}
			break;
		case 17:
			itemNum = getRandomInt(1, 5);
			inventory.Inv[i].It[j].pound = 0;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
			inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
			inventory.Inv[i].It[j].type = 6;
			inventory.Inv[i].It[j].typeofmodel = broshStyles[itemNum - 1];
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].name = "Наградный орден";
					break;
				case 2:
					inventory.Inv[i].It[j].name = "Медная брошь";
					break;
				case 3:
					inventory.Inv[i].It[j].name = "Брошь с сапфиром";
					break;
				case 4:
					inventory.Inv[i].It[j].name = "Медаль";
					break;
			}
			break;
		case 18:
			itemNum = getRandomInt(1, 1);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 1;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 22;
					inventory.Inv[i].It[j].typeofmodel = plashStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Раздвоенный плащ";
					break;
			}
			break;
		case 19:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(1 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 1;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 34;
					inventory.Inv[i].It[j].typeofmodel = poddocpehLegStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Стёганые поножи";
					break;
			}
			break;
		case 20:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(11 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 4;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 13;
					inventory.Inv[i].It[j].typeofmodel = poddospehStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Кольчуга";
					break;
			}
			break;
		case 21:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(1 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 0.1;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 33;
					inventory.Inv[i].It[j].typeofmodel = shtaniStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Чёрные шаровары";
					break;
			}
			break;
		case 22:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(2 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].crushingArmour = Mathf.Round(1 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].hackingArmour = Mathf.Round(1 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 0;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 24;
					inventory.Inv[i].It[j].typeofmodel = podshlemStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Стёганый подшлем";
					break;
			}
			break;
		case 23:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(1 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 0;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 21;
					inventory.Inv[i].It[j].typeofmodel = poasStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Армейский пояс";
					break;
			}
			break;
		case 24:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(1 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 0;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 12;
					inventory.Inv[i].It[j].typeofmodel = rubahaStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Расписная рубаха";
					break;
			}
			break;
		case 25:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					inventory.Inv[i].It[j].slashingArmour = Mathf.Round(1 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 0;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = 1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 26;
					inventory.Inv[i].It[j].typeofmodel = maskStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Маска";
					break;
			}
			break;
		case 26:
			itemNum = getRandomInt(1, 2);
			inventory.Inv[i].It[j].numItem = itemNum;
			switch (itemNum)
			{
				case 1:
					//inventory.Inv[i].It[j].slashingArmour = Mathf.Round(1 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100));
					inventory.Inv[i].It[j].pound = 0;//Mathf.Round(3 * getRandomInt(79, 121) / 100);
					inventory.Inv[i].It[j].orientation = -1;//getRandomInt(0, 1);
					inventory.Inv[i].It[j].type = 25;
					inventory.Inv[i].It[j].typeofmodel = glazaStyles[itemNum - 1];
					inventory.Inv[i].It[j].name = "Монокль";
					break;
			}
			break;
		}	
		if (genModel == 1)
		{
		for(var k = 0; k < modifictaors; k++)
		{
			var typMod = getRandomInt(0, 54);
			switch (typMod)
			{
				case 1: inventory.Inv[i].It[j].fireResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 2: inventory.Inv[i].It[j].frozenResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 3: inventory.Inv[i].It[j].poisonResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 4: inventory.Inv[i].It[j].darknessResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 5: inventory.Inv[i].It[j].lightResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 6:	inventory.Inv[i].It[j].natureResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 7: inventory.Inv[i].It[j].shockResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 8: inventory.Inv[i].It[j].bleedingResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 9: if ((inventory.Inv[i].It[j].wetPoint == 0) && (inventory.Inv[i].It[j].firePoint == 0) && (inventory.Inv[i].It[j].frozenDamage == 0) && (inventory.Inv[i].It[j].frozenPoint == 0))
						{
							inventory.Inv[i].It[j].fireDamage = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						}
						break;
				case 10: if ((inventory.Inv[i].It[j].wetPoint == 0) && (inventory.Inv[i].It[j].fireDamage == 0) && (inventory.Inv[i].It[j].frozenDamage == 0)  && (inventory.Inv[i].It[j].frozenPoint == 0))
						 {
					  		inventory.Inv[i].It[j].firePoint = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 11: if ((inventory.Inv[i].It[j].firePoint == 0) && (inventory.Inv[i].It[j].fireDamage == 0) && (inventory.Inv[i].It[j].frozenPoint == 0))
						 {
					  		inventory.Inv[i].It[j].frozenDamage = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 12: if ((inventory.Inv[i].It[j].firePoint == 0) && (inventory.Inv[i].It[j].fireDamage == 0) && (inventory.Inv[i].It[j].frozenDamage == 0))
						 {
					  		inventory.Inv[i].It[j].frozenPoint = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 13: if (inventory.Inv[i].It[j].poisonPoint == 0)
						 {
					  		inventory.Inv[i].It[j].poisonDamage = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 14: if (inventory.Inv[i].It[j].poisonDamage == 0)
						 {
					  		inventory.Inv[i].It[j].poisonPoint = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 15: if (inventory.Inv[i].It[j].darknessPoint == 0)
						 {
					  		inventory.Inv[i].It[j].darknessDamage = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 16: if (inventory.Inv[i].It[j].darknessDamage == 0)
						 {
					  		inventory.Inv[i].It[j].darknessPoint = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 17: inventory.Inv[i].It[j].lightDamage = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 18: inventory.Inv[i].It[j].natureDamage = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 19: inventory.Inv[i].It[j].shockDamage = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 20: inventory.Inv[i].It[j].shockPoint = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 21: inventory.Inv[i].It[j].wetPoint = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 22: inventory.Inv[i].It[j].bleedingPoint = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 23: inventory.Inv[i].It[j].healthRegen = 1 * getRandomInt(1, Mathf.Round(genTypeItem * (getRandomInt(79, 121) / 100))); break;
				case 24: inventory.Inv[i].It[j].manaRegen = 1 * getRandomInt(1, Mathf.Round(genTypeItem * (getRandomInt(79, 121) / 100))); break;
				case 25: if (inventory.Inv[i].It[j].maxHitsPers == 0)
						 {
					  		inventory.Inv[i].It[j].maxHits = 1 * getRandomInt(1, Mathf.Round(25 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 26: if (inventory.Inv[i].It[j].maxHits == 0)
						 {
					  		inventory.Inv[i].It[j].maxHitsPers = 1 * getRandomInt(1, Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 27: if (inventory.Inv[i].It[j].maxManaPers == 0)
						 {
					  		inventory.Inv[i].It[j].maxMana = 1 * getRandomInt(1, Mathf.Round(25 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 28: if (inventory.Inv[i].It[j].maxMana == 0)
						 {
					  		inventory.Inv[i].It[j].maxManaPers = 1 * getRandomInt(1, Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 29: inventory.Inv[i].It[j].experianse = 1 * getRandomInt(1, Mathf.Round(genTypeItem * (getRandomInt(79, 121) / 100))); break;
				case 30: if (inventory.Inv[i].It[j].forsePers == 0)
						 {
					  		inventory.Inv[i].It[j].forse = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 31: if (inventory.Inv[i].It[j].forse == 0)
						 {
					  		inventory.Inv[i].It[j].forsePers = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 32: if (inventory.Inv[i].It[j].lovkPers == 0)
						 {
					  		inventory.Inv[i].It[j].lovk = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 33: if (inventory.Inv[i].It[j].forsePers == 0)
						 {
					  		inventory.Inv[i].It[j].forse = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 34: if (inventory.Inv[i].It[j].teloPers == 0)
						 {
					  		inventory.Inv[i].It[j].telo = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 35: if (inventory.Inv[i].It[j].telo == 0)
						 {
					  		inventory.Inv[i].It[j].teloPers = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 36: if (inventory.Inv[i].It[j].intelectPer == 0)
						 {
					  		inventory.Inv[i].It[j].intelect = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 37: if (inventory.Inv[i].It[j].intelect == 0)
						 {
					  		inventory.Inv[i].It[j].intelectPer = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 38: if (inventory.Inv[i].It[j].mindForsePers == 0)
						 {
					  		inventory.Inv[i].It[j].mindForse = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 39: if (inventory.Inv[i].It[j].mindForse == 0)
						 {
					  		inventory.Inv[i].It[j].mindForsePers = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 40: inventory.Inv[i].It[j].manaDestr = 1 * getRandomInt(1, Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 41: inventory.Inv[i].It[j].manaDestrResistanse = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 42: inventory.Inv[i].It[j].speedAttackPers = 1 * getRandomInt(1, Mathf.Round(3 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break; 
				case 43: inventory.Inv[i].It[j].theifHits = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 44: inventory.Inv[i].It[j].theifMana = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 45: inventory.Inv[i].It[j].theifHitResistance = 1 * getRandomInt(1, Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 46: inventory.Inv[i].It[j].theifManaResistance = 1 * getRandomInt(1, Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 47: if (inventory.Inv[i].It[j].staminaPers == 0)
						 {
					  		inventory.Inv[i].It[j].stamina = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 48: if (inventory.Inv[i].It[j].stamina == 0)
						 {
					  		inventory.Inv[i].It[j].staminaPers = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 49: inventory.Inv[i].It[j].magicDamage = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
				case 50: inventory.Inv[i].It[j].staminaRegen = 1 * getRandomInt(1, Mathf.Round(genTypeItem * 30 *(getRandomInt(79, 121) / 100))); break;
				case 51: inventory.Inv[i].It[j].healthRegenPers = 1 * getRandomInt(1, Mathf.Round(genTypeItem * 30 * (getRandomInt(79, 121) / 100))); break;
				case 52: inventory.Inv[i].It[j].manaRegenPers = 1 * getRandomInt(1, Mathf.Round(genTypeItem * 30 * (getRandomInt(79, 121) / 100))); break;
				case 53: inventory.Inv[i].It[j].staminaRegenPers = 1 * getRandomInt(1, Mathf.Round(genTypeItem * 30 * (getRandomInt(79, 121) / 100))); break;
				case 54: inventory.Inv[i].It[j].damageForse = 1 * getRandomInt(1, Mathf.Round(genTypeItem * 5 * (getRandomInt(79, 121) / 100))); break;
			
			}		
		}
		}
		else
		{
			for(k = 0; k < modifictaors; k++)
		{
			typMod = getRandomInt(0, 38);
			switch (typMod)
			{
				case 1: inventory.Inv[i].It[j].fireResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 2: inventory.Inv[i].It[j].frozenResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 3: inventory.Inv[i].It[j].poisonResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 4: inventory.Inv[i].It[j].darknessResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 5: inventory.Inv[i].It[j].lightResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 6:	inventory.Inv[i].It[j].natureResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 7: inventory.Inv[i].It[j].shockResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 8: inventory.Inv[i].It[j].bleedingResistance = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 9: inventory.Inv[i].It[j].wetPoint = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				//case 10: inventory.Inv[i].It[j].bleedingPoint = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 10: inventory.Inv[i].It[j].healthRegen = 1 * getRandomInt(1, Mathf.Round(genTypeItem * (getRandomInt(79, 121) / 100))); break;
				case 11: inventory.Inv[i].It[j].manaRegen = 1 * getRandomInt(1, Mathf.Round(genTypeItem * (getRandomInt(79, 121) / 100))); break;
				case 12: if (inventory.Inv[i].It[j].maxHitsPers == 0)
						 {
					  		inventory.Inv[i].It[j].maxHits = 1 * getRandomInt(1, Mathf.Round(25 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 13: if (inventory.Inv[i].It[j].maxHits == 0)
						 {
					  		inventory.Inv[i].It[j].maxHitsPers = 1 * getRandomInt(1, Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 14: if (inventory.Inv[i].It[j].maxManaPers == 0)
						 {
					  		inventory.Inv[i].It[j].maxMana = 1 * getRandomInt(1, Mathf.Round(25 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 15: if (inventory.Inv[i].It[j].maxMana == 0)
						 {
					  		inventory.Inv[i].It[j].maxManaPers = 1 * getRandomInt(1, Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 16: inventory.Inv[i].It[j].experianse = 1 * getRandomInt(1, Mathf.Round(genTypeItem * (getRandomInt(79, 121) / 100))); break;
				case 17: if (inventory.Inv[i].It[j].forsePers == 0)
						 {
					  		inventory.Inv[i].It[j].forse = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 18: if (inventory.Inv[i].It[j].forse == 0)
						 {
					  		inventory.Inv[i].It[j].forsePers = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 19: if (inventory.Inv[i].It[j].lovkPers == 0)
						 {
					  		inventory.Inv[i].It[j].lovk = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 20: if (inventory.Inv[i].It[j].forsePers == 0)
						 {
					  		inventory.Inv[i].It[j].forse = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 21: if (inventory.Inv[i].It[j].teloPers == 0)
						 {
					  		inventory.Inv[i].It[j].telo = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 22: if (inventory.Inv[i].It[j].telo == 0)
						 {
					  		inventory.Inv[i].It[j].teloPers = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 23: if (inventory.Inv[i].It[j].intelectPer == 0)
						 {
					  		inventory.Inv[i].It[j].intelect = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 24: if (inventory.Inv[i].It[j].intelect == 0)
						 {
					  		inventory.Inv[i].It[j].intelectPer = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 25: if (inventory.Inv[i].It[j].mindForsePers == 0)
						 {
					  		inventory.Inv[i].It[j].mindForse = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 26: if (inventory.Inv[i].It[j].mindForse == 0)
						 {
					  		inventory.Inv[i].It[j].mindForsePers = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				//case 40: inventory.Inv[i].It[j].manaDestr = 1 * getRandomInt(1, Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 27: inventory.Inv[i].It[j].manaDestrResistanse = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 28: inventory.Inv[i].It[j].speedAttackPers = 1 * getRandomInt(1, Mathf.Round(3 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break; 
				//case 43: inventory.Inv[i].It[j].theifHits = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				//case 44: inventory.Inv[i].It[j].theifMana = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 29: inventory.Inv[i].It[j].theifHitResistance = 1 * getRandomInt(1, Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 30: inventory.Inv[i].It[j].theifManaResistance = 1 * getRandomInt(1, Mathf.Round(5 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100))); break;
				case 31: if (inventory.Inv[i].It[j].staminaPers == 0)
						 {
					  		inventory.Inv[i].It[j].stamina = 1 * getRandomInt(1, Mathf.Round(10 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 32: if (inventory.Inv[i].It[j].stamina == 0)
						 {
					  		inventory.Inv[i].It[j].staminaPers = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
						 }
						 break;
				case 33: inventory.Inv[i].It[j].magicDamage = 1 * getRandomInt(1, Mathf.Round(4 * (1 + genTypeItem) * (getRandomInt(79, 121) / 100)));
				case 34: inventory.Inv[i].It[j].staminaRegen = 1 * getRandomInt(1, Mathf.Round(genTypeItem * 30 *(getRandomInt(79, 121) / 100))); break;
				case 35: inventory.Inv[i].It[j].healthRegenPers = 1 * getRandomInt(1, Mathf.Round(genTypeItem * 30 * (getRandomInt(79, 121) / 100))); break;
				case 36: inventory.Inv[i].It[j].manaRegenPers = 1 * getRandomInt(1, Mathf.Round(genTypeItem * 30 * (getRandomInt(79, 121) / 100))); break;
				case 37: inventory.Inv[i].It[j].staminaRegenPers = 1 * getRandomInt(1, Mathf.Round(genTypeItem * 30 * (getRandomInt(79, 121) / 100))); break;
				case 38: inventory.Inv[i].It[j].damageForse = 1 * getRandomInt(1, Mathf.Round(genTypeItem * 5 * (getRandomInt(79, 121) / 100))); break;
	
		}
		}
}
}

var skillPoints : int;

var PlayerDefaultStamina = 10;
var PlayerDefaultStaminaRegen = 2;

public static var personMana : int;
public static var personMRegen : int;
public static var personStamina : int;
public static var personSRegen : int;

function Awake () {
	var fileLoadName = "Data/Save/Save.sg";
	PlayerDefaultStaminaRegen = 2;
	empty.clear();
	teorSkillPoint = 10;
	skillPoints = 10;
	//middleArmourTF = true;
//	downArmourTF = true;
	maskTF = true;
	styles = [inventoryButtonStyle, 
		sekiraStyle,
     	perchStyle1,
        sapogStyle1,
        upBodyStyle1,
        upMiddleBodyStyle1,
        shoulderStyle1,
        lokotStyle1,
        middleLegStyle1,
        middleLegStyle2,
        downLegStyle1,
        upLegStyle1,
        helmetStyle1,
        gorjetStyle1,
        narHandStyle1,
        middleBodyStyle1,
        downBodyStyle1,
        ringStyle1,
        ringStyle2,
        ringStyle3,
        ringStyle4,
        ringStyle5,
        ringStyle6,
        ringStyle7,
        ringStyle8,
        amuletStyle1,
        amuletStyle2,
        amuletStyle3,
        amuletStyle4,
        amuletStyle5,
        amuletStyle6,
        amuletStyle7,
        amuletStyle8,
        amuletStyle9,
        amuletStyle10,
        broshStyle1,
        broshStyle2,
        broshStyle3,
        broshStyle4,
        plashStyle1,
        poddocpehLeg1,
        poddospeh1,
        shtani1,
        helmetStyle2,
        podslemStyle1,
        poasStyle1,
        rubahaStyle1,
        maskStyle1,
        glazStyle1];
	help = GameObject.Find("ShitHelpMagicObject");
	//Save();
	Load();
		for (var zk = 0; zk < 11; zk++)
		{
			for(var kz = 0; kz < 8; kz++)
			{
					inventory.Inv[kz].It[zk].clear();
			}
		}
	for(var ty = 0; ty < 20; ty++)
	{
	  GenerateItem();
	}
	gen = true;
//	if ( upBodyArmour.type == 28 || upBodyArmour == )/
	//{
//		middleArmourTF = false;/
	//
	
	//}
	ListOfRash();
	CountCHaracteristic();
	Total();
	//totalCharacteristic.maxHits += PlayerHealthDefault;
	//totalCharacteristic.stamina += PlayerDefaultStamina;
	//totalCharacteristic.staminaRegen += PlayerDefaultStaminaRegen;
	personHits = totalCharacteristic.maxHits;
	personHRegen = totalCharacteristic.healthRegen;
	personMana = totalCharacteristic.maxMana;
	personMRegen = totalCharacteristic.manaRegen;
	personStamina = totalCharacteristic.stamina;
	personSRegen = totalCharacteristic.staminaRegen;
	for(var s = 0; s < equip; s++)
		{
			for(var i = 0; i<10; i++)
			{
				if (activeWeapon.Rash[s].equal(leftHand.Han[i]))
				{
					activeActiveWeapon[s] = -i - 1;
				}
				if (activeWeapon.Rash[s].equal(rightHand.Han[i]))
				{
					activeActiveWeapon[s] = i + 1;
				}
			}
		}
}

function Swap(a, b)
{
	var c = a;
	a = b;
	b = c;
}

var f = false;
var equip : int;
var activeWeapon : PoasW3;
var activeActiveWeapon = [-1] * 20;
var empty : Item;

function ListOfRash()
{
	for (var o = 0; o < 20; o++)
	{
		activeWeapon.Rash[o] = empty; 
	}
	var j = 0;
	for (var i = 0; i <= 7; i++)
	{
		if (poasWeapon.Rash[i].typeofmodel != 0)
		{
			activeWeapon.Rash[j] = poasWeapon.Rash[i];
			j++;
		}
	}
	if (neckLeft.typeofmodel != 0)
	{
		activeWeapon.Rash[j] = neckLeft;
		j++;
	}
	if (neckRight.typeofmodel != 0)
	{
		activeWeapon.Rash[j] = neckRight;
		j++;
	}
	if ( neckMiddle1.typeofmodel != 0)
	{
		activeWeapon.Rash[j] = neckMiddle1;
		j++;
	}
	if( neckMiddle2.typeofmodel != 0)
	{
		activeWeapon.Rash[j] = neckMiddle2;
		j++;
	}
	equip = j;
}

var flL = [false] * 11;
var flR = [false] * 11;

var buffs : Item;

function CountCHaracteristic()
{
	buffs.clear();
	for(var i = 0; i < 5; i++)
	{
		buffs.plus(ringsLeft.Ring[i]);
		buffs.plus(ringsRight.Ring[i]);
	}
	for(i = 0; i < 6; i++)
	{
		buffs.plus(amulets.Amul[i]);
	}
	for(i = 0; i < 9; i++)
	{
		buffs.plus(brosh.Br[i]);
	}
	buffs.plus(leftPerch);
	buffs.plus(rightPerch);
	buffs.plus(handDownLeft);
	buffs.plus(handDownRight);
	buffs.plus(handMiddleLeft);
	buffs.plus(handMiddleRight);
	buffs.plus(handUpLeft);
	buffs.plus(handUpRight);
	buffs.plus(shoulderLeft);
	buffs.plus(shoulderRight);
	buffs.plus(rubaha);
	buffs.plus(poddocpeh);
	buffs.plus(upBodyArmour);
	if ((!middleArmourTF) && (!downArmourTF))
	{
		buffs.plus(middleBodyArmour);
	}
	if(!downArmourTF)
	{
		buffs.plus(downBodyArmour);
	}
	buffs.plus(legUpLeft);
	buffs.plus(legUpRight);
	buffs.plus(legMiddleLeft);
	buffs.plus(legMiddleRight);
	buffs.plus(legDownLeft);
	buffs.plus(legDownRight);
	buffs.plus(sapogLeft);
	buffs.plus(sapogRight);
	buffs.plus(poas);
	buffs.plus(plash);	
	buffs.plus(helmet);
	buffs.plus(underHelmet);
	buffs.plus(glaza);
	buffs.plus(chelust);
	buffs.plus(gorjet);
	buffs.plus(shtani);
	buffs.plus(poddocpehLeg);
	for (i = 0; i <= 7; i++)
	{
		buffs.plus(poasWeapon.Rash[i]);
	}
	buffs.plus(neckLeft);
	buffs.plus(neckRight);
	buffs.plus(neckMiddle1);
	buffs.plus(neckMiddle2);
	changeItem = false;
}

var PersonCharacteristic : Item;
var PlayerHealthDefault = 10;

var changeItem = true;
var glazTF : boolean;

var gen = false;

public function DoGen(t : int)
{
	GenerateItem();
	gold += level * t;
	experianse += t;
	if (experianse >= 100)
	{
		teorSkillPoint += experianse / 100;
		level += experianse / 100;
		experianse = experianse % 100;
	}
	Save();
	Load();
}

function OnGUI () {
	//if (gen)
//	{/
	//	GenerateItem();
	//	gen = false;
	//}
	var c : Item;
	var st : GUIStyle;
	//xKof = Screen.width / 871;
	//yKof = Screen.height / 396;
	if (help.rigidbody.drag == 3)
	{
		middleArmourTF = false;
		downArmourTF = false;
		maskTF = false;
		glazTF = false;
		if (( upBodyArmour.type == 28) || (upBodyArmour.type == 29))
		{
			middleArmourTF = true;
		}
		if ( upBodyArmour.type == 29)
		{
			downArmourTF = true;
		}
		if (helmet.orientation != 0)
		{
			maskTF = true;
			glazTF = true;
		}
		if (chelust.orientation != 0)
		{
			glazTF = true;
		}
		if (changeItem)
		{
			CountCHaracteristic();
			Total();
		//	totalCharacteristic.maxHits += PlayerHealthDefault;
	//		totalCharacteristic.stamina += PlayerDefaultStamina;
//			totalCharacteristic.staminaRegen += PlayerDefaultStaminaRegen;
			personHits = totalCharacteristic.maxHits;
			personHRegen = totalCharacteristic.healthRegen;
			personMana = totalCharacteristic.maxMana;
			personMRegen = totalCharacteristic.manaRegen;	
			personStamina = totalCharacteristic.stamina;
			personSRegen = totalCharacteristic.staminaRegen;
		}
		GUI.Label(new Rect(5 + 330 / 2 - 20, 70, 330, 30), "Инвентарь");
		for (var i = 0; i < 11; i++)
		{
			for(var j = 0; j < 8; j++)
			{
				//Debug.Log(inventory.Inv[j].It[i].typeofmodel);
				st = styles[inventory.Inv[j].It[i].typeofmodel];
				if (inventory.Inv[j].It[i].typeofmodel == 0)
				{
					st = inventoryButtonStyle;
				}
				if(GUI.Button(new Rect(5 + i * 30, 100 + j * 30, 30, 30), "", st))
				{
					c = cursor;
					cursor = inventory.Inv[j].It[i];
					inventory.Inv[j].It[i] = c; 
					//Swap(cursor, inventory.Inv[j].It[i]);
				}
			}
		}
		if(GUI.Button(new Rect(Screen.width / 9 * 8 - 5, 5, Screen.width / 9, 30), "Магазин"))
		{
			changeItem = true;
			winMenu = 5;
		}
		if(GUI.Button(new Rect(Screen.width / 9 * 7 - 5, 5, Screen.width / 9, 30), "Статистика"))
		{
			changeItem = true;
			winMenu = 4;
		}
		if(GUI.Button(new Rect(Screen.width / 9 * 6 - 5, 5, Screen.width / 9, 30), "Параметры"))
		{
		    changeItem = true;
			winMenu = 3;
		}
		if(GUI.Button(new Rect(Screen.width / 9 * 5 - 5, 5, Screen.width / 9, 30), "Способности"))
		{
			changeItem = true;
			winMenu = 2;
		}
		if(GUI.Button(new Rect(Screen.width / 9 * 4 - 5, 5, Screen.width / 9, 30), "Экипировка"))
		{
			changeItem = true;
			winMenu = 1;
		}
		if (winMenu == 3 || winMenu == 1)
		{
			changeItem = true;
			GUI.Label(new Rect(Screen.width / 4 * 3, 85, 400, 30), "Здоровье: " + totalCharacteristic.maxHits.ToString());
			GUI.Label(new Rect(Screen.width / 4 * 3, 100, 400, 30), "Мана: " + totalCharacteristic.maxMana.ToString());
			GUI.Label(new Rect(Screen.width / 4 * 3, 115, 400, 30), "Выносливость: " + totalCharacteristic.stamina.ToString() );
			GUI.Label(new Rect(Screen.width / 4 * 3, 155, 400, 30), "Очки характеристик: " + skillPoints.ToString() );
			GUI.Label(new Rect(Screen.width / 4 * 3, 195, 400, 30), "Сила: "  + totalCharacteristic.forse.ToString());
			if(GUI.Button(new Rect(Screen.width / 4 * 3 + 110 + 20, 195, 17, 17), "+"))
				{			
					if ( skillPoints > 0)
					{
						buffForse += 1;
						skillPoints -= 1;
						changeItem = true;
					}	
				}
			if(GUI.Button(new Rect(Screen.width / 4 * 3 + 130 + 20, 195, 17, 17), "-"))
				{	
					if ( buffForse > 0)
					{
						buffForse -= 1;
						skillPoints += 1;
						changeItem = true;
					}			
				}
			GUI.Label(new Rect(Screen.width / 4 * 3, 215, 400, 30), "Ловкость: "  + totalCharacteristic.lovk.ToString());
			if(GUI.Button(new Rect(Screen.width / 4 * 3 + 110 + 20, 215, 17, 17), "+"))
				{			
					if ( skillPoints > 0)
					{
						buffLovk += 1;
						skillPoints -= 1;
						changeItem = true;
					}	
				}
			if(GUI.Button(new Rect(Screen.width / 4 * 3 + 130 + 20, 215, 17, 17), "-"))
				{	
					if ( buffLovk > 0)
					{
						buffLovk -= 1;
						skillPoints += 1;
						changeItem = true;
					}			
				}
			GUI.Label(new Rect(Screen.width / 4 * 3, 235, 400, 30), "Интелект: "  + totalCharacteristic.intelect.ToString());
			if(GUI.Button(new Rect(Screen.width / 4 * 3 + 110 + 20, 235, 17, 17), "+"))
				{			
					if ( skillPoints > 0)
					{
						buffIntelect += 1;
						skillPoints -= 1;
						changeItem = true;
					}	
				}
			if(GUI.Button(new Rect(Screen.width / 4 * 3 + 130 + 20, 235, 17, 17), "-"))
				{	
					if ( buffIntelect > 0)
					{
						buffIntelect -= 1;
						skillPoints += 1;
						changeItem = true;
					}			
				}
			GUI.Label(new Rect(Screen.width / 4 * 3, 255, 400, 30), "Сила воли: "  + totalCharacteristic.mindForse.ToString());
			if(GUI.Button(new Rect(Screen.width / 4 * 3 + 110 + 20, 255, 17, 17), "+"))
				{			
					if ( skillPoints > 0)
					{
						buffMindForse += 1;
						skillPoints -= 1;
						changeItem = true;
					}	
				}
			if(GUI.Button(new Rect(Screen.width / 4 * 3 + 130 + 20, 255, 17, 17), "-"))
				{	
					if ( buffMindForse > 0)
					{
						buffMindForse -= 1;
						skillPoints += 1;
						changeItem = true;
					}			
				}
			GUI.Label(new Rect(Screen.width / 4 * 3, 275, 400, 30), "Телосложение: "  + totalCharacteristic.telo.ToString());
			if(GUI.Button(new Rect(Screen.width / 4 * 3 + 110 + 20, 275, 17, 17), "+"))
				{			
					if ( skillPoints > 0)
					{
						buffTelo += 1;
						skillPoints -= 1;
						changeItem = true;
					}	
				}
			if(GUI.Button(new Rect(Screen.width / 4 * 3 + 130 + 20, 275, 17, 17), "-"))
				{	
					if ( buffTelo > 0)
					{
						buffTelo -= 1;
						skillPoints += 1;
						changeItem = true;
					}			
				}
			if(GUI.Button(new Rect(Screen.width / 4 * 3, 300, 200, 30), "Подтвердить распределение"))
				{	
					PersonCharacteristic.telo += buffTelo;
					PersonCharacteristic.forse += buffForse;
					PersonCharacteristic.lovk += buffLovk;
					PersonCharacteristic.mindForse += buffMindForse;
					PersonCharacteristic.intelect += buffIntelect;
					teorSkillPoint -= buffTelo + buffForse + buffMindForse + buffLovk + buffIntelect;
					buffTelo = 0;
					buffForse = 0;
					buffMindForse = 0;
					buffLovk = 0;
					buffIntelect = 0;
					skillPoints = teorSkillPoint;
					changeItem = true;		
					Total();	
				}
			if (winMenu == 3)
			{
				GUI.Label(new Rect(Screen.width / 2, 85, 400, 30), "Регенеация здоровья: "  + totalCharacteristic.healthRegen.ToString());
				GUI.Label(new Rect(Screen.width / 2, 100, 400, 30), "Регенеация маны:  "  + totalCharacteristic.manaRegen.ToString());
				GUI.Label(new Rect(Screen.width / 2, 115, 400, 30), "Регенеация выносливости:  "  + totalCharacteristic.staminaRegen.ToString());
				GUI.Label(new Rect(Screen.width / 2, 130, 400, 30), "Cопр огню:  "  + totalCharacteristic.fireResistance.ToString() + '%');
				GUI.Label(new Rect(Screen.width / 2, 145, 400, 30), "Сопр холоду:  "  + totalCharacteristic.frozenResistance.ToString() + '%');
				GUI.Label(new Rect(Screen.width / 2, 160, 400, 30), "Сопр кровотечению:  "  + totalCharacteristic.bleedingResistance.ToString() + '%');
				GUI.Label(new Rect(Screen.width / 2, 175, 400, 30), "Сопр ядам и кислотам:  "  + totalCharacteristic.poisonResistance.ToString() + '%');
				GUI.Label(new Rect(Screen.width / 2, 190, 400, 30), "Сопр свету:  "  + totalCharacteristic.lightResistance.ToString() + '%');
				GUI.Label(new Rect(Screen.width / 2, 205, 400, 30), "Сопр тьме:  "  + totalCharacteristic.darknessResistance.ToString() + '%');
				GUI.Label(new Rect(Screen.width / 2, 220, 400, 30), "Сопр электричеству:  "  + totalCharacteristic.shockResistance.ToString() + '%');
				GUI.Label(new Rect(Screen.width / 2, 235, 400, 30), "Сопр природе:  "  + totalCharacteristic.natureResistance.ToString() + '%');
				GUI.Label(new Rect(Screen.width / 2, 250, 400, 30), "Сила магии:  "  + totalCharacteristic.magicDamage.ToString() + '%');
				
			}
		}
		if (winMenu == 1)
		{
			if(GUI.Button(new Rect(Screen.width / 2 - 5, 35, Screen.width / 12, 30), "Голова"))
			{
				equpMenu = 1;
			}
			if(GUI.Button(new Rect(Screen.width / 2 + Screen.width / 12 - 5, 35, Screen.width / 12, 30), "Торс"))
			{
				equpMenu = 2;
			}
			if(GUI.Button(new Rect(Screen.width / 2 + Screen.width / 12 * 2 - 5, 35, Screen.width / 12, 30), "Пояс"))
			{
				equpMenu = 3;
			}
			if(GUI.Button(new Rect(Screen.width / 2 + Screen.width / 12 * 3 - 5, 35, Screen.width / 12, 30), "Ноги"))
			{
				equpMenu = 4;
			}
			if(GUI.Button(new Rect(Screen.width / 2 + Screen.width / 12 * 4 - 5, 35, Screen.width / 12, 30), "Руки"))
			{
				equpMenu = 5;
			}
			if(GUI.Button(new Rect(Screen.width / 2 + Screen.width / 12 * 5 - 5, 35, Screen.width / 12, 30), "Спина"))
			{
				equpMenu = 6;
			}
					
			if (equpMenu == 1)
			{
				st = styles[helmet.typeofmodel];
				if (helmet.typeofmodel == 0)
				{
					st = wallHelmetStyle;
				}	
				GUI.Label(new Rect(Screen.width / 2 + 100 / 2, 85, 100, 30), "Голова");
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 70 + 45, 90, 90), "Шлем", st))
				{				
					if (cursor.type == 0 || cursor.type == 23)
					{
						c = cursor;
						cursor = helmet;
						helmet = c; 
						changeItem = true;// helmet);
						if (helmet.orientation != 0)
						{
							maskTF = true;
							glazTF = true;
						}
					}
				}
				st = styles[underHelmet.typeofmodel];
				if (underHelmet.typeofmodel == 0)
				{
					st = wallUnderHeletStyle;
				}	
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 100, 70 + 15 + 45, 50, 50), "ПодШ.", st))
				{		
					if (cursor.type == 0 || cursor.type == 24)
					{
						c = cursor;
						cursor = underHelmet;
						underHelmet = c; 
						changeItem = true;// underHelmet);
					}			
				}
				if (! glazTF)
				{
					st = styles[glaza.typeofmodel];
					if (glaza.typeofmodel == 0)
					{
						st = wallGlazStyle;
					}	
					if(GUI.Button(new Rect(Screen.width / 2 - 5 + 100, 125 + 15 + 45, 50, 50), "Глаза", st))
					{		
						if (cursor.type == 0 || cursor.type == 25)
						{
							c = cursor;
							cursor = glaza;
							glaza = c; 
							changeItem = true;// glaza);
						}		
					}
				}
				else
				{
					GUI.Button(new Rect(Screen.width / 2 - 5 + 100, 125 + 15 + 45, 50, 50), "Глаза", itemBlockStyle);
				}
				if(! maskTF)
				{
					st = styles[chelust.typeofmodel];
					if (chelust.typeofmodel == 0)
					{
						st = wallChelustStyle;
					}
					if(GUI.Button(new Rect(Screen.width / 2 - 5 + 100, 180 + 15 + 45, 50, 50), "Лицо", st))
					{				
						if (cursor.type == 0 || cursor.type == 26)
						{
							c = cursor;
							cursor = chelust;
							chelust = c; 
							changeItem = true;// chelust);
						}
					}
				}
				else
				{
					GUI.Button(new Rect(Screen.width / 2 - 5 + 100, 180 + 15 + 45, 50, 50), "Лицо", itemBlockStyle);	
				}
				st = styles[gorjet.typeofmodel];
				if (gorjet.typeofmodel == 0)
				{
					st = wallGrogStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 170 + 45, 90, 90), "Горло", st))
				{				
					if (cursor.type == 0 || cursor.type == 32)
					{
						c = cursor;
						cursor = gorjet;
						gorjet = c; 
						changeItem = true;// gorjet);
					}
				}
				for (var y = 0; y < 6; y++)
				{
					st = styles[amulets.Amul[y].typeofmodel];
					if (amulets.Amul[y].typeofmodel == 0)
					{
						st = wallAmulStuyle;
					}
					if(GUI.Button(new Rect(Screen.width / 2 - 5 + 25 * y, 265 + 45, 20, 20), "O", st))
					{
						if (cursor.type == 0 || cursor.type == 5)
						{
							c = cursor;
							cursor = amulets.Amul[y];
							amulets.Amul[y] = c; 
							changeItem = true;// amulets.Amul[y]);
						}				
					}
				}
			}
			if (equpMenu == 2)
			{
				for (var e = 0; e < 2; e++)
				{
					st = styles[brosh.Br[e].typeofmodel];
					if (brosh.Br[e].typeofmodel == 0)
					{
						st = wallBroshStyle;
					}
					if(GUI.Button(new Rect(Screen.width / 2 - 5 + 75 + 2.5, 70 + 45 + e * 35, 30, 30), "Б", st))
					{					
							if (cursor.type == 0 || cursor.type == 6)
							{
								c = cursor;
								cursor = brosh.Br[e];
								brosh.Br[e] = c; 
					changeItem = true;// brosh.Br[e]);
							}				
					}
				}
				for (var h = 3; h <= 4; h++)
				{
					st = styles[brosh.Br[h].typeofmodel];
					if (brosh.Br[h].typeofmodel == 0)
					{
						st = wallBroshStyle;
					}
					if(GUI.Button(new Rect(Screen.width / 2 - 5 + 110 + 2.5, 70 + 45 + (h - 3) * 35, 30, 30), "Б", st))
					{	
							if (cursor.type == 0 || cursor.type == 6)
							{
								c = cursor;
								cursor = brosh.Br[h];
								brosh.Br[h] = c; 
					changeItem = true;// brosh.Br[h]);
							}						
					}
				}
				GUI.Label(new Rect(Screen.width / 2 + 100 / 2, 85, 100, 30), "Торс");
				st = styles[upBodyArmour.typeofmodel];
					if (upBodyArmour.typeofmodel == 0)
					{
						st = wallUpArmourStyle;
					}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 70 + 45, 70, 70), "Грудь", st))
				{					
					if (cursor.type == 0 || cursor.type == 14 || cursor.type == 28 || cursor.type == 29)
					{
						c = cursor;
						cursor = upBodyArmour;
						upBodyArmour = c; 
						changeItem = true;// upBodyArmour);
						if  (cursor.type == 28 || cursor.type == 29)
						{
							middleArmourTF = true;
						}
						if (cursor.type == 29)
						{
							downArmourTF = true;
						}
					}				
				}
				if (!(	middleArmourTF))
				{
					st = styles[ middleBodyArmour.typeofmodel];
					if (middleBodyArmour.typeofmodel == 0)
					{
						st = wallMidArmourStyle;
					}
					if(GUI.Button(new Rect(Screen.width / 2 - 5, 70 + 120, 70, 70), "Живот", st))
					{		
						if (cursor.type == 0 || cursor.type == 15)
						{
							c = cursor;
							cursor = middleBodyArmour;
							middleBodyArmour = c; 
							changeItem = true;// middleBodyArmour);
						}			
					}
				}
				else
				{
					GUI.Button(new Rect(Screen.width / 2 - 5, 70 + 120, 70, 70), "Живот", itemBlockStyle);
				}
				if ( !downArmourTF)
				{
					st = styles[downBodyArmour.typeofmodel];
					if (downBodyArmour.typeofmodel == 0)
					{
						st = wallDownArmourStyle;
					}
					if(GUI.Button(new Rect(Screen.width / 2 - 5, 70 + 195, 70, 70), "Юбка", st))
					{			
						if (cursor.type == 0 || cursor.type == 16)
						{
							c = cursor;
							cursor = downBodyArmour;
							downBodyArmour = c; 
						changeItem = true;// downBodyArmour);
						}		
					}	
				}
				else
				{
					GUI.Button(new Rect(Screen.width / 2 - 5, 70 + 195, 70, 70), "Юбка", itemBlockStyle);
				}
				st = styles[rubaha.typeofmodel];
				if (rubaha.typeofmodel == 0)
				{
					st = wallRubahaStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 75, 70 + 120, 70, 70), "Рубаха", st))
				{	
					if (cursor.type == 0 || cursor.type == 12)
					{
						c = cursor;
						cursor = rubaha;
						rubaha = c; 
					changeItem = true;// rubaha);
					}				
				}
				st = styles[poddocpeh.typeofmodel];
				if (poddocpeh.typeofmodel == 0)
				{
					st = wallUnderArmou;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 75, 70 + 195, 70, 70), "ПодДосп.", st))
				{	
					if (cursor.type == 0 || cursor.type == 13)
					{
						c = cursor;
						cursor = poddocpeh;
						poddocpeh = c; 
					changeItem = true;// poddocpeh);
					}							
				}		
			}
			if (equpMenu == 3)
			{
				GUI.Label(new Rect(Screen.width / 2 + 100 / 2, 85, 100, 30), "Пояс");
				st = styles[poas.typeofmodel];
				if (poas.typeofmodel == 0)
				{
					st = wallPoasStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 45, 70 + 45, 50, 50), "Пояс", st))
				{	
					if (cursor.type == 0 || cursor.type == 21)
					{
						c = cursor;
						cursor = poas;
						poas = c; 
					changeItem = true;// poas);
					}
				}
				st = styles[poasWeapon.Rash[0].typeofmodel];
				if (poasWeapon.Rash[0].typeofmodel == 0)
				{
					st = wallWeaonStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 70 + 45 + 55, 40, 40), "Расх", st))
				{		
					if (cursor.type <= 2)
					{
						c = cursor;
						cursor = poasWeapon.Rash[0];
						poasWeapon.Rash[0] = c; 
					changeItem = true;// poasWeapon.Rash[0]);
					}			
				}
				st = styles[poasWeapon.Rash[1].typeofmodel];
				if (poasWeapon.Rash[1].typeofmodel == 0)
				{
					st = wallWeaonStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 45 + 55, 70 + 45 + 55, 40, 40), "Расх", st))
				{		
					if (cursor.type <= 2)
					{
						c = cursor;
						cursor = poasWeapon.Rash[1];
						poasWeapon.Rash[1] = c; 
					changeItem = true;// poasWeapon.Rash[1]);
					}			
				}
				st = styles[poasWeapon.Rash[2].typeofmodel];
				if (poasWeapon.Rash[2].typeofmodel == 0)
				{
					st = wallWeaonStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 70 + 45 + 100, 40, 40), "Расх", st))
				{	
					if (cursor.type <= 2)
					{
						c = cursor;
						cursor = poasWeapon.Rash[2];
						poasWeapon.Rash[2] = c; 
					changeItem = true;// poasWeapon.Rash[2]);
					}				
				}
				st = styles[poasWeapon.Rash[3].typeofmodel];
				if (poasWeapon.Rash[3].typeofmodel == 0)
				{
					st = wallWeaonStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 45 + 55, 70 + 45 + 100, 40, 40), "Расх", st))
				{		
					if (cursor.type <= 2)
					{
						c = cursor;
						cursor = poasWeapon.Rash[3];
						poasWeapon.Rash[3] = c; 
					changeItem = true;// poasWeapon.Rash[3]);
					}			
				}
				st = styles[poasWeapon.Rash[4].typeofmodel];
				if (poasWeapon.Rash[4].typeofmodel == 0)
				{
					st = wallWeaonStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 70 + 45 + 145, 40, 40), "Расх", st))
				{		
					if (cursor.type <= 2)
					{
						c = cursor;
						cursor = poasWeapon.Rash[4];
						poasWeapon.Rash[4] = c; 
					changeItem = true;// poasWeapon.Rash[4]);
					}			
				}
				st = styles[poasWeapon.Rash[5].typeofmodel];
				if (poasWeapon.Rash[5].typeofmodel == 0)
				{
					st = wallWeaonStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 45 + 55, 70 + 45 + 145, 40, 40), "Расх", st))
				{					
					if (cursor.type <= 2)
					{
						c = cursor;
						cursor = poasWeapon.Rash[5];
						poasWeapon.Rash[5] = c; 
					changeItem = true;// poasWeapon.Rash[5]);
					}
				}
				for (var l = 5; l < 9; l++)
				{
					st = styles[brosh.Br[l].typeofmodel];
					if (brosh.Br[l].typeofmodel == 0)
					{
						st = wallBroshStyle;
					}
					if(GUI.Button(new Rect(Screen.width / 2 - 2.5  + 35 * (l - 5), 70 + 45 + 200, 30, 30), "Б", st))
					{		
						if (cursor.type == 0 || cursor.type == 6)
					{
						c = cursor;
						cursor = brosh.Br[l];
						brosh.Br[l] = c; 
					changeItem = true;// brosh.Br[l]);
					}			
					}
				}
			}
			if (equpMenu == 4)
			{
				st = styles[legUpLeft.typeofmodel];
				if (legUpLeft.typeofmodel == 0)
				{
					st = wallPonStyle;
				}
				GUI.Label(new Rect(Screen.width / 2 + 100 / 2, 85, 100, 30), "Ноги");
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 70 + 45, 40, 40), "По", st) && ( cursor.orientation == 0))
				{					
					if (cursor.type == 0 || cursor.type == 17)
					{
						c = cursor;
						cursor = legUpLeft;
						legUpLeft = c; 
					changeItem = true;// legUpLeft);
					}
				}
				st = styles[legMiddleLeft.typeofmodel];
				if (legMiddleLeft.typeofmodel == 0)
				{
					st = wallLegMidStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 115 + 45, 40, 40), "Ко", st) && ( cursor.orientation == 0))
				{					
					if (cursor.type == 0 || cursor.type == 18)
					{
						c = cursor;
						cursor = legMiddleLeft;
						legMiddleLeft = c; 
					changeItem = true;// legMiddleLeft);
					}
				}
				st = styles[legDownLeft.typeofmodel];
				if (legDownLeft.typeofmodel == 0)
				{
					st = wallPonDownStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 160 + 45, 40, 40), "Го", st) && ( cursor.orientation == 0))
				{					
					if (cursor.type == 0 || cursor.type == 19)
					{
						c = cursor;
						cursor = legDownLeft;
						legDownLeft = c; 
					changeItem = true;// legDownLeft);
					}
				}
				st = styles[sapogLeft.typeofmodel];
				if (sapogLeft.typeofmodel == 0)
				{
					st = wallSapogStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 205 + 45, 40, 40), "Са", st) && ( cursor.orientation == 0))
				{					
					if (cursor.type == 0 || cursor.type == 20)
					{
						c = cursor;
						cursor = sapogLeft;
						sapogLeft = c; 
					changeItem = true;// sapogLeft);
					}
				}
				st = styles[legUpRight.typeofmodel];
				if (legUpRight.typeofmodel == 0)
				{
					st = wallPonStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 110, 70 + 45, 40, 40), "По", st) && (( cursor.orientation == 1) || ( cursor.type == 0)))
				{
					if (cursor.type == 0 || cursor.type == 17)
					{
						c = cursor;
						cursor = legUpRight;
						legUpRight = c; 
					changeItem = true;// legUpRight);
					}					
				}
				st = styles[legMiddleRight.typeofmodel];
				if (legMiddleRight.typeofmodel == 0)
				{
					st = wallLegMidStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 110, 115 + 45, 40, 40), "Ко", st) && (( cursor.orientation == 1) || ( cursor.type == 0)))
				{				
					if (cursor.type == 0 || cursor.type == 18)
					{
						c = cursor;
						cursor = legMiddleRight;
						legMiddleRight = c; 
					changeItem = true;// legMiddleRight);
					}	
				}
				st = styles[legDownRight.typeofmodel];
				if (legDownRight.typeofmodel == 0)
				{
					st = wallPonDownStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 110, 160 + 45, 40, 40), "Го", st) && (( cursor.orientation == 1) || ( cursor.type == 0)))
				{					
					if (cursor.type == 0 || cursor.type == 19)
					{
						c = cursor;
						cursor = legDownRight;
						legDownRight = c; 
					changeItem = true;// legDownRight);
					}
				}
				st = styles[sapogRight.typeofmodel];
				if (sapogRight.typeofmodel == 0)
				{
					st = wallSapogStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 110, 205 + 45, 40, 40), "Са", st) && (( cursor.orientation == 1) || ( cursor.type == 0)))
				{					
					if (cursor.type == 0 || cursor.type == 20)
					{
						c = cursor;
						cursor = sapogRight;
						sapogRight = c; 
					changeItem = true;// sapogRight);
					}
				}
				for (var z = 6; z <= 7; z++)
				{
					st = styles[poasWeapon.Rash[z].typeofmodel];
					if (poasWeapon.Rash[z].typeofmodel == 0)
					{
						st = wallWeaonStyle;
					}
					if(GUI.Button(new Rect(Screen.width / 2 - 5 + 55, 70 + 45 * (z - 5), 40, 40), "Расх", st))
					{					
						if (cursor.type < 1)
					{
						c = cursor;
						cursor = poasWeapon.Rash[z];
						poasWeapon.Rash[z] = c; 
					changeItem = true;// poasWeapon.Rash[z]);
					}
					}
				}
				st = styles[shtani.typeofmodel];
				if (shtani.typeofmodel == 0)
				{
					st = wallStaniStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 50, 70 + 135, 50, 50), "Штаны", st))
				{					
					if (cursor.type == 0 || cursor.type == 33)
					{
						c = cursor;
						cursor = shtani;
						shtani = c; 
					changeItem = true;// shtani);
					}
				}
				st = styles[poddocpehLeg.typeofmodel];
				if (poddocpehLeg.typeofmodel == 0)
				{
					st = wallUnderArmourLeg;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 50, 70 + 190, 50, 50), "ПодД.", st))
				{				
					if (cursor.type == 0 || cursor.type == 34)
					{
						c = cursor;
						cursor = poddocpehLeg;
						poddocpehLeg = c; 
					changeItem = true;// poddocpehLeg);
					}	
				}


			}
			if (equpMenu == 5)
			{
				GUI.Label(new Rect(Screen.width / 2 + 120 / 2, 85, 100, 30), "Руки");
				for (var x = 9; x <= 10; x++)
				{
					st = styles[poasWeapon.Rash[x].typeofmodel];
					if (poasWeapon.Rash[x].typeofmodel == 0)
					{
						st = wallWeaonStyle;
					}
					if(GUI.Button(new Rect(Screen.width / 2 - 5 + 55, 70 + 170 + 45 * (x - 9), 40, 40), "Расх", st))
					{					
						if (cursor.type <= 1)
						{
							c = cursor;
							cursor = poasWeapon.Rash[x];
							poasWeapon.Rash[x] = c; 
					//		Swap(cursor, poasWeapon.Rash[x]);
						}
					}
				}
				for (var k = 0; k < 5; k++)
				{
					st = styles[ringsLeft.Ring[k].typeofmodel];
					if (ringsLeft.Ring[k].typeofmodel == 0)
					{
						st = wallRingStyle;
					}
					if(GUI.Button(new Rect(Screen.width / 2 - 5 + 45, 70 + 45 + 25 * k, 20, 20), "К", st))
					{			
						if (cursor.type == 0 || cursor.type == 4)
						{
							c = cursor;
							cursor = ringsLeft.Ring[k];
							ringsLeft.Ring[k] = c; 
							changeItem = true;
					//		Swap(cursor, ringsLeft.Ring[k]);
						}		
					}
				}
				for (var t = 0; t < 5; t++)
				{
					st = styles[ringsRight.Ring[t].typeofmodel];
					if (ringsRight.Ring[t].typeofmodel == 0)
					{
						st = wallRingStyle;
					}
					if(GUI.Button(new Rect(Screen.width / 2 - 5 + 45 + 75 - 35, 70 + 45 + 25 * t, 20, 20), "К", st))
					{		
						if (cursor.type == 0 || cursor.type == 4)
						{
							c = cursor;
							cursor = ringsRight.Ring[t];
							ringsRight.Ring[t] = c; 
							changeItem = true;
					//		Swap(cursor, ringsRight.Ring[t]);
						}			
					}
				}
				st = styles[shoulderLeft.typeofmodel];
				if (shoulderLeft.typeofmodel == 0)
				{
					st = wallShoulderStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 70 + 45, 40, 40), "Пл", st) && ( cursor.orientation == 0))
				{				
					if (cursor.type == 0 || cursor.type == 11)
					{
						c = cursor;
						cursor = shoulderLeft;
						shoulderLeft = c; 
					changeItem = true;// shoulderLeft);
					}	
				}
				st = styles[handUpLeft.typeofmodel];
				if (handUpLeft.typeofmodel == 0)
				{
					st = wallNarStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 115 + 45, 40, 40), "На", st) && ( cursor.orientation == 0))
				{			
					if (cursor.type == 0 || cursor.type == 8)
					{
						c = cursor;
						cursor = handUpLeft;
						handUpLeft = c; 
					changeItem = true;// handUpLeft);
					}		
				}
				st = styles[handMiddleLeft.typeofmodel];
				if (handMiddleLeft.typeofmodel == 0)
				{
					st = wallHandMidStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 160 + 45, 40, 40), "Ло", st) && ( cursor.orientation == 0))
				{			
					if (cursor.type == 0 || cursor.type == 9)
					{
						c = cursor;
						cursor = handMiddleLeft;
						handMiddleLeft = c; 
					changeItem = true;// handMiddleLeft);
					}		
				}
				st = styles[handDownLeft.typeofmodel];
				if (handDownLeft.typeofmodel == 0)
				{
					st = wallNarStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 205 + 45, 40, 40), "На", st) && ( cursor.orientation == 0))
				{			
					if (cursor.type == 0 || cursor.type == 8)
					{
						c = cursor;
						cursor = handDownLeft;
						handDownLeft = c; 
					changeItem = true;// handDownLeft);
					}		
				}
				st = styles[leftPerch.typeofmodel];
				//Debug.Log(leftPerch.typeofmodel);
				if (leftPerch.typeofmodel == 0)
				{
					st = wallPerchStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5, 250 + 45, 40, 40), "Ки", st) && ( cursor.orientation == 0))
				{					
					if (cursor.type == 0 || cursor.type == 7)
					{
						c = cursor;
						cursor = leftPerch;
						leftPerch = c; 
					changeItem = true;// leftPerch);
					}
				}
				st = styles[shoulderRight.typeofmodel];
				if (shoulderRight.typeofmodel == 0)
				{
					st = wallShoulderStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 110, 70 + 45, 40, 40), "Пл", st) && (( cursor.orientation == 1) || ( cursor.type == 0)))
				{					
					if (cursor.type == 0 || cursor.type == 11)
					{
						c = cursor;
						cursor = shoulderRight;
						shoulderRight = c; 
					changeItem = true;// shoulderRight);
					}
				}
				st = styles[handUpRight.typeofmodel];
				if (handUpRight.typeofmodel == 0)
				{
					st = wallNarStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 110, 115 + 45, 40, 40), "На", st) && (( cursor.orientation == 1) || ( cursor.type == 0)))
				{					
					if (cursor.type == 0 || cursor.type == 8)
					{
						c = cursor;
						cursor = handUpRight;
						handUpRight = c; 
					changeItem = true;// handUpRight);
					}
				}
				st = styles[handMiddleRight.typeofmodel];
				if (handMiddleRight.typeofmodel == 0)
				{
					st = wallHandMidStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 110, 160 + 45, 40, 40), "Ло", st) && (( cursor.orientation == 1) || ( cursor.type == 0)))
				{					
					if (cursor.type == 0 || cursor.type == 9)
					{
						c = cursor;
						cursor = handMiddleRight;
						handMiddleRight = c; 
					changeItem = true;// handMiddleRight);
					}
				}
				st = styles[handDownRight.typeofmodel];
				if (handDownRight.typeofmodel == 0)
				{
					st = wallNarStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 110, 205 + 45, 40, 40), "На", st) && (( cursor.orientation == 1) || ( cursor.type == 0)))
				{					
					if (cursor.type == 0 || cursor.type == 8)
					{
						c = cursor;
						cursor = handDownRight;
						handDownRight = c; 
					changeItem = true;// handDownRight);
					}
				}
				st = styles[rightPerch.typeofmodel];
				if (rightPerch.typeofmodel == 0)
				{
					st = wallPerchStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 110, 250 + 45, 40, 40), "Ки", st) && (( cursor.orientation == 1) || ( cursor.type == 0)))
				{					
					if (cursor.type == 0 || cursor.type == 7)
					{
						c = cursor;
						cursor = rightPerch;
						rightPerch = c; 
					changeItem = true;// shoulderRight);
					}
				}
			}
			if (equpMenu == 6)
			{
				GUI.Label(new Rect(Screen.width / 2 + 100 / 2, 85, 100, 30), "Спина");
				st = styles[plash.typeofmodel];
				if (plash.typeofmodel == 0)
				{
					st = wallPlashStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 50, 70 + 45, 50, 50), "Плащ", st))
				{				
					if (cursor.type == 0 || cursor.type == 22)
					{
						c = cursor;
						cursor = plash;
						plash = c; 
					changeItem = true;// plash);
					}	
				}
				st = styles[neckLeft.typeofmodel];
				if (neckLeft.typeofmodel == 0)
				{
					st = wallWeaonStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 10, 70 + 100, 60, 60), "Расх", st))
				{			
					if (cursor.type <= 3)
					{
						c = cursor;
						cursor = neckLeft;
						neckLeft = c; 
					changeItem = true;// neckLeft);
					}		
				}
				st = styles[neckRight.typeofmodel];
				if (neckRight.typeofmodel == 0)
				{
					st = wallWeaonStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 80, 70 + 100, 60, 60), "Расх", st))
				{			
					if (cursor.type <= 3)
					{
						c = cursor;
						cursor = neckRight;
						neckRight = c; 
					changeItem = true;// neckRight);
					}		
				}
				st = styles[neckMiddle1.typeofmodel];
				if (neckMiddle1.typeofmodel == 0)
				{
					st = wallWeaonStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 50, 70 + 165, 50, 50), "Расх", st))
				{					
					if (cursor.type < 2)
					{
						c = cursor;
						cursor = neckMiddle1;
						neckMiddle1 = c; 
					changeItem = true;// neckMiddle1);
					}
				}
				st = styles[neckMiddle2.typeofmodel];
				if (neckMiddle2.typeofmodel == 0)
				{
					st = wallWeaonStyle;
				}
				if(GUI.Button(new Rect(Screen.width / 2 - 5 + 50, 70 + 220, 50, 50), "Расх", st))
				{		
					if (cursor.type < 2)
					{
						c = cursor;
						cursor = neckMiddle2;
						neckMiddle2 = c; 
					changeItem = true;// neckMiddle2);
					}			
				}
			}
			for (i = 0; i < 11; i++)
		{
			for(j = 0; j < 8; j++)
			{
				if(new Rect(5 + i * 30, 100 + j * 30, 30, 30).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{
					ShowParametres(inventory.Inv[j].It[i]);
				}
			}
		}
		if (equpMenu == 1)
		{
			if(new Rect(Screen.width / 2 - 5, 70 + 45, 90, 90).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{				
				ShowParametres(helmet);
			}
			if(new Rect(Screen.width / 2 - 5 + 100, 70 + 15 + 45, 50, 50).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{		
				ShowParametres(underHelmet);		
			}
			if(new Rect(Screen.width / 2 - 5 + 100, 125 + 15 + 45, 50, 50).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{		
				ShowParametres(glaza);	
			}
			if(new Rect(Screen.width / 2 - 5 + 100, 180 + 15 + 45, 50, 50).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{				
				ShowParametres(chelust);
			}
			if(new Rect(Screen.width / 2 - 5, 170 + 45, 90, 90).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{				
				ShowParametres(gorjet);
			}
			for (y = 0; y < 6; y++)
			{
				if(new Rect(Screen.width / 2 - 5 + 25 * y, 265 + 45, 20, 20).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{
					ShowParametres(amulets.Amul[y]);		
				}
			}
		}
		if (equpMenu == 2)
		{
			for (e = 0; e < 2; e++)
			{
				if(new Rect(Screen.width / 2 - 5 + 75 + 2.5, 70 + 45 + e * 35, 30, 30).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{					
					ShowParametres(brosh.Br[e]);			
				}	
				for (h = 3; h <= 4; h++)
				{
					if(new Rect(Screen.width / 2 - 5 + 110 + 2.5, 70 + 45 + (h - 3) * 35, 30, 30).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
					{	
						ShowParametres(brosh.Br[h]);					
					}
				}
				if(new Rect(Screen.width / 2 - 5, 70 + 45, 70, 70).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{					
					ShowParametres(upBodyArmour);			
					}
				if(new Rect(Screen.width / 2 - 5, 70 + 120, 70, 70).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{		
					ShowParametres(middleBodyArmour);		
				}
					if(new Rect(Screen.width / 2 - 5, 70 + 195, 70, 70).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{			
					ShowParametres(downBodyArmour);	
				}	
				if(new Rect(Screen.width / 2 - 5 + 75, 70 + 120, 70, 70).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{	
					ShowParametres(rubaha);				
				}
				if(new Rect(Screen.width / 2 - 5 + 75, 70 + 195, 70, 70).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{	
					ShowParametres(poddocpeh);						
				}		
			}
		}
		if (equpMenu == 3)
		{
			if(new Rect(Screen.width / 2 - 5 + 45, 70 + 45, 50, 50).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{	
				ShowParametres(poas);
			}
			if(new Rect(Screen.width / 2 - 5, 70 + 45 + 55, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{		
				ShowParametres(poasWeapon.Rash[0]);		
			}
			if(new Rect(Screen.width / 2 - 5 + 45 + 55, 70 + 45 + 55, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{		
				ShowParametres(poasWeapon.Rash[1]);		
			}
			if(new Rect(Screen.width / 2 - 5, 70 + 45 + 100, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{	
				ShowParametres(poasWeapon.Rash[2]);	
			}
			if(new Rect(Screen.width / 2 - 5 + 45 + 55, 70 + 45 + 100, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{		
				ShowParametres(poasWeapon.Rash[3]);			
			}
			if(new Rect(Screen.width / 2 - 5, 70 + 45 + 145, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{		
				ShowParametres(poasWeapon.Rash[4]);			
			}
			if(new Rect(Screen.width / 2 - 5 + 45 + 55, 70 + 45 + 145, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(poasWeapon.Rash[5]);
			}
			for (l = 5; l < 9; l++)
			{
				if(new Rect(Screen.width / 2 - 2.5  + 35 * (l - 5), 70 + 45 + 200, 30, 30).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{		
					ShowParametres(brosh.Br[l]);			
				}
			}
		}
		if (equpMenu == 4)
		{
			if(new Rect(Screen.width / 2 - 5, 70 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(legUpLeft);
			}
			if(new Rect(Screen.width / 2 - 5, 115 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(legMiddleLeft);
			}
			if(new Rect(Screen.width / 2 - 5, 160 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(legDownLeft);
			}
			if(new Rect(Screen.width / 2 - 5, 205 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(sapogLeft);
			}
			if(new Rect(Screen.width / 2 - 5 + 110, 70 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{
				ShowParametres(legUpRight);				
			}
			if(new Rect(Screen.width / 2 - 5 + 110, 115 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{				
				ShowParametres(legMiddleRight);
			}
			if(new Rect(Screen.width / 2 - 5 + 110, 160 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(legDownRight);
			}
			if(new Rect(Screen.width / 2 - 5 + 110, 205 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(sapogRight);
			}
			for (z = 6; z <= 7; z++)
				{
				if(new Rect(Screen.width / 2 - 5 + 55, 70 + 45 * (z - 5), 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{					
					ShowParametres(poasWeapon.Rash[z]);
				}
			}
			if(new Rect(Screen.width / 2 - 5 + 50, 70 + 135, 50, 50).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(shtani);
			}

			if(new Rect(Screen.width / 2 - 5 + 50, 70 + 190, 50, 50).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{				
				ShowParametres(poddocpehLeg);
			}

		}
		if (equpMenu == 5)
			{
			for (x = 9; x <= 10; x++)
			{
				if(new Rect(Screen.width / 2 - 5 + 55, 70 + 170 + 45 * (x - 9), 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{					
			 		ShowParametres(poasWeapon.Rash[x]);
				}
			}
			for (k = 0; k < 5; k++)
			{
				if(new Rect(Screen.width / 2 - 5 + 45, 70 + 45 + 25 * k, 20, 20).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{			
					ShowParametres(ringsLeft.Ring[k]);	
				}
			}
			for (t = 0; t < 5; t++)
			{
				if(new Rect(Screen.width / 2 - 5 + 45 + 75 - 35, 70 + 45 + 25 * t, 20, 20).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{		
					ShowParametres(ringsRight.Ring[t]);	
				}
			}
			if(new Rect(Screen.width / 2 - 5, 70 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{				
				ShowParametres(shoulderLeft);	
			}
			if(new Rect(Screen.width / 2 - 5, 115 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{			
				ShowParametres(handUpLeft);		
			}
			if(new Rect(Screen.width / 2 - 5, 160 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{			
				ShowParametres(handMiddleLeft);	
			}
			if(new Rect(Screen.width / 2 - 5, 205 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{			
				ShowParametres(handDownLeft);	
			}
			if(new Rect(Screen.width / 2 - 5, 250 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(leftPerch);
			}
			if(new Rect(Screen.width / 2 - 5 + 110, 70 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(shoulderRight);
			}
			if(new Rect(Screen.width / 2 - 5 + 110, 115 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(handUpRight);
			}
			if(new Rect(Screen.width / 2 - 5 + 110, 160 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(handMiddleRight);
			}
			if(new Rect(Screen.width / 2 - 5 + 110, 205 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(handDownRight);
			}
			if(new Rect(Screen.width / 2 - 5 + 110, 250 + 45, 40, 40).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(rightPerch);
			}
		}
		if (equpMenu == 6)
		{
			if(new Rect(Screen.width / 2 - 5 + 50, 70 + 45, 50, 50).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{				
				ShowParametres(plash);
			}
			if(new Rect(Screen.width / 2 - 5 + 10, 70 + 100, 60, 60).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{			
				ShowParametres(neckLeft);	
			}
			if(new Rect(Screen.width / 2 - 5 + 80, 70 + 100, 60, 60).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{		
				ShowParametres(neckRight);		
			}
			if(new Rect(Screen.width / 2 - 5 + 50, 70 + 165, 50, 50).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{					
				ShowParametres(neckMiddle1);
			}
			if(new Rect(Screen.width / 2 - 5 + 50, 70 + 220, 50, 50).Contains(Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{		
				ShowParametres(neckMiddle2);		
			}
		}
		}
		else if (winMenu == 2)
		{
		
		}
		if (winMenu == 4)
		{
		}
		else 
		{
		}
		for(i = 0; i < equip; i++)
		{
			activeActiveWeapon[i] = 0;
		}
		for(var s = 0; s < equip; s++)
		{
			for(i = 0; i<10; i++)
			{
				if (activeWeapon.Rash[s].equal(leftHand.Han[i]))
				{
					activeActiveWeapon[s] = -i - 1;
				}
				if (activeWeapon.Rash[s].equal(rightHand.Han[i]))
				{
					activeActiveWeapon[s] = i + 1;
				}
			}
		}
		ListOfRash();
		if(GUI.Button(new Rect(5, 5, Screen.width / 10, 30), "Назад"))
		{
			help.rigidbody.drag = 2;
		}
		if(GUI.Button(new Rect(5 + Screen.width / 10, 5, Screen.width / 10, 30), "Сохранить"))
		{
			Save();
		}
		if(GUI.Button(new Rect(5, 35, Screen.width / 5, 30), "Откатить изменения"))
		{
			Load();
		}
		GUI.Label(new Rect(5 + 2 * Screen.width / 10, 5, Screen.width / 5, 30), "Уровень: " + level.ToString());
		GUI.Label(new Rect(5 + 2 * Screen.width / 10, 25, Screen.width / 5, 30), "Опыт: " + experianse.ToString() + '/1000');
		GUI.Label(new Rect(5 + 2 * Screen.width / 10, 45, Screen.width / 5, 30), "Золото: " + gold.ToString());
		for (var m = 10; m >= 1; m--)
		{
			var eq = false;
			for(s = 0; s < equip; s++)
			{
				if (activeWeapon.Rash[s].equal(leftHand.Han[m - 1]))
				{
					eq = true;
				//	activeActiveWeapon[s] = -m;
					break;
				}
			}
			if (!eq)
			{
				leftHand.Han[m - 1] = empty;
			}
			st = styles[leftHand.Han[m - 1].typeofmodel];
			if (leftHand.Han[m - 1].typeofmodel == 0)
			{
				st = wallWeaonStyle;
			}
			if(GUI.Button(new Rect(5 + (10 - m) * 35, Screen.height - 40, 35, 35), "F" + m.ToString(), st))
			{
				flL[m] = !flL[m];
			}
			if (flL[m])
			{
				for(var u = 0; u < equip; u++)
				{
					if (activeActiveWeapon[u] == 0)
					{
						if(GUI.Button(new Rect(5 + (10 - m) * 35, Screen.height - 40 - 35 * (u + 1), 35, 35), "", styles[activeWeapon.Rash[u].typeofmodel]))	
						{
							leftHand.Han[m - 1] = activeWeapon.Rash[u];
							flL[m] = false;
							activeActiveWeapon[u] = -m;
						}
					}
					else
					{
						if(GUI.Button(new Rect(5 + (10 - m) * 35, Screen.height - 40 - 35 * (u + 1), 35, 35), "", itemBlockStyle) && activeActiveWeapon[u] == -m)
						{
							activeActiveWeapon[u] = 0;
							flL[m] = true;
							leftHand.Han[m - 1]= empty;
						}
					}
				}
			}
		}
		for (var o = 1; o <= 10; o++)
		{
			eq = false;
			for(s = 0; s < equip; s++)
			{
				if (activeWeapon.Rash[s].equal(rightHand.Han[o - 1]))
				{
					eq = true;
				//	activeActiveWeapon[s] = o;
					break;
				}
			}
			if (!eq)
			{
				rightHand.Han[o - 1] = empty;
			}
			st = styles[rightHand.Han[o - 1].typeofmodel];
			if (rightHand.Han[o - 1].typeofmodel == 0)
			{
				st = wallWeaonStyle;
			}
			if(GUI.Button(new Rect(Screen.width - 40 - 35 * (10 - o), Screen.height - 40, 35, 35), o.ToString(), st))
			{
				flR[o] = !flR[o];
			}
			if (flR[o])
			{
				for(var p = 0; p < equip; p++)
				{
					if (activeActiveWeapon[p] == 0)
					{
						if(GUI.Button(new Rect(Screen.width - 40 - 35 * (10 - o), Screen.height - 40 - 35 * (p + 1), 35, 35), "", styles[activeWeapon.Rash[p].typeofmodel]))	
						{
							rightHand.Han[o - 1] = activeWeapon.Rash[p];
							flR[o] = false;
							activeActiveWeapon[p] = o;
						}
					}
					else
					{
						if(GUI.Button(new Rect(Screen.width - 40 - 35 * (10 - o), Screen.height - 40 - 35 * (p + 1), 35, 35), "", itemBlockStyle) && activeActiveWeapon[p] == o)
						{
							activeActiveWeapon[p] = 0;
							flR[o] = true;
							rightHand.Han[o - 1] = empty;
						}
					}
				}
			}
		}
		}
		else
		{
			skillPoints = teorSkillPoint;
			buffForse = 0;
			buffLovk = 0;
			buffMindForse = 0;
			buffTelo = 0;	
			changeItem = true;
			Total();
			//	totalCharacteristic.maxHits += PlayerHealthDefault;
		//		totalCharacteristic.stamina += PlayerDefaultStamina;
	//			totalCharacteristic.staminaRegen += PlayerDefaultStaminaRegen;
				personHits = totalCharacteristic.maxHits;
				personHRegen = totalCharacteristic.healthRegen;
				personMana = totalCharacteristic.maxMana;
				personMRegen = totalCharacteristic.manaRegen;
				personStamina = totalCharacteristic.stamina;
				personSRegen = totalCharacteristic.staminaRegen;
		}
}